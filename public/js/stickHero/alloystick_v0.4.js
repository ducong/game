/**
* 简单的HTML5游戏引擎
* @author   bizaitan
* @version  0.4
*
*/
var alloyge = {};


;(function(){

var requestAnimationFrame = window.requestAnimationFrame ||
							window.mozRequestAnimationFrame ||
							window.webkitRequestAnimationFrame ||
							window.msRequestAnimationFrame ||
							function( callback ){  
					            window.setTimeout(callback, 1000/60);  
					        };  



/**************  AlloyStict 基础函数 || 辅助类 *****************/
;(function(){
    function emptyFn() {};

    alloyge.inherit = function(child, parent) {
      var tmp = child;
      emptyFn.prototype = parent.prototype;
      child.prototype = new emptyFn;
      child.prototype.constructor = tmp;
      
      child.prototype._super = parent.prototype;
      return child;  
   	};

  	alloyge.delegate = function(fn,target){
   		//注意这个匿名函数非常重要，即让fn不立刻执行，还能够让arguments正确传参
   		return function(){
			fn.apply(target,arguments);
		}
   	};


   	alloyge.type = {
   		isArray : function(o){
			return o && (o.constructor === Array || ots.call(o) === "[object Array]");
		},
	    isObject : function(o) {
	    	return o && (o.constructor === Object || ots.call(o) === "[object Object]");
		},
	    isBoolean : function(o) {
	    	return (o === false || o) && (o.constructor === Boolean);
		},
	    isNumber : function(o) {
	    	return (o === 0 || o) && o.constructor === Number;
		},
	    isUndefined : function(o) {
	   		return typeof(o) === "undefined";
		},
	    isNull : function(o) {
	   		return o === null;
		},
	    isFunction : function(o) {
	   		return o && (o.constructor === Function);
		},
		isString : function(o) {
	    	return (o === "" || o) && (o.constructor === String);
		}
   	}
   


   	alloyge.monitorDom = null; 
   	alloyge.monitorFPS = function(scene){
   		if(!alloyge.monitorDom){
   			var monitorDom = document.createElement('div');
   			monitorDom.id = 'alloyge_monitor';
   			monitorDom.setAttribute('style','width: 150px;height: 80px;background-color: rgba(60, 60, 60, 0.51);'+
   				'position: absolute;top: 0;left: 0;color: #ddd;');
   			monitorDom.innerHTML = 'FPS:'+scene.avgFPS;
   			document.body.appendChild(monitorDom);

   			alloyge.monitorDom = monitorDom;
   		}
   	};

   	//辅助函数，获取page visibility的前缀
   	function getHiddenPrefix() {
	    return 'hidden' in document ? 'hidden' : function() {
	        var r = null;
	        ['webkit', 'moz', 'ms', 'o'].forEach(function(prefix) {
	            if((prefix + 'Hidden') in document) {
	                return r = prefix + 'Hidden';
	            }
	        });
	 
	        return r;
	    }();
	};

	alloyge.initEvenHadler = false;
   	alloyge.addGobalEvenHandler = function(scene){
   		if(getHiddenPrefix() == null || alloyge.initEvenHadler == true) return;  //不支持pagevisiblility 直接退出

   		alloyge.initEvenHadler = true;	//全局之执行一次
   		var hPrefix = getHiddenPrefix(),
   			prefix = hPrefix.substring(0, hPrefix.length - 6);

		document.addEventListener(prefix+'visibilitychange',function(){
			console.log('I have paused: ',scene.paused);
			//if(scene.paused){
				//切换回来的时候才用delay的方式防止“暴走动画”。TODO不是最优的解决“暴走动画的bug”
			//	setTimeout(function(){scene.pause();},50);
			//}else{
				scene.pause();
			//}
			
		});
   		
   	}

})();

/******************************		Obj 对象继承类	************************************/
;(function(){
	alloyge.Obj = function(){
		this.height = 0;
		this.width = 0;
		this.x = 0;			    //这个x,y是骨骼的偏移位置（具体实现在bone的updateDisplay的时候具体赋值）
		this.y = 0;			
		this.scaleX = 1;
		this.scaleY = 1;
		this.alpha = 1;
		this.originX = 0;		//这里是位图旋转中心，就是决定关节joint的位置	
		this.originY = 0;		//如果为0,0就是左上角为旋转中心
		this.rotation = 0;
		this.visible = true;	
		this.size = 1; 			//骨骼按比例渲染


		this.scene = null;
		this.parent = null;
	}

	var ptt = alloyge.Obj.prototype;
	//具体的render函数，由具体实现类去实现
	ptt.render = function(){}; 

	//私有方法 ,子类继承后，重写render类即可。_render方法有游戏循环调用
	ptt._render = function(context) {
		if(this.visible && this.alpha > 0){
			context.save();
			
			this._transform(context); //根据自身的参数映射到ctx中去
			
			this.render(context);
			context.restore();
		}  
	};

	ptt._transform = function(context) {

		context.translate(this.x*this.size, this.y*this.size); 
		if(this.size !== 1 ){ //整体上缩放
			context.scale(this.size, this.size); 
		}
		if(this.rotation % 360 > 0){
			context.rotate(this.rotation % 360 / 180 * Math.PI);
		}
		if(this.scaleX != 1 || this.scaleY != 1) {
			context.scale(this.scaleX, this.scaleY);
		}
		
		context.translate(-this.originX, -this.originY);
		
		context.globalAlpha *= this.alpha;	
	};

})();


/******************************		Display		**************************************/
(function() {


	/**
	* Display 渲染类
	* 具体有两种渲染方式：矢量渲染和图片渲染
	* x,y是骨骼的偏移位置，具体的由Armature类的x,y决定。
	*/
	alloyge.Display = function(image, frame) {
		alloyge.Obj.call(this);
		//x,y变量都没有赋值，按照默认的为0，因为是因为具体绘画位置xy是由bone的_render确定位置，
		this.image = image;
		this.frame = alloyge.type.isArray(frame) ? frame : [0, 0, image.width, image.height];  //注意位图资源先加载

		this.width = this.frame[2];
		this.height = this.frame[3];

		this.originX = -this.frame[4] || 0;  //origin参数在_render方法里面需要
		this.originY = -this.frame[5] || 0;  

		this.isVector = false;
	}; 

	alloyge.inherit(alloyge.Display, alloyge.Obj);
	var ptt = alloyge.Display.prototype;

	//注意是先执行_render
	ptt.render = function(context) {
		//  矢量的渲染实现
		if(this.isVector){
			context.strokeStyle="#aaa";
			context.strokeRect(0,0,this.width,this.height);
			//context.fillStyle="#fff";
			//context.fillRect(0,0,this.width,this.height);
		}else{
			//img,sx,sy,swidth,sheight,x,y,width,height
			//img; 裁切的xy，裁切的w,h  ; 在canvas上绘图的xy,在绘制的w,h
			context.drawImage(this.image, this.frame[0], this.frame[1], this.frame[2], this.frame[3], 0, 0, this.width, this.height);
		}
	}

})();

/******************************		Scene		**************************************/
;(function(){
	alloyge.Scene = function(context){
		this.context = context;
		this.canvas = context.canvas;

		this.children = [];

		this.cb = null;   //函数的额外回调函数

		this.paused = false;
		this.fps = 60;
		this.avgFPS = 0;  //实际运行的每秒的fps
		this.intervalID = null;
	};

	var ptt = alloyge.Scene.prototype;

	ptt.addObj = function(obj){
		obj.scene = this;
		this.children.push(obj);
	};
	ptt.removeObj = function(obj){
		var index = this.children.indexOf(obj);
		if(index > 0 || index < this.children.length){
			this.children.splice(index,1);
		}
	}

	ptt.setFPS = function(fps){
		this.fps = fps;
	};
	ptt.getFPS = function(){
		return this.fps;
	}

	ptt.start = function(cb){
		this.cb = alloyge.type.isFunction(cb) ? cb : null;
		requestAnimationFrame(this.loop.call(this));

		//alloyge.addGobalEvenHandler(this); //引擎的一些全局事件处理
	}

	ptt.pause = function(value){
		if(value == undefined){
			this.paused = !this.paused;
		}else{
			this.paused = value;
		}
	}

	ptt.loop = function(){

		var lastTime = window.mozAnimationStartTime || Date.now(),
			//都是用于计算平均fps
			frameCountPerSecond = 0,  //计算每秒实际绘制的次数 
       		preCountTime = lastTime;

		var	_this = this;

		//这玩意就用来控制fps的
		var sumDuration = 0,  //实际的时间间隔和
			sumInterval = 0,  //期望的时间间隔和
			frameDrawCount = 1; //实际的绘画次数而不是进入loop的次数

		return function(){

			var now = Date.now(),
				duration = now - lastTime,   //这是实际的时间间隔
				interval = 1000 / _this.fps;  //这是期望的时间间隔

			frameCountPerSecond++;		//这里主循环没loop一次就++记录一次

			//以一秒为一个周期对一些参数进击计算和更新
			if(now - preCountTime > 1000){

                _this.avgFPS = frameCountPerSecond; //平均fps
                frameCountPerSecond = 0;  //清0后重新计算
                preCountTime = now;       //再次记录每秒fps的起始记录时间

                if(alloyge.monitorDom){
                	alloyge.monitorDom.innerHTML = 'FPS:' + _this.avgFPS;
                }
                //console.log('avgFPS:',_this.avgFPS,' drawCount:',frameDrawCount);

                //这里每一秒就会对sumTime清零，虽然这样不能百分百控制实际fps绘制，但权衡得不错了
                sumDuration = 0;  
                sumInterval = 0;
                frameDrawCount = 1;
            }

			//检查停止循环的条件
			if (!_this.paused ) {

				/* TODO 下面的渲染计算其实可以优化到具体每个子类里面，也可以为每个子类设置fps，自己渲染计算
				*  这样一些场景不需要这么高的重绘率，会给整个游戏提高性能，这个有待优化
				*/
				sumDuration += duration;
	            sumInterval = interval * frameDrawCount;
	            if (_this.fps == 60 || sumDuration >= sumInterval) {

	                /************		所以这里是逻辑计算更新			**************/
					if(_this.cb != null) _this.cb(duration); //调用主程序的callback

					for (var i = 0, len = _this.children.length; i < len; i++) {
						_this.children[i].update(duration);
					}
						
					/************		所以这里是把更新计算的内容绘画			**************/
					_this.render(this.context);


	                frameDrawCount++; //更新实际绘画次数
	            };
			}

			lastTime = now;  //暂停了也要记录lastTime
			requestAnimationFrame(arguments.callee);
		}
		
	};


	ptt.render = function(context,rect){
		if (!context) context = this.context;
		if(rect){
			this.clear(rect.x,rect.y, rect.width, rect.height)
		}else{
			this.clear();
		}
		
		//把Scene中的Obj都render
		for (var i = 0, len = this.children.length; i < len; i++) {
			this.children[i].render(context);
		}
	}

	ptt.clear = function(x, y, width, height) {
		if(arguments.length >= 4){
			 this.context.clearRect(x, y, width, height);
		}else{
			this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
		}
	};
})();


})();



/**
* alloysk   HTML5骨骼动画引擎
* @author   bizaitan
* @version  0.4
*
*/


var alloysk = {};

;(function(){


//全局变量
var PI = Math.PI,
	HALF_PI = Math.PI * 0.5,
	DEG = PI/180;

//资源文件数据
alloysk.textureDatas = {};
alloysk.boneDatas = {};
alloysk.animationDatas = {};


//输出格式结构优化，减少处理逻辑
alloysk.addTextureData = function(data){
	alloysk.textureDatas = data;
};
//编辑器输出格式结构优化，减少处理逻辑
alloysk.addBoneData = function(data){
	alloysk.boneDatas = data;
}
//编辑器输出格式结构优化，减少处理逻辑
alloysk.addAnimationData = function(data){
	alloysk.animationDatas = data;
}

//旧版编辑器输出数据格式
alloysk.addSkeletonData = function(data){
	//console.log('data',data);
	for(var name in data){

		var boneDatas = data[name].bone;  //is Array
		var obj = {};
		for(var i = 0; i<boneDatas.length; i++){
			obj[boneDatas[i].name] =  boneDatas[i]
		}
		alloysk.boneDatas[name] = obj;


		var animationDatas = data[name].animation; //is Array
		
		var _obj = {};
		for(var j = 0; j<animationDatas.length;j++){


			var _aniObj = {},node = null, _bone = {};
			//将动画数据封装到Node类中去
			for(var boneName in animationDatas[j]){
				if(boneName == 'eventFrame') continue;
				var boneAniData = animationDatas[j][boneName];

				if(boneName == 'frame' || boneName == 'name'){
					_aniObj[boneName] = boneAniData;  //保持一样的数据
				}else{
					
					if(typeof boneAniData.length == 'undefined'){
						node = new alloysk.Node();
						node.initValue(boneAniData);
						_aniObj[boneName] = {
							'scale' : boneAniData.scale,
							'delay' : boneAniData.delay,
							'nodeList' : [node]   //注意这里如果只有一帧也用数组包装起来
						}
						
					}else{
						var nodeArray = [];
						for(var k =0; k<boneAniData.length; k++){
							node = new alloysk.Node();
							node.initValue(boneAniData[k]);
							nodeArray[k] = node;
						}
						_aniObj[boneName] = {
							'scale' : boneAniData[0].scale,
							'delay' : boneAniData[0].delay,
							'nodeList' : nodeArray   //注意这里如果只有一帧也用数组包装起来
						}
							
					}

				}
			}

			_obj[animationDatas[j].name] = _aniObj;
		}
		alloysk.animationDatas[name] = _obj;

	}
	//console.log('boneDatas',alloysk.boneDatas);
	//console.log('animationDatas',alloysk.animationDatas);
};

//兼容龙骨Flash编辑器输出的数据(先把数据转为旧数据再用addSkeletonData方法转换新数据)
alloysk.addDragonBoneData = function(data){
	var outputObj = {};
	for(var i = 0; i<data.armature.length; i++){
		var roleName = data.armature[i].name,
			old_bones = data.armature[i].bone,
			old_anims = data.armature[i].animation,
			old_skin = data.armature[i].skin[0];  //这里先默认只有一层skin

		var new_bones = [];
		for(var j = 0; j<old_bones.length; j++){
			var old_bone = old_bones[j];
			new_bones.push({
				'name' : old_bone.name,
				//'parent' : old_bone.parent,
				'x' : old_bone.transform.x,
				'y' : old_bone.transform.y,
				'scaleX' : old_bone.transform.scX,
				'scaleY' : old_bone.transform.scY
				// 'z'
			});
		}

		var new_anims = [];
		for(var k = 0; k<old_anims.length; k++){
			var anim = old_anims[k];
			var new_anim = {
				'name': anim.name,
				'frame' : anim.duration
			}
			for(var m = 0; m< anim.timeline.length; m++){
				var timeline = anim.timeline[m];
				var new_oneBoneAnimFrames = [];
				var delay = timeline.offset;  //TODO 龙骨编辑器的delay参数实现出现断骨的bug

				for(var n = 0; n<timeline.frame.length; n++){
					var frame = timeline.frame[n];
					var obj = {
						'x' : frame.transform.x,
						'y' : frame.transform.y,
						'z' : frame.z,
						'scaleX' : frame.transform.scX,
						'scaleY' : frame.transform.scY,
						'frame' : frame.duration,
						'rotation' : frame.transform.skX  //skX 和skY一样
					}
					//这里是转换为旧的格式，旧格式delay是在第一个关键帧obj上赋值delay的
					if(n == 0 && delay !== 0) 
						obj.delay = delay;   

					new_oneBoneAnimFrames.push(obj);
				}
				//某个骨骼的在这个动作里面的所有帧处理
				new_anim[timeline.name] = new_oneBoneAnimFrames;
			}
			new_anims.push(new_anim);
		}

		//对skin数据处理
		for(var x =0; x< old_skin.slot.length; x++){
			var slot = old_skin.slot[x];
			//z之后整理数据格式后会很好处理，这里先放着，小小的z现在不会影响
			//new_bones[slot.name].z = slot.z;
			alloysk.textureDatas[roleName][slot.name].originX = slot.display[0].transform.x;
			alloysk.textureDatas[roleName][slot.name].originY = slot.display[0].transform.y;
		}

		outputObj[roleName] = {
			'bone': new_bones,
			'animation' : new_anims
		}
	}

	alloysk.addSkeletonData(outputObj);
	
}

/**************  Node 基础节点类  *****************/
;(function(){
	//只带有渲染变量
	alloysk.Node = function(x,y,rotation){
		/**  渲染变量 **/
		this.x = x || 0;
		this.y = y || 0;
		this.rotation = rotation || 0;
		this.offR = 0;  //是否旋转过一圈 ，所以真实是角度应该是rotation+360*offR

		this.scaleX = 1;
		this.scaleY = 1;
		this.alpha = 1;
		this.frame = 1;
	}

	alloysk.Node.prototype.initValue = function(data){
		this.x = data.x;
		this.y = data.y;
		this.rotation = data.rotation;

		
		this.scaleX = data.scaleX || 1;
		this.scaleY = data.scaleY || 1;
		this.alpha = data.alpha || 1;
		this.frame = data.frame || 1;
		this.offR = data.offR || 0;
	}

})();


/**************  TweenNode 缓动节点类  ****************/
;(function(){

	alloysk.TweenNode = function(x, y, rotation){
		/**  渲染变量 **/
		alloysk.Node.call(this,x,y,rotation);

		/**  _sXX起始变量  _dXX差值变量 **/
		//_sXX,_dXX都是用于，根据currentPrencent调用tweento来更新

		this._sR = 0;   //from节点的rotation
		this._sX = 0;	//from节点的x
		this._sY = 0;	//from节点的y
		this._sSX = 0;	//from节点的scaleX
		this._sSY = 0;	//from节点的scaleY
		this._sA = 0;	//from节点的alpha
		
		this._dR = 0;	//rotation的差值
		this._dX = 0;	//x的差值
		this._dY = 0;	//y的差值
		this._dSX = 0;	//scaleX的差值
		this._dSY = 0;	//scaleY的差值
		this._dA = 0;	//alpha的差值
	}
	alloysk.TweenNode.prototype = new alloysk.Node();

	var ptt = alloysk.TweenNode.prototype;

	/**
	* 差值计算这个tweenNode的_sR,_sX等差值变量
	* from,to 可以是Node类也可以是TweenNode类
	* 但是如果是TweenNode类是不会用到里面_sR,_sX等的差值
	*/
	ptt.betweenValue = function(from, to){
		this._sR = from.rotation;
		this._sX = from.x;
		this._sY = from.y;
		this._sSX = from.scaleX;
		this._sSY = from.scaleY;
		this._sA = from.alpha;
		if(to.offR){
			this._dR = to.rotation + to.offR * 360 - this._sR;
		}else{
			this._dR = to.rotation - this._sR;
		}

		//TODO hack 基于输出数据是跨度不超过180
		if(this._dR > 180 ){
			this._dR = this._dR-360;
		}else if(this._dR < -180){
			this._dR = this._dR + 360;
		}

		this._dX = to.x - this._sX;
		this._dY = to.y - this._sY;
		this._dSX = to.scaleX - this._sSX;
		this._dSY = to.scaleY - this._sSY;
		this._dA = to.alpha - this._sA;
	}
	/**
	* 差值计算这个tweenNode的rotation,x,y等渲染变量
	* 注意：这个函数需要先执行betweenValue方法计算了差值后，才能正确调用
	* 根据上面betweenValue的差值和要缓动的precent，计算这个TweenNode的实际x,y,rotation,scaleX,scaleY,alpha
	*/
	ptt.tweenTo = function(currentPercent){
		this.rotation = this._sR + this._dR * currentPercent;
		this.x = this._sX + this._dX * currentPercent;
		this.y = this._sY + this._dY * currentPercent;
		
		if(this._dSX){
			this.scaleX = this._sSX + this._dSX * currentPercent;
		}else{
			this.scaleX = NaN;
		}
		if(this._dSY){
			this.scaleY = this._sSY + this._dSY * currentPercent;
		}else{
			this.scaleY = NaN;
		}
		if(this._dA){
			this.alpha = this._sA + this._dA * currentPercent;
		}else{
			this.alpha = NaN;
		}
	}

})();

/**************	 Tween 每个骨骼的缓动动画管理类  *****************/
;(function(){

	alloysk.Tween = function(tweenNode){
		this.tweenNode = tweenNode;

		//下面一块的变量是在playTo的时候才具体赋值
		this.nodeList = [];                
		this.delay = 0;						
		this.transitionFrames = 0;          //过渡动画的需要的总帧数 
		this.totalFrames = 0; 	 			//总帧数 是要演示使用的共多少帧(包括补间帧)  
		this.keyFrametotal = 0;             //总的关键帧数 区分totalFrames 在playTo的时候才具体赋值 
		this.loopTpye = -2;                 //循环类型，-1静态显示(包括循环不循环)，-2动态循环显示，-3动态不循环显示
		
		this.ease = true;        			 //默认为true
		this.rate = 1;           		     //显示帧的速率
		this.hasTransition = true;           //用于表示过渡动画展示了没有(每次playTo后都重新更新)

		this.currentPercent = 0;   
		this.currentFrame = 0;
		
		this.isComplete = false;
		this.isPause = false; 
		

		this.betweenFrame = 0;  //运行到某个关键帧obj的frame
		this.listEndFrame = 0;  //某个关键帧obj自身加上前面所有的关键帧obj的frame之和，临界点判断是否要切换from，to的node

	};

	var ptt = alloysk.Tween.prototype;

	ptt.update = function(){
		
		if(this.isComplete || this.isPause){
			return;
		}

		if(this.hasTransition){
			this.currentFrame += this.rate;
			this.currentPercent = this.currentFrame / this.transitionFrames;
			this.currentFrame %= this.transitionFrames;
			if(this.currentPercent < 1){
				if(this.ease){
					this.currentPercent = Math.sin(this.currentPercent * HALF_PI);
				}
			}else {
				/***  这里过渡动画执行结束后，根据loopTpye切换参数更新currentPercen t***/
				this.hasTransition = false;

				//静态显示  TODO 输出数据上没有输出静态显示的数据，待优化
				if(this.loopTpye == -1){
					this.currentPrecent = 1;
					this.isComplete = true;   //静态显示设置了isComplete，之后的update都不用做逻辑更新了
				}
				//动态循环显示
				if(this.loopTpye == -2){
					if(this.delay != 0){
						this.currentFrame = (1 - this.delay) * this.totalFrames;
						this.currentPercent += this.currentFrame / this.totalFrames;
					}
					this.currentPercent %= 1;  //这里我们更新了currentPercent
					this.listEndFrame = 0;    //注意这里赋值了一个这样的变量
				}
				//动态不循环显示
				if(this.loopTpye == -3){
					this.currentPercent = (this.currentPercent-1)*this.transitionFrames/this.totalFrames;
					if(this.currentPercent <1){
						this.currentPrecent %= 1;
						this.listEndFrame = 0;
					}
				}

				this.updateCurrentPercent();			
			}
		}else{
			this.currentFrame += this.rate;//每次updatey一次都会根据rate(显示速度)增加
			this.currentPercent = this.currentFrame / this.totalFrames;
			this.currentFrame %= this.totalFrames;	
			if(this.loopTpye == -3 && this.currentPercent >= 1){
				//动态不循环动画在循环动画执行一次后就停止了
				this.currentPercent = 1;
				this.isComplete = true;
			}else{
				this.currentPercent %= 1; 
			}

			this.updateCurrentPercent();	
		}
		
		this.tweenNode.tweenTo(this.currentPercent);
	};

	ptt.updateCurrentPercent = function(){
		//playedKeyFrames  相对于总关键帧和，运行到某个的当前帧数 
		var playedKeyFrames = this.keyFrametotal * this.currentPercent;
		/**
		* 关键帧obj切换的时候更新node差值
		*/
		if(playedKeyFrames <= this.listEndFrame-this.betweenFrame || playedKeyFrames > this.listEndFrame){
			this.listEndFrame = 0;
			var toIndex = 0,fromIndex = 0;
			//循环显示的核心
			while(playedKeyFrames >= this.listEndFrame){
				this.betweenFrame = this.nodeList[toIndex].frame;
				this.listEndFrame += this.betweenFrame;
				fromIndex = toIndex;
				//当运动到最后一个关键帧obj的时候，循环动画的时候，我们就使用from为最后一个关键帧obj，to为第一个关键帧obj
				if(++toIndex >= this.nodeList.length){
					toIndex = 0;
				}
			}
			
			var fromNode = this.nodeList[fromIndex];
			var toNode = this.nodeList[toIndex];

			//每当切换 关节帧之间的动作 会更新node的差值
			this.tweenNode.betweenValue(fromNode, toNode);
		}
		
		//这里我们换算currentPrecent，原来的currentPrecent是在总帧数上面，换算当前帧在某个关键帧obj的百分比
		this.currentPercent = 1 - (this.listEndFrame - playedKeyFrames) / this.betweenFrame;
		if(this.ease){
			this.currentPercent = 0.5 * (1 - Math.cos(this.currentPercent * PI));
		}
	}

})();



/**************  Bone 骨骼类           *****************/
;(function(){
	alloysk.Bone = function(roleName,boneName,display){
		this.body = null;  		//继承关系的父亲，一般都是armature类
		this._parent = null;  	//骨骼上的父子关系
		this.name = boneName;
		this.display = display;
		this.tweenNode = new alloysk.TweenNode();

		var boneData = alloysk.boneDatas[roleName][boneName] || {};

		//TODO 骨骼的绑定位置（没有选择角度）
		this._lockX = boneData.x || 0;
		this._lockY = boneData.y || 0;
		this._lockZ = boneData.z || 0;  //TODO目前还没有实现z排序的功能

		//_parent父骨骼的_transform属性
		this._parentX = 0;
		this._parentY = 0;
		this._parentR = 0;

		//根据update的时候赋值
		this._transformX = 0;		//要偏移的x
		this._transformY = 0;		//要偏移的y

	}

	var ptt = alloysk.Bone.prototype;

	ptt.addChild = function(child){
		child._parent = this;
		return child;
	}

	ptt.getGlobalX = function(){
		return this._transformX + this._parentX;
	}
	ptt.getGlobalY = function(){
		return this._transformY + this._parentY;
	}
	ptt.getGlobalR = function(){
		return this.tweenNode.rotation + this._parentR;
	}

	//update顺序：
	//1.tweenNode的update  (统一交给了Tween管理,bone的update都是在tweenupdate之后)
	//2.bone的update  (tweennode在上一步得到赋值)
	//3 bone.display的update
	ptt.update = function(){
		//bone的自身update
		if(this._parent){
			//更新parent属性
			this._parentX = this._parent.getGlobalX();	
			this._parentY = this._parent.getGlobalY();
			this._parentR = this._parent.getGlobalR();
			
			var _dX = this._lockX  + this.tweenNode.x;
			var _dY = this._lockY  + this.tweenNode.y;
			var _r = Math.atan2(_dY, _dX) + this._parentR * DEG;
			var _len = Math.sqrt(_dX * _dX + _dY * _dY);
			this._transformX = _len * Math.cos(_r);
			this._transformY = _len * Math.sin(_r);
		}else{
			//下面这句代码我们可以看出：
			//playTo过后Bone的tweenNode的起值(_sXX)和差值(_dXX)是赋值了，
			//而在这个Bone.update上面现有Tween的update，其实就是Bone.tweenNode的update(本质就是根据currentPercent来tweenTo来使到tweenNode的渲染值得到赋值)
			//所以下面两句代码中的tweenNode.x就是我所说的tween在每次update后的赋值
			this._transformX = this.tweenNode.x;
			this._transformY = this.tweenNode.y;
		}

		//display的update
		this.updateDisplay();
	}

	ptt.updateDisplay = function(){
		//也存在没有display渲染层的bone
		if(this.display){
			this.display.x = this._transformX + this._parentX;
			this.display.y = this._transformY + this._parentY;
			var rotation = this.tweenNode.rotation + this._parentR; //i delete node
			rotation%=360;
			if(rotation<0){
				rotation+=360;
			}
			this.display.rotation = rotation;
			
			if(isNaN(this.tweenNode.scaleX)){
			}else{
				this.display.scaleX = this.tweenNode.scaleX;
			}
			if(isNaN(this.tweenNode.scaleY)){
			}else{
				this.display.scaleY = this.tweenNode.scaleY;
			}
			if(!isNaN(this.tweenNode.alpha)){
				if(this.tweenNode.alpha){
					this.display.visible = true;
					this.display.alpha = this.tweenNode.alpha;
				}else{
					this.display.visible = false;
				}
			}
		}
	}

})();



/**************  Armature 整体类       *****************/

;(function(){	
	alloysk.Armature = function(roleName,img){
		this.visible = true;
		this.alpha = 1;
		this.x = 0;
		this.y = 0;

		this.boneList = [];		   //两种不同的结构对bone的存储
		this.boneObjs = {};
		this.tweenObjs = {};       //装的是骨骼的tween类
		this.roleName = roleName;  //记录这个armature的name

		this.fps = 60;             //一秒重绘多少次画面，只影响性能(速度？) TODO 目前fps没有应用到具体某个armature,后记统一性能优化处理 
		this.scene = null;         //表示这个armature被那个scene add，最重要的是使到两者数据打通
		this.size = 1;			   //按输出数据的比例播放

		var boneDatas = alloysk.boneDatas[roleName];
		var textureDatas = alloysk.textureDatas[roleName];

		for(var boneName in boneDatas){
			var boneTd =  textureDatas[boneName];
			//TODO 是否也把这个display用list装起来放到armature的变量里面
			//TODO Display类优化
			var display = new alloyge.Display(img,[boneTd.x,boneTd.y,boneTd.width,boneTd.height,boneTd.originX,boneTd.originY]);

			//这里没有把bone的父子关系逻辑放到构造函数里面，一是由于输出数据的独立性，二是bone可以不依赖关系就可以new出来
			var bone = new alloysk.Bone(roleName,boneName,display);
			bone.body = this;  //bone的parent在外面赋值
			this.boneList.push(bone);
			this.boneObjs[boneName] = bone;

			this.tweenObjs[boneName] = new alloysk.Tween(bone.tweenNode); //这里先赋值上一个空的tween类
		}

		//建立bone的关系
		this.buildJoint();
	}

	var ptt = alloysk.Armature.prototype;

	ptt.setPos = function(x,y){
		this.x = x;
		this.y = y;
	};
	ptt.setFps = function(fps){
		this.fps = fps;
	};
	ptt.setFrameTotals = function(totalFrames){
		for(var boneName in this.tweenObjs){
			this.tweenObjs[boneName].totalFrames = totalFrames;
		}
	};

	ptt.setEaseType = function(type){
		for(var boneName in this.tweenObjs){
			this.tweenObjs[boneName].ease = type;
		}
	};
	ptt.setSize = function(newSize){
		for(var boneName in this.boneObjs){
			this.boneObjs[boneName].display.size = newSize;
		}
	};
	ptt.isVector = function(){
		for(var boneName in this.boneObjs){
			if(this.boneObjs[boneName].display.isVector == false){
				return false
			}
		}
		return true;
	};
	//不传值代表取反
	ptt.setVector = function(isVector){
		if(isVector == undefined){
			this.setVector(! this.isVector());
		}else{
			for(var boneName in this.boneObjs){
				this.boneObjs[boneName].display.isVector = isVector;
			}
		}
	};
	ptt.isComplete = function(){
		//注意不是所有的骨骼的动画总帧数是一样的（如有scale参数的骨骼），所以不能以一代全。
		for(var boneName in this.tweenObjs){
			if(this.tweenObjs[boneName].isComplete == false){
				return false
			}
		}
		return true;
	};
	ptt.isPause = function(){
		for(var boneName in this.tweenObjs){
			if(this.tweenObjs[boneName].isPause == false){
				return false
			}
		}
		return true;
	}
	//不传值就是取反动画的pause值
	ptt.pause = function(value){
		if(value == undefined){
			var isPause = this.isPause();
			this.pause(!isPause);
		}else{
			for(var boneName in this.tweenObjs){
				this.tweenObjs[boneName].isPause = value;
			}
		}
	}

	//bone都new好后，建立bone之间的父子关系
	ptt.buildJoint = function(){
		var boneDatas = alloysk.boneDatas[this.roleName],
			boneObjs = this.boneObjs,
			bone,boneData,boneParent;
		for(var boneName in boneDatas){

			boneData = boneDatas[boneName];
			bone = boneObjs[boneName];
			boneParent = boneObjs[boneData._parent];
			if(boneParent){
				boneParent.addChild(bone);
			}
		}
	};
	
	ptt.update = function(duration){
		
		for(var index = 0,len = this.boneList.length; index < len; index++){

			/************      bone的tweenNode的update   ****************/
			var bone = this.boneList[index];

			//why 不把tween类的更新放到bone类中去？
			//因为有延迟动画的计算，tween类的currentPercent需要重新计算，当把计算currentPercent放到每个骨骼里面去了
			//又会缺少一些全局参数，例如totoalFrame。所以抽离tween类为Armature类统一管理，计算结果自然到bone的tweenNode中
			this.tweenObjs[bone.name].update();
			bone.update();							
		}
	};

	ptt.render = function(context) {
		if(this.visible && this.alpha > 0){
			context.save();

			//this.transform(context);
			context.translate(this.x,this.y);

			for(var i =0; i< this.boneList.length; i++){
				this.boneList[i].display._render(context);
			}
			context.restore();
		}  
	};

	/**
	* 本质上这里实现了bone的某个动画tweenNode的差值(tween类赋值)
	*/
	ptt.playTo = function(aniName,totalFrames,transitionFrames,isloop){
		var aniData = alloysk.animationDatas[this.roleName][aniName],
			fromNode = new alloysk.TweenNode(),
			toNode  = new alloysk.TweenNode();

		for(var boneName in aniData){
			if(boneName != 'name' && boneName != 'frame'){

				var bone = this.boneObjs[boneName],
					boneAniData = aniData[boneName];

				//tween具体赋值。
				var tween = this.tweenObjs[boneName];
				tween.nodeList = boneAniData.nodeList;
				tween.delay = boneAniData.delay || 0;
				tween.keyFrametotal = aniData['frame'];
				tween.totalFrames = totalFrames * (boneAniData.scale || 1);
				tween.transitionFrames = transitionFrames;
				tween.hasTransition = true;    //重新更新
				tween.isComplete = tween.isPause = false;  //刷新
 
				

				/**************    这里计算的from to 是过渡动画的   ****************/
				//fromNode使用上一次的tweenNode的渲染值作为起始值，如果上一次没有，则是空值开始
				fromNode.initValue(tween.tweenNode);

				if(boneAniData.nodeList.length > 1){
					//如果isloop为false，则是动态不循环动画(默认是动态循环动画)
					if(!isloop){
						tween.loopTpye = -3;
						tween.keyFrametotal -= 1;
					}else{
						tween.loopTpye = -2;   //这里如果在动画切换的时候上一次是-3，切换回-2的时候，你这里不赋值就出错了。
					}
					
					if(tween.delay !== 0){

						//这个delay的计算跟Tween类的updateCurrentPercent方法的逻辑是一模一样的，TODO 看以后可以优化为一个函数不？
						var playedKeyFrames = tween.keyFrametotal * (1 - tween.delay);
						var fromIndex = 0,
							toIndex = 0,	
							listEndFrame = 0,	
							betweenFrame = 0;
						while(playedKeyFrames >= listEndFrame){

							betweenFrame = tween.nodeList[toIndex].frame;
							listEndFrame += betweenFrame;
							fromIndex = toIndex;
							if(++toIndex >= tween.nodeList.length){
								toIndex = 0;
							}
						}			
						toNode.betweenValue(tween.nodeList[fromIndex],tween.nodeList[toIndex]); 
						var currentPercent = 1 - (listEndFrame - playedKeyFrames)/betweenFrame;
						if(tween.ease){
							currentPercent = 0.5 * (1 - Math.cos(currentPercent * PI ));
						}
						toNode.tweenTo(currentPercent);

					}else {
						toNode.initValue(aniData[boneName].nodeList[0]);
					}

				}else {
					//静态显示
					toNode.initValue(aniData[boneName].nodeList[0]); //(数据做了处理，静态显示的也装在只有一个数据的数组里面)
					tween.loopTpye = -1;
				}

				bone.tweenNode.betweenValue(fromNode,toNode);	
				
			}
		}
	};

})();


})(); //end of all

alloysk.addTextureData(
{"xiaoxiao":{"head":{"x":0,"width":66,"y":0,"originX":-33,"originY":-33,"height":66},"bodyUp":{"x":68,"width":28,"y":0,"originX":-13.65,"originY":-13.15,"height":77},"bodyDown":{"x":68,"width":28,"y":0,"originX":-14,"originY":-19.5,"height":77},"armLeft":{"x":68,"width":28,"y":0,"originX":-14,"originY":-18.5,"height":77},"armRight":{"x":68,"width":28,"y":0,"originX":-10.85,"originY":-6.05,"height":77},"handLeft":{"x":68,"width":28,"y":0,"originX":-15.35,"originY":-10.95,"height":77},"handRight":{"x":68,"width":28,"y":0,"originX":-16.85,"originY":-9.85,"height":77},"legLeft":{"x":68,"width":28,"y":0,"originX":-18.15,"originY":-15.7,"height":77},"legRight":{"x":68,"width":28,"y":0,"originX":-14,"originY":-10.5,"height":77},"shankLeft":{"x":68,"width":28,"y":0,"originX":-12.05,"originY":-13.2,"height":77},"shankRight":{"x":68,"width":28,"y":0,"originX":-14,"originY":-13.65,"height":77},"footLeft":{"x":98,"width":28,"y":0,"originX":-15.7,"originY":-5.7,"height":46},"footRight":{"x":98,"width":28,"y":0,"originX":-14,"originY":-8.95,"height":46}}}
);
alloysk.addBoneData(
{"xiaoxiao":{"head":{"name":"head","x":-1,"y":-80,"scaleX":1,"scaleY":1},"bodyUp":{"name":"bodyUp","x":2.65,"y":-50.5,"scaleX":1,"scaleY":1},"bodyDown":{"name":"bodyDown","x":14,"y":1.5,"scaleX":1,"scaleY":1},"armLeft":{"name":"armLeft","x":8.25,"y":-39.2,"scaleX":1,"scaleY":1},"handLeft":{"name":"handLeft","x":14,"y":1.5,"scaleX":1,"scaleY":0.87},"armRight":{"name":"armRight","x":-0.95,"y":-47,"scaleX":1,"scaleY":1.16},"handRight":{"name":"handRight","x":33,"y":14.7,"scaleX":1,"scaleY":0.91},"legLeft":{"name":"legLeft","x":14,"y":49,"scaleX":1,"scaleY":1},"shankLeft":{"name":"shankLeft","x":4.7,"y":99.85,"scaleX":1,"scaleY":1},"footLeft":{"name":"footLeft","x":55.2,"y":121.55,"scaleX":1,"scaleY":1},"legRight":{"name":"legRight","x":14,"y":49,"scaleX":1,"scaleY":1},"shankRight":{"name":"shankRight","x":14,"y":104.5,"scaleX":1,"scaleY":1},"footRight":{"name":"footRight","x":15.25,"y":156.05,"scaleX":1,"scaleY":1.03}}}
);
alloysk.addAnimationData(
{"xiaoxiao":{"run":{"name":"run","frame":44,"footRight":{"nodeList":[{"x":15.25,"y":156.05,"rotation":90,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":4},{"x":59.95,"y":151.6,"rotation":75,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":100.4,"y":111.65,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":5},{"x":93.3,"y":85.4,"rotation":-60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":71.7,"y":76.8,"rotation":-30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":19.25,"y":100.4,"rotation":15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-52.2,"y":105.4,"rotation":67.04,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":5},{"x":-19.6,"y":150.55,"rotation":82.04,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":5},{"x":-11.5,"y":149.2,"rotation":82.04,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":5}]},"shankRight":{"nodeList":[{"x":14,"y":104.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":31.85,"y":106.95,"rotation":-30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":53.45,"y":88.2,"rotation":-60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":46.15,"y":97.6,"rotation":-105,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":29.4,"y":104.75,"rotation":-120,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-27.65,"y":87.5,"rotation":-75,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-40.6,"y":61.25,"rotation":22.04,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-18.9,"y":98.8,"rotation":7.04,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5},{"x":-7.15,"y":99.05,"rotation":7.04,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5}]},"legRight":{"nodeList":[{"x":14,"y":49,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":14,"y":49,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":14,"y":49,"rotation":-45,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":14,"y":49,"rotation":-30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":14,"y":49,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":8.45,"y":49.95,"rotation":45,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":1.35,"y":49.05,"rotation":75,"offR":0,"scaleX":1,"scaleY":0.83,"alpha":1,"frame":5},{"x":7.6,"y":46.2,"rotation":30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":8.85,"y":41.45,"rotation":15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]},"footLeft":{"nodeList":[{"x":55.2,"y":121.55,"rotation":45,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-10.45,"y":104.65,"rotation":60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-50.25,"y":101.15,"rotation":75,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":5},{"x":-34.75,"y":149.65,"rotation":90,"offR":0,"scaleX":1,"scaleY":1.12,"alpha":1,"frame":5},{"x":-17.75,"y":154.65,"rotation":90,"offR":0,"scaleX":1,"scaleY":1.12,"alpha":1,"frame":5},{"x":39.7,"y":160.1,"rotation":60,"offR":0,"scaleX":1,"scaleY":1.12,"alpha":1,"frame":5},{"x":106.25,"y":105.25,"rotation":8.45,"offR":0,"scaleX":1,"scaleY":1.12,"alpha":1,"frame":5},{"x":108.1,"y":60.3,"rotation":-29.99,"offR":0,"scaleX":1,"scaleY":1.12,"alpha":1,"frame":5},{"x":75.45,"y":63.45,"rotation":-44.99,"offR":0,"scaleX":1,"scaleY":1.12,"alpha":1,"frame":5}]},"shankLeft":{"nodeList":[{"x":4.7,"y":99.85,"rotation":-60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-35.75,"y":69.15,"rotation":-30,"offR":0,"scaleX":0.92,"scaleY":0.84,"alpha":1,"frame":5},{"x":-38.2,"y":54.8,"rotation":30,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-32.45,"y":98.3,"rotation":15,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-15.4,"y":98.4,"rotation":15,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5},{"x":13.6,"y":110.2,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5},{"x":50.95,"y":94.75,"rotation":-66.56,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5},{"x":58.3,"y":86.45,"rotation":-105,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5},{"x":34.15,"y":101.55,"rotation":-120,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5}]},"legLeft":{"nodeList":[{"x":14,"y":49,"rotation":15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":14,"y":48.95,"rotation":60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":14.05,"y":49,"rotation":75,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":14.05,"y":49,"rotation":38.26,"offR":0,"scaleX":1,"scaleY":1.21,"alpha":1,"frame":5},{"x":14.05,"y":49,"rotation":23.26,"offR":0,"scaleX":1,"scaleY":1.21,"alpha":1,"frame":5},{"x":14.4,"y":52.8,"rotation":-6.73,"offR":0,"scaleX":1,"scaleY":1.21,"alpha":1,"frame":5},{"x":15.65,"y":45.35,"rotation":-43.29,"offR":0,"scaleX":1,"scaleY":1.21,"alpha":1,"frame":5},{"x":18.4,"y":48.95,"rotation":-51.73,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":5},{"x":19.75,"y":48.5,"rotation":-21.74,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":5}]},"handRight":{"delay":0.05,"nodeList":[{"x":33,"y":14.7,"rotation":105,"offR":0,"scaleX":1,"scaleY":0.91,"alpha":1,"frame":4},{"x":51.15,"y":-4.2,"rotation":-15,"offR":0,"scaleX":1,"scaleY":0.82,"alpha":1,"frame":5},{"x":62.55,"y":-33.15,"rotation":-45,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":5},{"x":51.1,"y":-26.8,"rotation":-30,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":5},{"x":49.05,"y":-10,"rotation":30,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":5},{"x":17,"y":2,"rotation":60,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":5},{"x":-46.85,"y":-2.85,"rotation":90,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-24.1,"y":-0.25,"rotation":75,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":5},{"x":-11.5,"y":-0.95,"rotation":75,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":5}]},"armRight":{"delay":0.05,"nodeList":[{"x":-0.95,"y":-47,"rotation":-30,"offR":0,"scaleX":1,"scaleY":1.16,"alpha":1,"frame":4},{"x":-0.95,"y":-47,"rotation":-45,"offR":0,"scaleX":1,"scaleY":1.16,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-75,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":-1.05,"y":-47,"rotation":-60,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":-0.45,"y":-51,"rotation":-45,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":-5.4,"y":-58.15,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":-8.6,"y":-44.45,"rotation":45,"offR":0,"scaleX":1,"scaleY":0.93,"alpha":1,"frame":5},{"x":-2.4,"y":-47.55,"rotation":30,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-6.65,"y":-53.3,"rotation":15,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5}]},"handLeft":{"nodeList":[{"x":14,"y":1.5,"rotation":45,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":4},{"x":-16.8,"y":4,"rotation":60,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-30.35,"y":-15.6,"rotation":105,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":5},{"x":-25.4,"y":-2.55,"rotation":105,"offR":0,"scaleX":1,"scaleY":0.83,"alpha":1,"frame":5},{"x":-17.4,"y":5.45,"rotation":75,"offR":0,"scaleX":1,"scaleY":0.83,"alpha":1,"frame":5},{"x":24.85,"y":-2.55,"rotation":90,"offR":0,"scaleX":1,"scaleY":0.83,"alpha":1,"frame":5},{"x":55.5,"y":-28.65,"rotation":45,"offR":0,"scaleX":1,"scaleY":0.7,"alpha":1,"frame":5},{"x":50.15,"y":-21.3,"rotation":-15,"offR":0,"scaleX":1,"scaleY":0.77,"alpha":1,"frame":5},{"x":51.35,"y":-18.5,"rotation":30,"offR":0,"scaleX":1,"scaleY":0.83,"alpha":1,"frame":5}]},"armLeft":{"delay":0.05,"nodeList":[{"x":8.25,"y":-39.2,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":8.2,"y":-39.15,"rotation":30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":8.25,"y":-39.2,"rotation":60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":8.25,"y":-39.2,"rotation":45,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":8.2,"y":-39.2,"rotation":30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":10.85,"y":-41.25,"rotation":-75,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":10.25,"y":-45,"rotation":-60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11.45,"y":-42.25,"rotation":-60,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]},"bodyDown":{"nodeList":[{"x":14,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":14,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":11,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]},"bodyUp":{"nodeList":[{"x":2.65,"y":-50.5,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":2.65,"y":-50.5,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-1,"y":-47,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]},"head":{"nodeList":[{"x":-1,"y":-80,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-10.75,"y":-72.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-19.75,"y":-81.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-10.75,"y":-81.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-11.75,"y":-81.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-11.75,"y":-81.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-16.75,"y":-78.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-18.75,"y":-81.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-15.75,"y":-85.7,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]}},"simpleHit":{"name":"simpleHit","frame":9,"footRight":{"nodeList":[{"x":82.4,"y":145.55,"rotation":82.04,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":83.9,"y":145.05,"rotation":81.79,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5}]},"shankRight":{"nodeList":[{"x":42.9,"y":98.5,"rotation":-37.95,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":4},{"x":39.5,"y":95.1,"rotation":-38.2,"offR":0,"scaleX":1,"scaleY":1.14,"alpha":1,"frame":5}]},"legRight":{"nodeList":[{"x":13.4,"y":47.1,"rotation":-30,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":10.55,"y":44.85,"rotation":-30.25,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]},"footLeft":{"nodeList":[{"x":-57.9,"y":132.9,"rotation":90.01,"offR":0,"scaleX":1,"scaleY":1.2,"alpha":1,"frame":5},{"x":-57.9,"y":132.9,"rotation":90.01,"offR":0,"scaleX":1,"scaleY":1.2,"alpha":1,"frame":3},{"x":-65.35,"y":148,"rotation":89.76,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-36.35,"y":71.7,"rotation":30,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":5},{"x":-36.35,"y":71.7,"rotation":30,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":3},{"x":-41.05,"y":86.75,"rotation":29.75,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":11.1,"y":59,"rotation":68.26,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":5},{"x":11.1,"y":59,"rotation":68.26,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":3},{"x":6.2,"y":58.8,"rotation":53.01,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":55.15,"y":-70.75,"rotation":-135,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":4},{"x":19.8,"y":-87.25,"rotation":-180,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":2},{"x":-32.35,"y":-85.25,"rotation":120,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":2},{"x":-73.9,"y":-46,"rotation":90,"offR":0,"scaleX":1,"scaleY":1.26,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":1.25,"y":-50.05,"rotation":-105,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":4},{"x":-3.65,"y":-34.6,"rotation":-150,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":2},{"x":0.4,"y":-42.55,"rotation":150,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":2},{"x":0.35,"y":-42.45,"rotation":97.76,"offR":0,"scaleX":1,"scaleY":1.25,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":-15.05,"y":-8.3,"rotation":75,"offR":0,"scaleX":1,"scaleY":0.96,"alpha":1,"frame":4},{"x":32.65,"y":-19.5,"rotation":75,"offR":0,"scaleX":1,"scaleY":0.96,"alpha":1,"frame":4},{"x":42.35,"y":-32.85,"rotation":82.94,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":-9.9,"y":-39.65,"rotation":15,"offR":0,"scaleX":1,"scaleY":0.73,"alpha":1,"frame":4},{"x":2.3,"y":-46.95,"rotation":-45,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":4},{"x":3,"y":-49.2,"rotation":-75,"offR":0,"scaleX":1,"scaleY":0.96,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":12,"y":7.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":0.79,"alpha":1,"frame":4},{"x":13,"y":1.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]},"bodyUp":{"nodeList":[{"x":-3.75,"y":-52.7,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-2.75,"y":-51.7,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]},"head":{"nodeList":[{"x":-33.25,"y":-79.8,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-30.25,"y":-68.8,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5}]}},"secondHit":{"name":"secondHit","frame":6,"footRight":{"nodeList":[{"x":83.9,"y":145.05,"rotation":81.79,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":58.55,"y":153.7,"rotation":-83.21,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":1}]},"shankRight":{"nodeList":[{"x":39.5,"y":95.1,"rotation":-38.2,"offR":0,"scaleX":1,"scaleY":1.14,"alpha":1,"frame":5},{"x":55.25,"y":101.85,"rotation":-8.21,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":1}]},"legRight":{"nodeList":[{"x":10.55,"y":44.85,"rotation":-30.25,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":18.55,"y":64.85,"rotation":-45.25,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"footLeft":{"nodeList":[{"x":-65.35,"y":148,"rotation":89.76,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":5},{"x":-65.35,"y":148,"rotation":89.76,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-41.05,"y":86.75,"rotation":29.75,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":5},{"x":-41.05,"y":86.75,"rotation":29.75,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":6.2,"y":58.8,"rotation":53.01,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":5},{"x":12.2,"y":71.8,"rotation":68.01,"offR":0,"scaleX":1,"scaleY":1.01,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":-24.35,"y":-25.45,"rotation":90,"offR":0,"scaleX":1,"scaleY":0.67,"alpha":1,"frame":3},{"x":56.05,"y":-29.45,"rotation":128.49,"offR":0,"scaleX":1,"scaleY":0.67,"alpha":1,"frame":2},{"x":51.05,"y":-10.3,"rotation":173.49,"offR":0,"scaleX":1,"scaleY":0.73,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":9.65,"y":-26.65,"rotation":97.76,"offR":0,"scaleX":1,"scaleY":0.64,"alpha":1,"frame":3},{"x":15.55,"y":-15.65,"rotation":-112.23,"offR":0,"scaleX":1,"scaleY":0.73,"alpha":1,"frame":2},{"x":15.55,"y":-15.65,"rotation":-82.24,"offR":0,"scaleX":1,"scaleY":0.73,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":43.8,"y":-13.85,"rotation":82.94,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":3},{"x":-20.95,"y":-8.1,"rotation":89.98,"offR":0,"scaleX":1,"scaleY":0.72,"alpha":1,"frame":2},{"x":-50.15,"y":-9.75,"rotation":91.48,"offR":0,"scaleX":1,"scaleY":1.33,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":9.65,"y":-26.65,"rotation":-75,"offR":0,"scaleX":1,"scaleY":0.77,"alpha":1,"frame":3},{"x":0.7,"y":-17.1,"rotation":75,"offR":0,"scaleX":1,"scaleY":0.52,"alpha":1,"frame":2},{"x":0.65,"y":-17,"rotation":83.47,"offR":0,"scaleX":1,"scaleY":1.11,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":16,"y":20.25,"rotation":0,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":3},{"x":16,"y":20.25,"rotation":0,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":3}]},"bodyUp":{"nodeList":[{"x":1.25,"y":-39.95,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":1.25,"y":-39.95,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3}]},"head":{"nodeList":[{"x":-12.25,"y":-66.05,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-7.05,"y":-59.55,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-0.25,"y":-54,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]}},"jump_kick":{"name":"jump_kick","frame":12,"footRight":{"nodeList":[{"x":58.55,"y":153.65,"rotation":-91.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":3},{"x":57.25,"y":148.95,"rotation":-61.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":2},{"x":52.25,"y":113.95,"rotation":-61.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":2},{"x":47.45,"y":105.6,"rotation":-46.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":2},{"x":3.7,"y":91.2,"rotation":-46.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":2},{"x":-2,"y":83.65,"rotation":-31.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":1}]},"shankRight":{"nodeList":[{"x":62.55,"y":99,"rotation":-0.75,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":3},{"x":43.95,"y":100.65,"rotation":-16.77,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":2},{"x":38.95,"y":65.65,"rotation":-16.77,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":2},{"x":48.5,"y":52.5,"rotation":-1.78,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":2},{"x":29.65,"y":65.95,"rotation":34.69,"offR":0,"scaleX":1,"scaleY":0.71,"alpha":1,"frame":2},{"x":29.7,"y":65.95,"rotation":49.69,"offR":0,"scaleX":1,"scaleY":0.71,"alpha":1,"frame":1}]},"legRight":{"nodeList":[{"x":18.55,"y":68.45,"rotation":-54.24,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":3},{"x":5.75,"y":56.1,"rotation":-42.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":0.75,"y":21.1,"rotation":-42.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":0.75,"y":21.15,"rotation":-57.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":0.75,"y":21.15,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":0.75,"y":21.15,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"footLeft":{"nodeList":[{"x":-14.05,"y":155.15,"rotation":89.76,"offR":0,"scaleX":1,"scaleY":1.14,"alpha":1,"frame":3},{"x":-17.3,"y":153.85,"rotation":89.76,"offR":0,"scaleX":1,"scaleY":1.14,"alpha":1,"frame":2},{"x":-0.3,"y":105.2,"rotation":37.28,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-46.1,"y":89.2,"rotation":75.49,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-105.35,"y":53.55,"rotation":90.49,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-127.4,"y":35.65,"rotation":105.49,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-32.75,"y":103.25,"rotation":-7.94,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":3},{"x":-36,"y":94.85,"rotation":-7.94,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":2},{"x":-39.8,"y":60.5,"rotation":-30.45,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":2},{"x":-49.45,"y":29.65,"rotation":7.75,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":2},{"x":-59.1,"y":25.75,"rotation":67.75,"offR":0,"scaleX":1,"scaleY":1.11,"alpha":1,"frame":2},{"x":-67.4,"y":24.8,"rotation":85.4,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":8.05,"y":80.1,"rotation":53.01,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":3},{"x":4.85,"y":66.15,"rotation":45.24,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":2},{"x":-0.15,"y":31.15,"rotation":45.24,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":2},{"x":-0.15,"y":31.15,"rotation":83.45,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":2},{"x":-0.15,"y":31.15,"rotation":89.67,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":2},{"x":-2.8,"y":31.15,"rotation":89.67,"offR":0,"scaleX":1,"scaleY":1.15,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":59.25,"y":6.8,"rotation":66.68,"offR":0,"scaleX":1,"scaleY":0.86,"alpha":1,"frame":3},{"x":0.25,"y":18.05,"rotation":105.23,"offR":0,"scaleX":1,"scaleY":0.91,"alpha":1,"frame":2},{"x":-30.7,"y":-31.05,"rotation":75.23,"offR":0,"scaleX":1,"scaleY":0.77,"alpha":1,"frame":2},{"x":-35.6,"y":-21.95,"rotation":75.23,"offR":0,"scaleX":1,"scaleY":0.77,"alpha":1,"frame":2},{"x":-21.4,"y":-12.6,"rotation":75.23,"offR":0,"scaleX":1,"scaleY":0.77,"alpha":1,"frame":2},{"x":-19.4,"y":-10.6,"rotation":75.23,"offR":0,"scaleX":1,"scaleY":0.77,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":7.4,"y":-11.2,"rotation":-67.24,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":3},{"x":-16.7,"y":-24.85,"rotation":-13.69,"offR":0,"scaleX":1,"scaleY":0.73,"alpha":1,"frame":2},{"x":-20.4,"y":-62.7,"rotation":31.3,"offR":0,"scaleX":1,"scaleY":0.6,"alpha":1,"frame":2},{"x":-20.4,"y":-62.75,"rotation":31.3,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":2},{"x":-15.1,"y":-53.2,"rotation":23.11,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":2},{"x":-15.1,"y":-53.2,"rotation":8.11,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":-5.25,"y":10.15,"rotation":91.48,"offR":0,"scaleX":1,"scaleY":0.76,"alpha":1,"frame":3},{"x":8.7,"y":17.5,"rotation":71.56,"offR":0,"scaleX":1,"scaleY":0.84,"alpha":1,"frame":2},{"x":36.4,"y":-49.2,"rotation":-41.16,"offR":0,"scaleX":1,"scaleY":0.84,"alpha":1,"frame":2},{"x":36.4,"y":-49.2,"rotation":-41.16,"offR":0,"scaleX":1,"scaleY":0.84,"alpha":1,"frame":2},{"x":49.25,"y":-55.95,"rotation":-41.16,"offR":0,"scaleX":1,"scaleY":0.84,"alpha":1,"frame":2},{"x":49.95,"y":-52.75,"rotation":-71.16,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":-0.75,"y":-13.55,"rotation":23.47,"offR":0,"scaleX":1,"scaleY":0.57,"alpha":1,"frame":3},{"x":-11.7,"y":-21.05,"rotation":-26.45,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":2},{"x":-3.9,"y":-60.6,"rotation":-71.45,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":2},{"x":-3.9,"y":-60.6,"rotation":-71.45,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":2},{"x":4.6,"y":-55.15,"rotation":-86.45,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":2},{"x":4.6,"y":-55.15,"rotation":-86.45,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":14.5,"y":23.75,"rotation":0,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":3},{"x":1.55,"y":11.25,"rotation":-6.46,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":2},{"x":-3.45,"y":-23.75,"rotation":-6.46,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":2},{"x":-3.45,"y":-23.75,"rotation":-6.46,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":2},{"x":5.7,"y":-21.9,"rotation":8.27,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":2},{"x":5.7,"y":-21.9,"rotation":8.27,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":1}]},"bodyUp":{"nodeList":[{"x":-0.25,"y":-36.45,"rotation":-15,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-19.85,"y":-46.9,"rotation":-21.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-24.85,"y":-81.9,"rotation":-21.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-26.65,"y":-80.1,"rotation":-21.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-23.2,"y":-70.5,"rotation":-35.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-23.2,"y":-70.5,"rotation":-35.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"head":{"nodeList":[{"x":-1.75,"y":-50.5,"rotation":0,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-22.95,"y":-60.65,"rotation":-6.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-27.95,"y":-95.65,"rotation":-6.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-35.15,"y":-90.85,"rotation":-6.46,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-34.15,"y":-78.9,"rotation":-20.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-34.15,"y":-78.9,"rotation":-20.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]}},"drop":{"name":"drop","frame":3,"footRight":{"nodeList":[{"x":10.95,"y":97.1,"rotation":-46.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":2},{"x":46.6,"y":149.9,"rotation":-76.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":1}]},"shankRight":{"nodeList":[{"x":29.65,"y":65.95,"rotation":19.69,"offR":0,"scaleX":1,"scaleY":0.71,"alpha":1,"frame":2},{"x":40.2,"y":91.05,"rotation":-10.31,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":1}]},"legRight":{"nodeList":[{"x":0.75,"y":21.15,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":8,"y":42.3,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":1}]},"footLeft":{"nodeList":[{"x":-96.55,"y":99.35,"rotation":56,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":2},{"x":-56.9,"y":148.55,"rotation":80.01,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-65.8,"y":46.75,"rotation":35.9,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":2},{"x":-47.1,"y":83.35,"rotation":14.92,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":-2.8,"y":31.15,"rotation":70.17,"offR":0,"scaleX":1,"scaleY":1.15,"alpha":1,"frame":2},{"x":-0.65,"y":53,"rotation":49.19,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":-32.15,"y":-21.1,"rotation":75.22,"offR":0,"scaleX":1,"scaleY":0.77,"alpha":1,"frame":2},{"x":-36.6,"y":-17.1,"rotation":33.43,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":-9.2,"y":-58.9,"rotation":38.11,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":2},{"x":-2.95,"y":-44.35,"rotation":56.31,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":52.1,"y":-40.3,"rotation":-11.16,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":2},{"x":58.1,"y":-30.2,"rotation":-11.16,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":8.95,"y":-54.3,"rotation":-71.45,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":2},{"x":4.75,"y":-44.9,"rotation":-71.45,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":5.7,"y":-21.9,"rotation":8.27,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":2},{"x":6.8,"y":2.45,"rotation":0.8,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":1}]},"bodyUp":{"nodeList":[{"x":-13.9,"y":-76.4,"rotation":-20.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-4.9,"y":-58.95,"rotation":-11.78,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"head":{"nodeList":[{"x":-14.35,"y":-92.3,"rotation":-5.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-11,"y":-81.4,"rotation":-5.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]}},"relax":{"name":"relax","frame":13,"footRight":{"nodeList":[{"x":51.6,"y":152,"rotation":-84.77,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":3},{"x":51.6,"y":152,"rotation":-91.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":51.6,"y":152,"rotation":-91.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":51.6,"y":152,"rotation":-91.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":1}]},"shankRight":{"nodeList":[{"x":40.2,"y":91.05,"rotation":-10.31,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":3},{"x":40.2,"y":91.05,"rotation":-10.31,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":4},{"x":40.2,"y":91.05,"rotation":-10.31,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":5},{"x":40.2,"y":91.05,"rotation":-10.31,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":1}]},"legRight":{"nodeList":[{"x":8,"y":42.3,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":3},{"x":8,"y":45.3,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":4},{"x":8,"y":42.3,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":5},{"x":8,"y":42.3,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":1}]},"footLeft":{"nodeList":[{"x":-56.9,"y":148.55,"rotation":80.01,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":3},{"x":-56.9,"y":148.55,"rotation":80.01,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":4},{"x":-56.9,"y":148.55,"rotation":80.01,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":-56.9,"y":148.55,"rotation":80.01,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-47.1,"y":83.35,"rotation":14.92,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":3},{"x":-47.1,"y":83.35,"rotation":14.92,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":4},{"x":-47.1,"y":83.35,"rotation":14.92,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":5},{"x":-47.1,"y":83.35,"rotation":14.92,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":-0.65,"y":53,"rotation":49.19,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-0.65,"y":56,"rotation":49.19,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-0.65,"y":53,"rotation":49.19,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-0.65,"y":53,"rotation":49.19,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":-34,"y":-12.2,"rotation":25.62,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":3},{"x":-18.25,"y":5,"rotation":-4.38,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":2},{"x":-28.45,"y":-2.25,"rotation":28.07,"offR":0,"scaleX":1,"scaleY":1.18,"alpha":1,"frame":2},{"x":-24.85,"y":-0.5,"rotation":-8.67,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":5},{"x":-19.25,"y":0,"rotation":19.63,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":-4.25,"y":-43.75,"rotation":48.5,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":3},{"x":-8.3,"y":-37.2,"rotation":18.5,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":2},{"x":-6.15,"y":-39.6,"rotation":35.95,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":2},{"x":-7.25,"y":-40.1,"rotation":29.21,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":5},{"x":-11.25,"y":-42.2,"rotation":18.5,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":50.3,"y":-20.8,"rotation":3.25,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":3},{"x":49.05,"y":-8.7,"rotation":10.28,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":4},{"x":48.9,"y":-10.05,"rotation":10.28,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":5},{"x":48.05,"y":-13.7,"rotation":10.28,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":2.3,"y":-48.4,"rotation":-57.03,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":3},{"x":4.75,"y":-41.95,"rotation":-50.01,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":4},{"x":4.6,"y":-43.3,"rotation":-50.01,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":5},{"x":3.75,"y":-46.95,"rotation":-50.01,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":6.8,"y":2.45,"rotation":0.8,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":3},{"x":6.8,"y":5.45,"rotation":0.8,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":4},{"x":6.8,"y":2.45,"rotation":0.8,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":5},{"x":6.8,"y":2.45,"rotation":0.8,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":1}]},"bodyUp":{"nodeList":[{"x":-4.9,"y":-58.95,"rotation":-11.78,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-4.9,"y":-55.95,"rotation":-11.78,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-5.05,"y":-57.3,"rotation":-11.78,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-5.9,"y":-60.95,"rotation":-11.78,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"head":{"nodeList":[{"x":-10,"y":-83.4,"rotation":-5.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-8.55,"y":-71.9,"rotation":-5.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-9.35,"y":-74.05,"rotation":-5.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-15.15,"y":-81.45,"rotation":-5.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]}},"soap":{"name":"soap","frame":32,"footRight":{"nodeList":[{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":3},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":7},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":3},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":1}]},"shankRight":{"nodeList":[{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":3},{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":4},{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":5},{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":7},{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":4},{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":5},{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":3},{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":1}]},"legRight":{"nodeList":[{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":3},{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":4},{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":5},{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":7},{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":4},{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":5},{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":3},{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":1}]},"footLeft":{"nodeList":[{"x":-47.65,"y":144.9,"rotation":85.5,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":3},{"x":-1.4,"y":111.25,"rotation":85.5,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":4},{"x":35.65,"y":97.75,"rotation":40.5,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":79.9,"y":58.45,"rotation":-23.45,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":7},{"x":79.9,"y":58.45,"rotation":-23.45,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":4},{"x":35.65,"y":97.75,"rotation":40.5,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":-1.4,"y":111.25,"rotation":85.5,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":3},{"x":-47.65,"y":144.9,"rotation":85.5,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-31.65,"y":80.85,"rotation":20.4,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":3},{"x":-31.6,"y":81,"rotation":-35.79,"offR":0,"scaleX":1,"scaleY":0.71,"alpha":1,"frame":4},{"x":-27.9,"y":82.85,"rotation":-69.82,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":5},{"x":24.75,"y":82.85,"rotation":-103.78,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":7},{"x":24.75,"y":82.85,"rotation":-103.78,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":4},{"x":-27.9,"y":82.85,"rotation":-69.82,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":5},{"x":-31.6,"y":81,"rotation":-35.79,"offR":0,"scaleX":1,"scaleY":0.71,"alpha":1,"frame":3},{"x":-31.65,"y":80.85,"rotation":20.4,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":-2.75,"y":36.1,"rotation":24.67,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-2.5,"y":28.45,"rotation":24.67,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":4},{"x":-3.8,"y":28.45,"rotation":20.64,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":5},{"x":1.45,"y":23.85,"rotation":-24.36,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":7},{"x":1.45,"y":23.85,"rotation":-24.36,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":4},{"x":-3.8,"y":28.45,"rotation":20.64,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":5},{"x":-2.5,"y":28.45,"rotation":24.67,"offR":0,"scaleX":1,"scaleY":0.98,"alpha":1,"frame":3},{"x":-2.75,"y":36.1,"rotation":24.67,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":-30.9,"y":-17.35,"rotation":41.21,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":3},{"x":-58.35,"y":1.8,"rotation":34.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-76.65,"y":42.45,"rotation":24.23,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-80.15,"y":85.95,"rotation":18.53,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":7},{"x":-80.15,"y":85.95,"rotation":18.53,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":4},{"x":-76.65,"y":42.45,"rotation":24.23,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-58.35,"y":1.8,"rotation":34.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-30.9,"y":-17.35,"rotation":41.21,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":-35.6,"y":-61.9,"rotation":1.11,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":3},{"x":-63.15,"y":-46,"rotation":2.61,"offR":0,"scaleX":1,"scaleY":0.88,"alpha":1,"frame":4},{"x":-90.05,"y":-3.7,"rotation":-7.89,"offR":0,"scaleX":1,"scaleY":0.88,"alpha":1,"frame":5},{"x":-91.5,"y":32.75,"rotation":-5.35,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":7},{"x":-91.5,"y":32.75,"rotation":-5.35,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":4},{"x":-90.05,"y":-3.7,"rotation":-7.89,"offR":0,"scaleX":1,"scaleY":0.88,"alpha":1,"frame":5},{"x":-63.15,"y":-46,"rotation":2.61,"offR":0,"scaleX":1,"scaleY":0.88,"alpha":1,"frame":3},{"x":-35.6,"y":-61.9,"rotation":1.11,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":28.25,"y":-51.5,"rotation":6.62,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":3},{"x":-8.9,"y":-13.85,"rotation":31.17,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":4},{"x":-29.3,"y":13.35,"rotation":16.17,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":5},{"x":-36.2,"y":50.4,"rotation":3.2,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":7},{"x":-36.2,"y":50.4,"rotation":3.2,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":4},{"x":-29.3,"y":13.35,"rotation":16.17,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":5},{"x":-8.9,"y":-13.85,"rotation":31.17,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":3},{"x":28.25,"y":-51.5,"rotation":6.62,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":-26,"y":-62.8,"rotation":-75.12,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":3},{"x":-53.55,"y":-46.6,"rotation":-50.56,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":4},{"x":-80.9,"y":-6.8,"rotation":-65.56,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":5},{"x":-87.15,"y":28.35,"rotation":-63.53,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":7},{"x":-87.15,"y":28.35,"rotation":-63.53,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":4},{"x":-80.9,"y":-6.8,"rotation":-65.56,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":5},{"x":-53.55,"y":-46.6,"rotation":-50.56,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":3},{"x":-26,"y":-62.8,"rotation":-75.12,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":-9.05,"y":-18.9,"rotation":-11.11,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":3},{"x":-22.35,"y":-11.45,"rotation":-31.55,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":4},{"x":-37.4,"y":5.85,"rotation":-61.55,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":5},{"x":-42.15,"y":22.65,"rotation":-85,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":7},{"x":-42.15,"y":22.65,"rotation":-85,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":4},{"x":-37.4,"y":5.85,"rotation":-61.55,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":5},{"x":-22.35,"y":-11.45,"rotation":-31.55,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":3},{"x":-9.05,"y":-18.9,"rotation":-11.11,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":1}]},"bodyUp":{"nodeList":[{"x":-34.5,"y":-71.55,"rotation":-29.18,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-64.55,"y":-51.9,"rotation":-49.63,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-94.2,"y":-8.1,"rotation":-79.62,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-99.85,"y":32.45,"rotation":-103.07,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":7},{"x":-99.85,"y":32.45,"rotation":-103.07,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-94.2,"y":-8.1,"rotation":-79.62,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-64.55,"y":-51.9,"rotation":-49.63,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-34.5,"y":-71.55,"rotation":-29.18,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"head":{"nodeList":[{"x":-49.05,"y":-92.85,"rotation":-23.13,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-85.7,"y":-68.7,"rotation":-43.58,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-120.25,"y":-11.4,"rotation":-73.58,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-113.95,"y":43.7,"rotation":-97.03,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":7},{"x":-113.95,"y":43.7,"rotation":-97.03,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-120.25,"y":-11.4,"rotation":-73.58,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-85.7,"y":-68.7,"rotation":-43.58,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-49.05,"y":-92.85,"rotation":-23.13,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]}},"roll":{"name":"roll","frame":50,"footRight":{"nodeList":[{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":12.7,"y":147.45,"rotation":88.53,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":0.65,"y":123.7,"rotation":-16.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":6},{"x":45.65,"y":116.55,"rotation":-16.47,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":3},{"x":25.6,"y":105,"rotation":-6,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":55.45,"y":14.85,"rotation":-46.03,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":3},{"x":-17.6,"y":-17.2,"rotation":-91.24,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":-146.95,"y":64.45,"rotation":175.85,"offR":0,"scaleX":1,"scaleY":1.18,"alpha":1,"frame":5},{"x":-110.9,"y":146.2,"rotation":90.14,"offR":0,"scaleX":1,"scaleY":1.18,"alpha":1,"frame":4},{"x":-74.9,"y":128.1,"rotation":90.14,"offR":0,"scaleX":1,"scaleY":1.18,"alpha":1,"frame":4},{"x":36.15,"y":153.75,"rotation":90.83,"offR":0,"scaleX":1,"scaleY":1.18,"alpha":1,"frame":4},{"x":50.1,"y":158.2,"rotation":41.55,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":2},{"x":47.1,"y":157.2,"rotation":41.55,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":1}]},"shankRight":{"nodeList":[{"x":1.95,"y":85.2,"rotation":-6.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":4},{"x":-39.95,"y":106.95,"rotation":-51.08,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":5},{"x":-64.95,"y":146.95,"rotation":-111.08,"offR":0,"scaleX":1,"scaleY":1.29,"alpha":1,"frame":6},{"x":-2.55,"y":93,"rotation":-61.06,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":3},{"x":-20.15,"y":71.6,"rotation":-50.59,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":5},{"x":-1.05,"y":18.7,"rotation":-90.62,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":3},{"x":-54.7,"y":25.65,"rotation":-135.83,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":4},{"x":-90.25,"y":64.4,"rotation":93.25,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":5},{"x":-83.9,"y":92.9,"rotation":29.57,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":4},{"x":-47.9,"y":74.8,"rotation":29.57,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":4},{"x":-17.85,"y":104.05,"rotation":-44.74,"offR":0,"scaleX":1,"scaleY":1.29,"alpha":1,"frame":4},{"x":-6.85,"y":134.05,"rotation":-64.02,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":2},{"x":13.9,"y":101.9,"rotation":-27.54,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":1}]},"legRight":{"nodeList":[{"x":2.45,"y":25.45,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":4},{"x":-21.45,"y":43.95,"rotation":18,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":5},{"x":-65.8,"y":80.8,"rotation":3,"offR":0,"scaleX":1,"scaleY":1.2,"alpha":1,"frame":6},{"x":-41.1,"y":52.7,"rotation":-41.99,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":3},{"x":-64.25,"y":22.7,"rotation":-41.07,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":5},{"x":-66.25,"y":9.6,"rotation":-81.1,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":3},{"x":-107.05,"y":65.5,"rotation":-126.31,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":4},{"x":-77.45,"y":128.95,"rotation":169.73,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":5},{"x":-44.4,"y":145.5,"rotation":144.03,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":4},{"x":-8.4,"y":127.4,"rotation":144.03,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":4},{"x":39.65,"y":76.95,"rotation":69.71,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":4},{"x":10.85,"y":68.45,"rotation":15.46,"offR":0,"scaleX":1,"scaleY":1.21,"alpha":1,"frame":2},{"x":-4.45,"y":39.75,"rotation":-14.53,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":1}]},"footLeft":{"nodeList":[{"x":-47.65,"y":144.9,"rotation":85.5,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":4},{"x":-84.25,"y":146.95,"rotation":85.5,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":5},{"x":-153.65,"y":158.25,"rotation":93.69,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":6},{"x":-28.25,"y":144.15,"rotation":88.91,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":3},{"x":-10.75,"y":112.9,"rotation":45.89,"offR":0,"scaleX":1,"scaleY":0.93,"alpha":1,"frame":5},{"x":39.05,"y":46.4,"rotation":-33.14,"offR":0,"scaleX":1,"scaleY":0.93,"alpha":1,"frame":3},{"x":-6.75,"y":16.7,"rotation":-78.34,"offR":0,"scaleX":1,"scaleY":0.93,"alpha":1,"frame":4},{"x":-91.75,"y":9.8,"rotation":-162.81,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":-115.2,"y":68.75,"rotation":165.79,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":-142.9,"y":109.85,"rotation":156.81,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":-78.9,"y":162.95,"rotation":88.57,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":4},{"x":-88.05,"y":174.15,"rotation":94.87,"offR":0,"scaleX":1,"scaleY":1.18,"alpha":1,"frame":2},{"x":-57.95,"y":162.85,"rotation":70.63,"offR":0,"scaleX":1,"scaleY":1.18,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-31.65,"y":80.85,"rotation":20.4,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":4},{"x":-78.9,"y":78.7,"rotation":11.65,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":5},{"x":-130.85,"y":93.7,"rotation":26.65,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":6},{"x":-73.4,"y":90.2,"rotation":-38.13,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":3},{"x":-69,"y":74.6,"rotation":-51.15,"offR":0,"scaleX":1,"scaleY":1.1,"alpha":1,"frame":5},{"x":-28.2,"y":51.1,"rotation":-85.17,"offR":0,"scaleX":1,"scaleY":1.3,"alpha":1,"frame":3},{"x":-50.85,"y":67.8,"rotation":-130.38,"offR":0,"scaleX":1,"scaleY":1.3,"alpha":1,"frame":4},{"x":-40.45,"y":68.6,"rotation":145.15,"offR":0,"scaleX":1,"scaleY":1.3,"alpha":1,"frame":5},{"x":-37.2,"y":72.1,"rotation":98.75,"offR":0,"scaleX":1,"scaleY":1.3,"alpha":1,"frame":4},{"x":-65.3,"y":101,"rotation":89.77,"offR":0,"scaleX":1,"scaleY":1.3,"alpha":1,"frame":4},{"x":-23.4,"y":107.9,"rotation":51.53,"offR":0,"scaleX":1,"scaleY":1.3,"alpha":1,"frame":4},{"x":-58.75,"y":98.8,"rotation":27.84,"offR":0,"scaleX":1,"scaleY":1.44,"alpha":1,"frame":2},{"x":-62.15,"y":87,"rotation":3.59,"offR":0,"scaleX":1,"scaleY":1.28,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":-2.75,"y":36.1,"rotation":24.67,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-27.45,"y":51.9,"rotation":54.67,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":5},{"x":-71.15,"y":86.7,"rotation":79.13,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":6},{"x":-41.1,"y":59.8,"rotation":41.61,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":3},{"x":-66.65,"y":24.85,"rotation":0.86,"offR":0,"scaleX":1,"scaleY":1.06,"alpha":1,"frame":5},{"x":-66.75,"y":12.8,"rotation":-48.17,"offR":0,"scaleX":1,"scaleY":1.06,"alpha":1,"frame":3},{"x":-105.1,"y":68.1,"rotation":-93.37,"offR":0,"scaleX":1,"scaleY":1.06,"alpha":1,"frame":4},{"x":-73.5,"y":127.8,"rotation":-153.1,"offR":0,"scaleX":1,"scaleY":1.35,"alpha":1,"frame":5},{"x":-41.35,"y":142.75,"rotation":-178.81,"offR":0,"scaleX":1,"scaleY":1.35,"alpha":1,"frame":4},{"x":-5.35,"y":123.65,"rotation":106.25,"offR":0,"scaleX":1,"scaleY":1.35,"alpha":1,"frame":4},{"x":33.75,"y":77.85,"rotation":59.8,"offR":0,"scaleX":1,"scaleY":1.35,"alpha":1,"frame":4},{"x":1.05,"y":70.05,"rotation":59.8,"offR":0,"scaleX":1,"scaleY":1.35,"alpha":1,"frame":2},{"x":-13.85,"y":43.95,"rotation":44.8,"offR":0,"scaleX":1,"scaleY":1.25,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":-30.9,"y":-17.35,"rotation":41.21,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":4},{"x":-85.35,"y":27.05,"rotation":41.21,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":5},{"x":-160.35,"y":74.05,"rotation":14.21,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":6},{"x":-138.2,"y":95.35,"rotation":41.51,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":3},{"x":-143.8,"y":81,"rotation":-3.48,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":5},{"x":-77.85,"y":79.2,"rotation":-73.7,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":3},{"x":-48.9,"y":105.5,"rotation":-138.65,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":4},{"x":16.25,"y":100.95,"rotation":-138.42,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":5},{"x":21.85,"y":65.9,"rotation":170.24,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":4},{"x":2.75,"y":55.95,"rotation":114.79,"offR":0,"scaleX":1,"scaleY":0.94,"alpha":1,"frame":4},{"x":-30.8,"y":10.85,"rotation":99.79,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":4},{"x":-24.7,"y":21.3,"rotation":84.91,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":2},{"x":-37.75,"y":-2,"rotation":55.66,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":-35.6,"y":-61.9,"rotation":1.11,"offR":0,"scaleX":1,"scaleY":0.74,"alpha":1,"frame":4},{"x":-125.3,"y":-11.4,"rotation":-43.89,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-176.75,"y":26.8,"rotation":-11.66,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":6},{"x":-168.25,"y":55.35,"rotation":-29.36,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":3},{"x":-193.3,"y":73.95,"rotation":-74.36,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-109.9,"y":119.75,"rotation":-135.29,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":3},{"x":-25.75,"y":151.7,"rotation":159.77,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":4},{"x":59.25,"y":129.55,"rotation":130,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":72.5,"y":86.1,"rotation":115.09,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":4},{"x":54.4,"y":38.35,"rotation":74.65,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":4},{"x":20.85,"y":-6.75,"rotation":74.65,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":4},{"x":-13.85,"y":-39.15,"rotation":14.76,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":2},{"x":-22.55,"y":-57.75,"rotation":24.2,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":28.25,"y":-51.5,"rotation":6.62,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":4},{"x":-58.75,"y":0.25,"rotation":6.62,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":5},{"x":-132.5,"y":73.8,"rotation":91.67,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":6},{"x":-119.35,"y":92.3,"rotation":75.69,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":3},{"x":-132.6,"y":65.55,"rotation":24.68,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5},{"x":-54.4,"y":96,"rotation":-33.08,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":3},{"x":-23.8,"y":91.4,"rotation":-98.02,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":4},{"x":1.55,"y":111.75,"rotation":163.24,"offR":0,"scaleX":1,"scaleY":1.02,"alpha":1,"frame":5},{"x":44.85,"y":140.9,"rotation":118.33,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":4},{"x":102.8,"y":75,"rotation":2.89,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":4},{"x":79.8,"y":23,"rotation":-12.11,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":4},{"x":41.85,"y":-8.45,"rotation":-19.15,"offR":0,"scaleX":1,"scaleY":1.07,"alpha":1,"frame":2},{"x":40.75,"y":-15.6,"rotation":-0.99,"offR":0,"scaleX":1,"scaleY":1.06,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":-26,"y":-62.8,"rotation":-75.12,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":4},{"x":-119.15,"y":-12.75,"rotation":-75.12,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":5},{"x":-175.1,"y":23.55,"rotation":-42.37,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":6},{"x":-167.65,"y":51.8,"rotation":-50.37,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":3},{"x":-195.4,"y":70.9,"rotation":-95.37,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":5},{"x":-112.2,"y":121.35,"rotation":-114.14,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":3},{"x":-25.25,"y":154.5,"rotation":-179.09,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":4},{"x":62,"y":129.8,"rotation":106.14,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":5},{"x":78.95,"y":88.35,"rotation":31.24,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":4},{"x":53.1,"y":36.9,"rotation":-54.2,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":4},{"x":30.1,"y":-15.1,"rotation":-54.2,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":4},{"x":-12.7,"y":-35.6,"rotation":-61.25,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":2},{"x":-11.5,"y":-52,"rotation":-51.81,"offR":0,"scaleX":1,"scaleY":1.28,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":-9.05,"y":-18.9,"rotation":-11.11,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":4},{"x":-59,"y":1.6,"rotation":-49.38,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":5},{"x":-108.8,"y":43.35,"rotation":-49.38,"offR":0,"scaleX":1,"scaleY":0.99,"alpha":1,"frame":6},{"x":-107.55,"y":44.4,"rotation":-82.08,"offR":0,"scaleX":1,"scaleY":1.21,"alpha":1,"frame":3},{"x":-132.55,"y":37.3,"rotation":-103.29,"offR":0,"scaleX":1,"scaleY":1.21,"alpha":1,"frame":5},{"x":-124.3,"y":48.3,"rotation":-128.5,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":3},{"x":-96.6,"y":134.55,"rotation":166.55,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":4},{"x":-10.85,"y":149.85,"rotation":102.59,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":5},{"x":24.7,"y":135.4,"rotation":76.88,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":4},{"x":53.1,"y":103.85,"rotation":66.44,"offR":0,"scaleX":1,"scaleY":1.04,"alpha":1,"frame":4},{"x":38.25,"y":30.35,"rotation":-3.75,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":4},{"x":6.6,"y":20.1,"rotation":-3.75,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":2},{"x":-9.55,"y":-4.9,"rotation":-3.75,"offR":0,"scaleX":1,"scaleY":0.89,"alpha":1,"frame":1}]},"bodyUp":{"nodeList":[{"x":-34.5,"y":-71.55,"rotation":-29.18,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-112.4,"y":-10.25,"rotation":-74.18,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-162.2,"y":31.5,"rotation":-74.18,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":6},{"x":-168.25,"y":55.35,"rotation":-98.63,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":3},{"x":-188,"y":65.65,"rotation":-115.09,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":5},{"x":-115.3,"y":113.75,"rotation":174.7,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":3},{"x":-33.5,"y":154.05,"rotation":109.75,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":4},{"x":53.65,"y":135.4,"rotation":79.98,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":5},{"x":70.65,"y":93.95,"rotation":50.08,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":4},{"x":58.15,"y":45.55,"rotation":9.63,"offR":0,"scaleX":1,"scaleY":1.13,"alpha":1,"frame":4},{"x":16.7,"y":-18.05,"rotation":-20.37,"offR":0,"scaleX":1,"scaleY":1.06,"alpha":1,"frame":4},{"x":-13.05,"y":-34.4,"rotation":-20.37,"offR":0,"scaleX":1,"scaleY":1.06,"alpha":1,"frame":2},{"x":-18.8,"y":-52.9,"rotation":-10.93,"offR":0,"scaleX":1,"scaleY":0.92,"alpha":1,"frame":1}]},"head":{"nodeList":[{"x":-49.05,"y":-92.85,"rotation":-23.13,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-144.95,"y":-26.45,"rotation":-53.13,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-208.3,"y":18.85,"rotation":-122.1,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":6},{"x":-198.15,"y":65.65,"rotation":-154.8,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-200.3,"y":102.4,"rotation":145.21,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-93,"y":134.55,"rotation":74.99,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-5.2,"y":142.7,"rotation":10.05,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":72.55,"y":111.45,"rotation":-19.72,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":75.05,"y":63.75,"rotation":-49.63,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":50.85,"y":15.7,"rotation":-90.07,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":12.65,"y":-51.3,"rotation":-90.07,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":4},{"x":-30.35,"y":-60.3,"rotation":-120.07,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":2},{"x":-25.6,"y":-86.55,"rotation":-95.63,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]}},"comeon":{"name":"comeon","frame":35,"footRight":{"nodeList":[{"x":51.6,"y":152,"rotation":-84.77,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":3},{"x":51.6,"y":152,"rotation":-84.77,"offR":0,"scaleX":1,"scaleY":1.08,"alpha":1,"frame":5},{"x":47.7,"y":152,"rotation":-84.77,"offR":0,"scaleX":1,"scaleY":1.2,"alpha":1,"frame":6},{"x":47.7,"y":152,"rotation":-84.77,"offR":0,"scaleX":1,"scaleY":1.2,"alpha":1,"frame":20},{"x":47.7,"y":152,"rotation":-84.77,"offR":0,"scaleX":1,"scaleY":1.2,"alpha":1,"frame":1}]},"shankRight":{"nodeList":[{"x":40.2,"y":91.05,"rotation":-10.31,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":3},{"x":24.75,"y":96.9,"rotation":-30.01,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":5},{"x":5.65,"y":106.85,"rotation":-45,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":6},{"x":5.65,"y":106.85,"rotation":-45,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":20},{"x":5.65,"y":106.85,"rotation":-45,"offR":0,"scaleX":1,"scaleY":1.19,"alpha":1,"frame":1}]},"legRight":{"nodeList":[{"x":8,"y":42.3,"rotation":-34.5,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":3},{"x":-22,"y":61.85,"rotation":-54.2,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":5},{"x":-41.05,"y":71.8,"rotation":-54.2,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":6},{"x":-41.05,"y":71.8,"rotation":-54.2,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":20},{"x":-41.05,"y":71.8,"rotation":-54.2,"offR":0,"scaleX":1,"scaleY":0.97,"alpha":1,"frame":1}]},"footLeft":{"nodeList":[{"x":-56.9,"y":148.55,"rotation":80.01,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":3},{"x":-110.15,"y":116.35,"rotation":104.72,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":5},{"x":-103.8,"y":144.65,"rotation":78.25,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":6},{"x":-103.8,"y":144.65,"rotation":78.25,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":20},{"x":-103.8,"y":144.65,"rotation":78.25,"offR":0,"scaleX":1,"scaleY":1.03,"alpha":1,"frame":1}]},"shankLeft":{"nodeList":[{"x":-47.1,"y":83.35,"rotation":14.92,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":3},{"x":-74,"y":61.2,"rotation":39.62,"offR":0,"scaleX":1,"scaleY":1.22,"alpha":1,"frame":5},{"x":-95.4,"y":93.15,"rotation":13.16,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":6},{"x":-95.4,"y":93.15,"rotation":13.16,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":20},{"x":-95.4,"y":93.15,"rotation":13.16,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"legLeft":{"nodeList":[{"x":-0.65,"y":53,"rotation":49.19,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-18.9,"y":67.5,"rotation":88.89,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-48.65,"y":77.55,"rotation":66.67,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":6},{"x":-48.65,"y":77.55,"rotation":66.67,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":20},{"x":-48.65,"y":77.55,"rotation":66.67,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"handRight":{"nodeList":[{"x":-25.1,"y":-21.65,"rotation":-64.38,"offR":0,"scaleX":1,"scaleY":1.24,"alpha":1,"frame":3},{"x":-55.9,"y":-4.7,"rotation":29.9,"offR":0,"scaleX":1,"scaleY":0.91,"alpha":1,"frame":5},{"x":-117.35,"y":-19.3,"rotation":134.1,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":6},{"x":-117.35,"y":-19.3,"rotation":134.1,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":20},{"x":-117.35,"y":-19.3,"rotation":134.1,"offR":0,"scaleX":1,"scaleY":1.05,"alpha":1,"frame":1}]},"armRight":{"nodeList":[{"x":-1.7,"y":-57.1,"rotation":33.04,"offR":0,"scaleX":1,"scaleY":0.8,"alpha":1,"frame":3},{"x":-46.35,"y":-43.85,"rotation":22.32,"offR":0,"scaleX":1,"scaleY":0.8,"alpha":1,"frame":5},{"x":-66.5,"y":-17.5,"rotation":98.27,"offR":0,"scaleX":1,"scaleY":0.9,"alpha":1,"frame":6},{"x":-66.5,"y":-17.5,"rotation":98.27,"offR":0,"scaleX":1,"scaleY":0.9,"alpha":1,"frame":20},{"x":-66.5,"y":-17.5,"rotation":98.27,"offR":0,"scaleX":1,"scaleY":0.9,"alpha":1,"frame":1}]},"handLeft":{"nodeList":[{"x":21.1,"y":-14.15,"rotation":61.12,"offR":0,"scaleX":1,"scaleY":1.25,"alpha":1,"frame":3},{"x":-2.2,"y":-20.2,"rotation":8.43,"offR":0,"scaleX":1,"scaleY":0.88,"alpha":1,"frame":5},{"x":-2.9,"y":10.5,"rotation":-86.84,"offR":0,"scaleX":1,"scaleY":1.38,"alpha":1,"frame":6},{"x":-2.9,"y":10.5,"rotation":-86.84,"offR":0,"scaleX":1,"scaleY":1.38,"alpha":1,"frame":6},{"x":-2.95,"y":10.5,"rotation":-101.84,"offR":0,"scaleX":1,"scaleY":1.38,"alpha":1,"frame":6},{"x":-2.9,"y":10.5,"rotation":-86.84,"offR":0,"scaleX":1,"scaleY":1.38,"alpha":1,"frame":8},{"x":-2.95,"y":10.5,"rotation":-101.84,"offR":0,"scaleX":1,"scaleY":1.38,"alpha":1,"frame":1}]},"armLeft":{"nodeList":[{"x":-0.1,"y":-56.1,"rotation":-29.17,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":3},{"x":-43.65,"y":-45.75,"rotation":-59.61,"offR":0,"scaleX":1,"scaleY":0.87,"alpha":1,"frame":5},{"x":-56.9,"y":-15.6,"rotation":-64.63,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":6},{"x":-56.9,"y":-15.6,"rotation":-64.63,"offR":0,"scaleX":1,"scaleY":1.09,"alpha":1,"frame":20},{"x":-58.2,"y":-15.6,"rotation":-64.63,"offR":0,"scaleX":1,"scaleY":1.12,"alpha":1,"frame":1}]},"bodyDown":{"nodeList":[{"x":6.8,"y":2.45,"rotation":0.8,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":3},{"x":-30.15,"y":7.45,"rotation":-9.91,"offR":0,"scaleX":1,"scaleY":0.85,"alpha":1,"frame":5},{"x":-57.95,"y":20.05,"rotation":-19.43,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":6},{"x":-56.9,"y":21.1,"rotation":-19.43,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":20},{"x":-57.95,"y":20.05,"rotation":-19.43,"offR":0,"scaleX":1,"scaleY":0.95,"alpha":1,"frame":1}]},"bodyUp":{"nodeList":[{"x":-4.9,"y":-58.95,"rotation":-11.78,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-49.8,"y":-45.15,"rotation":-22.5,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-67.45,"y":-31.2,"rotation":-11.05,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":6},{"x":-67.45,"y":-31.2,"rotation":-11.05,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":20},{"x":-67.45,"y":-31.2,"rotation":-11.05,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]},"head":{"nodeList":[{"x":-10,"y":-83.4,"rotation":-5.73,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":3},{"x":-59.35,"y":-68.2,"rotation":-16.45,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":5},{"x":-63.75,"y":-57.7,"rotation":25,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":6},{"x":-63.75,"y":-57.7,"rotation":25,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":20},{"x":-63.75,"y":-57.7,"rotation":25,"offR":0,"scaleX":1,"scaleY":1,"alpha":1,"frame":1}]}}}}
);
window.textureImgBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAHsUlEQVR42u2dXWwUVRTHb1qwhdKiNaLttqUG0BgTE4vxIzyTKLGg+FbExMRE39BnEtNEE7VYDMY3W3hC0ARM8c2YWG2VSj+k0Wjsgwn9AFMQBcpLoa3n7p7B2duZ3ZnduffO7Px/yT/ZtLP33Jl79n6c+zFCmGELqYv0AelL0s+kOdICaYW1wH87Txrga7v4u3FkDamGVEeqJzXw5xr+X5UGm1UedusN2A1NNWkn6VPSrKuQS9Usp7WT07Z5X+tITaTtpD2k10gHWPLzbtITpGa+tjpCu82ctpfdPZynKO2GppV/uTMRFHohZ5A22gzfWy2pnQv4XdIp0k+kP/h+p0lT/LfTfM1u/k5tGb9M1e5ptjHFdmf48wjn6R3FrhG2kvpJixoLXtUi29yq+d5kwW0k7eCHO0S6QloqkLclvmaYv7OD06gqw+5wALvLEdgNhWx/DpFuGyx4VdJ2L+dFR+E3kjpJn5EulZC/v0jHOY3GgIXhtnuc01gOYXO5RLuh2BtR+x5l0/BSxPe4kR/gGdL1MvJ2nTu2nZxmkF9+J3/HlN3ArOeqdyWm6uc8RtHmyyr0hDJaKVU3SCc5zdoAdk8athuIzTxMW4m5znNey6n6ZSfqPdJ8hPm6zGm2+/TS3XavRGh33mW35Kbg8RLbQFu6xHkuBTmMepE0riFf45z2ekt215XyQJ4k/Zugwnck8/x0Cfcrx9IfRVQFq1rgtDMev36ddm9y2s1ha4HHSNcSWPiOroWsCarZab7XmCeZ9jNKM7DGgN3vSE+xrcCBndkEF757hNAa8J5lSPVlzfc9yzZqFLv7DQTR9it2falLSIcvTMewLsB9byB1c5WpKy832cYGJabSran6dzc/3UFjJv0VVPiOjga470a+Tmdw6zY/30YPu7cM2/UN8qxUqPYWuff7OfCzrDEPy2xjk2G7K2zjgWLh3bkKdoC5IlWg7J0PGiiIb5WRQIY7abrvf5BHAr58WMGF76i3yBDwrIE8nFUKImPI7o8eQ9A7bLM8sWNyAmlbgZHPqIE8jCkjE1N2zxVygP4UFL57zsCvCRgz5AAtlhzP0wHaDM/n29aiz6ISkw7grgFaNIWAVY36xUR6UlT4jno8nkOToV/iuEcn0JTdVZ3AuyKe9UqK5vnehYWCGFMKosVQzTPq1QQ8l8LCd/SsRQdQ+wBjtvoAfSl2gD7lWbRYqgFaDdYAq/oA0yl2gGmPOMC4obbY1ijAbTe78WIl5dqiOICN4VjGUuczu/sm7Q7QZWEYOO4xDLQRgErl8E/V+4oDjFuoAUx2AvOagAE4QPYZ2OiM2egDjKoBsAk4QHazqu1hoLQ7YsDuCAe77nABDpBdQWyjD5BROp+DhqaD8xzgbzhAdvOEuyBszAXIBSFfaba5zDbcC1FSNQFUSKabALUPYGspGhyAn4Ht4Vi9pcWoaAJIVy0EgtQ+gLMcXeey8BmvZeHoBOaegelhoBoIcjaGDGm0OcQbQ6oxDMzXhIVpWXUuQPfWMGdL2qq1AAgE5QeCbIWCJbo3h74gPDaHHoIDZM8cMu0AE8J7g2i70LstfdXGUEwG2ZkMWhWTZ9wHRNyIKMZxQhQ4IALTwfGYDnbXArqOiPHdFj6T4sKfsbQqeNSnBnCcwH1IVLmHU90jipwJgCVh5kPBhRzAXRMYOSZuV4odYJclBxhTJ2V88DooUs7oTXH84oLwPqByswhxKBSWhZvfoDEhgh9a4T6iNuhRsaEPg0rjcNBrY4ipTmAYBxBKxNA5LLpB5B9SXS3KOAUMW8PMh4JbRMzA5lCzfYBM3BwA28PNLgmLnQNIelPgAIcL3L+tFUGxIe1HxMQlDmCVSj4kqtiJ4iang1tFjDlWgYV/LMB9xy0QZA05tpysoMKfFMEOijQ5DGwTMedBnktOeuFf5nCqiFENEOs+gJsOkfzDojtC3K+tJWGxRi4kvJrAwpd5DntcfOrjAH48mrB1AzOc57C0wAH8kVOMSThBfDJEm2+zBmgVCUTOMX8S48I/GrC370cTOoHBg0VxihjOiWheG2dqOvhc0h3ACRv3CvsvjjwsontxpHSAYQP5/iHugaCws4i2Xh37UMT3Ih3gawP5/0YUObY9icjIltxkMau5qtf58uj7SF8I/S+M+Fwo+/QrCef18X0iutfH9wkzr4+/m3REc20m0/6YbaUCufFin8itwRvgYdpF8f8mSPmenH9If/L/BvjafSJ/04YJ5ILKA5wfXQ4g035LRPOKWxAxa0nPk37X6AC/idxy9LV43PFDrqp9mHRKYz9Apv2IgeYMlIjcTfOm0POu5Iuc9kY85vgi197LGcQzGnr/8pSu7aj+44/ccPEK6ZcIHeBXTrMBjzcZfQE5MXQwwqHsQU6zCo83OU1BO+ltUd47FaY5jXYR4s3dID5OICdtXhe5WcKlEAW/xN95Q+SmflH4CW4OnL36PRwjKPSS51t8TY8IsUcfJCNIdC+PEF4VuZCxHCnI8/eGuYd/hP/Xwdeit1+ByACOPFdATkFv4o5dhj/LHn4NfvEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU/gN1JuIXJFLiZQAAAABJRU5ErkJggg==';
function alloyInit(){alloyLoaded();}
