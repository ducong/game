;
function fallDown(){
	var ctx = window.bufferCanvas.getContext('2d');
	var cw = window.bufferCanvas.width;
	var ch = window.bufferCanvas.height;
	var curRotationAngle = 1;
	window.fallDownIng = true;
	function animation(){
		ctx.save();
		ctx.clearRect(0,0,cw,ch);
		ctx.translate(window.stickBottomX,window.stickBottomY);
		ctx.rotate(curRotationAngle*Math.PI/180);
		ctx.fillRect(0,0,-window.stickWidth,-window.curStickLength);
		ctx.restore();
		draw();
		if(curRotationAngle<90){
			curRotationAngle = Math.min(90,curRotationAngle+4);
			window.RAF(animation);
		}
		else{
			window.fallDownIng = false;
		}
	}
	animation();
}
function growStart(){
	if(window.growIng||window.stopGrow){return;}
	window.growIng = true;
	var ctx = window.bufferCanvas.getContext('2d');
	ctx.clearRect(0,0,window.bufferCanvas.width,window.bufferCanvas.height);
	ctx.fillStyle = '#000';
	window.stickBottomX = Math.floor(window.bufferCanvas.width*0.3);
	window.stickBottomY = Math.floor(window.bufferCanvas.height*0.618);
	window.curStickLength = 1;
	function animation(){
		ctx.save();
		ctx.translate(window.stickBottomX,window.stickBottomY);
		ctx.fillRect(0,0,-window.stickWidth,-window.curStickLength);
		ctx.restore();
		draw();
		if(!window.stopGrow){
			window.curStickLength += groWDLen;
			window.RAF(animation);
		}
		else{
			window.growIng = false;
			fallDown();
		}
	}
	animation();
}
function draw(){
	var ctx = window.outputCanvas.getContext('2d');
	ctx.clearRect(0,0,window.outputCanvas.width,window.outputCanvas.height);
	ctx.drawImage(window.bufferCanvas,0,0);
}
function myResize(){
	window.outputCanvas.width = window.myWidth;
	window.outputCanvas.height = window.myHeight;
	window.bufferCanvas.height = window.myHeight;
	window.shortSide = Math.min(window.myWidth,window.myHeight);
	window.groWDLen = Math.floor(window.shortSide/120);
	window.stickWidth = Math.floor(window.shortSide/90);;
}
function generateMap(){
	window.mainCanvas = $('#mainCanvas')[0];
	window.bufferCanvas = document.createElement('canvas');
	bufferCanvas.height = window.myHeight;
	bufferCanvas.width = 20*window.myWidth;
}
function gameInit(){
	$('#myScript').remove();
	window.stopGrow = false;
	window.fallDownIng = false;
	window.growIng = false;
	window.touchNum = 0;
	$(window).resize(myResize);
	$(document.body).on('touchmove',function(e){
		e.preventDefault();
	}).on('touchstart',function(e){
		window.touchNum++;
		if(window.touchNum>1||window.fallDownIng||window.growIng){return;}
		window.stopGrow = false;
		growStart();
	}).on('touchend',function(e){
		window.touchNum = Math.max(0,window.touchNum-1);
		if(window.touchNum>0||window.fallDownIng||!window.growIng){return;}
		window.stopGrow = true;
	});
	$(window).resize();
	generateMap();
}