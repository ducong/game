/*447101029@qq.com*/
//标记旗帜对map的变更是个问题
//dfs的时候标记已搜索的区域也是个问题
window.mapRowNum = 8;//地图行数
window.mapColNum = 8;//地图列数
window.gridSize = 0;//单个格子的尺寸
window.map = null;//地雷地图,有三种状态(空地,地雷,宝藏)
window.bombFlag = 1;//地雷标记
window.floorFlag = 2;//平地标记
window.treasureFlag = 3;//宝藏标记
window.flagMap = null;//用户状态地图,有种状态(已抵达,红旗标记,未抵达)
window.visitedFlag = 4;//已抵达标记
window.bombMarkFlag = 5;//红旗标记
window.unknownFlag = 6;//未抵达标记
window.safeFlag = 7;//已点开确认安全的标记
window.canTouchMap = [];
window.canTouchFlag = 7;
window.canNotTouchFlag = 8;
window.traveled = 100;//递归搜索时标记已经搜索过的点
window.mapContainer = null;//地图Container
window.curBombNum = 0;//标记当前地图中共有多少个地雷
window.gridsArray = [];//存储所有点对应的Dom
window.curPosX = 0;//当前点击选中的坐标
window.curPosY = 0;
window.curLevel = 0;//当前关卡
window.curGold = 10;//当前关卡筹码
window.ownGold = 100;//已有财富
window.treasurePos = {
	x:0,
	y:0
};//宝藏所在坐标
function readLocalData(){
	window.curLevel = parseInt(getLocalData('curLevel'));
	if(!window.curLevel||window.curLevel==''){
		window.curLevel = 0;
	}
	window.ownGold = parseInt(getLocalData('ownGold'));
	if(!window.ownGold||window.ownGold==''){
		window.ownGold = 100;
	}
}
function checkPos(x,y){
	return (x>=0&&x<window.mapRowNum&&y>=0&&y<window.mapColNum);
}
function markPos(tx,ty,okArray){
	if(checkPos(tx,ty)){
		window.canTouchMap[tx][ty] = window.canTouchFlag;
		if(window.map[tx][ty]==window.bombFlag){
			if(window.flagMap[tx][ty]==window.unknownFlag){
				window.flagMap[tx][ty] = window.visitedFlag;
				$('#grid'+tx+'r'+ty+'c .flagImg').attr('src',window.circleImg).show();
			}
			return 1;
		}
		else if(window.map[tx][ty]==window.floorFlag){
			if(window.flagMap[tx][ty]==window.unknownFlag){
				window.flagMap[tx][ty] = window.visitedFlag;
				$('#grid'+tx+'r'+ty+'c .flagImg').attr('src',window.circleImg).show();
			}
			if(window.flagMap[tx][ty]!=window.safeFlag){
				okArray.push({
					x:tx,
					y:ty
				});
			}
		}
	}
	return 0;
}
function dfsExplore(gx,gy){
	if(!checkPos(gx,gy)){return;}
	var bombNum = 0;
	var okArray = [];
	window.flagMap[gx][gy] = window.safeFlag;
	bombNum += markPos(gx-1,gy,okArray);
	bombNum += markPos(gx+1,gy,okArray);
	bombNum += markPos(gx,gy-1,okArray);
	bombNum += markPos(gx,gy+1,okArray);
	bombNum += markPos(gx-1,gy+1,okArray);
	bombNum += markPos(gx-1,gy-1,okArray);
	bombNum += markPos(gx+1,gy-1,okArray);
	bombNum += markPos(gx+1,gy+1,okArray);
	$('#grid'+gx+'r'+gy+'c .gridBgImg').attr('src','').hide();
	if(bombNum!=0){
		$('#grid'+gx+'r'+gy+'c .flagImg').attr('src',window['number'+bombNum+'Img']).show();
	}
	else{
		$('#grid'+gx+'r'+gy+'c .flagImg').attr('src','').hide();
		var okNum = okArray.length;
		for(var i = 0; i < okNum; i++){
			dfsExplore(okArray[i].x,okArray[i].y);
		}
	}
}
function heroBirth(){
	var gx = -1;
	var gy = -1;
	var maxLen = 0;
	for(var i = 0; i < 30; i++){
		var x = Math.floor(Math.random()*window.mapRowNum);
		var y = Math.floor(Math.random()*window.mapColNum);
		if(window.map[x][y] === window.floorFlag){
			var tmpLen = Math.pow(x-window.treasurePos.x,2)+Math.pow(y-window.treasurePos.y,2);
			if(tmpLen>maxLen){
				gx = x;
				gy = y;
				maxLen = tmpLen;
			}
		}
	}
	if(gx<0){
		heroBirth();
	}
	else{
		dfsExplore(gx,gy);
		if(window.canTouchMap[window.treasurePos.x][window.treasurePos.y] == window.canTouchFlag){
			generateMap();
			gameStart();
		}
	}
}
function gameStart(){
	window.curGold = 10*Math.pow(2,window.curLevel);
	hideCmdArea();
	$('#topBar').html('已有财富'+window.ownGold+'金,本局筹码'+window.curGold+'金');
	window.canTouchArea = [];
	heroBirth();
}
function userLose(){
	alert('你输了!');
	window.curLevel = Math.max(0,window.curLevel-1);
	window.ownGold = Math.max(1,window.ownGold-window.curGold/2);
	setLocalData('curLevel',window.curLevel);
	setLocalData('ownGold',window.ownGold);
	generateMap();
	gameStart();
}
function userWin(str){
	alert(str);
	window.curLevel++;
	window.ownGold += window.curGold;
	setLocalData('curLevel',window.curLevel);
	setLocalData('ownGold',window.ownGold);
	generateMap();
	gameStart();
}
function initMap(){
	window.mapContainer = document.getElementById('mapContainer');
	for(var i = 0; i < window.mapRowNum; i++){
		for(var j = 0; j < window.mapColNum; j++){
			var tmpGrid = document.createElement('div');
			tmpGrid.className = 'grid';
			var tmpBg = document.createElement('img');
			tmpBg.className = 'gridBgImg';
			tmpGrid.appendChild(tmpBg);
			var tmpFlag = document.createElement('img');
			tmpFlag.className = 'flagImg';
			tmpGrid.appendChild(tmpFlag);
			tmpGrid.id = 'grid'+i+'r'+j+'c';
			//$(tmpGrid).on('mousedown',gridOnclick);
			$(tmpGrid).on('click',showCmdArea);
			window.mapContainer.appendChild(tmpGrid);
			window.gridsArray.push(tmpGrid);
		}
	}
	window.map = [];
	window.flagMap = [];
	window.canTouchMap = [];
	for(var i = 0; i < window.mapRowNum; i++){
		var tmpMapArray = [];
		var tmpFlagArray = [];
		var tmpTouchArray = [];
		for(var j = 0; j < window.mapColNum; j++){
			tmpMapArray.push(window.floorFlag);//地板
			tmpFlagArray.push(window.unknownFlag);//未知区域
			tmpTouchArray.push(window.canNotTouchFlag);//不可达
		}
		window.map.push(tmpMapArray);
		window.flagMap.push(tmpFlagArray);
		window.canTouchMap.push(tmpTouchArray);
	}
}
function hideCmdArea(){
	if(checkPos(window.curPosX,window.curPosY)&&window.canTouchMap[window.curPosX][window.curPosY] == window.canTouchFlag){
		switch(window.flagMap[window.curPosX][window.curPosY]){
			case window.unknownFlag:
			case window.visitedFlag:
			$('#grid'+window.curPosX+'r'+window.curPosY+'c .flagImg').attr('src',window.circleImg);
			break;
			case window.bombMarkFlag:
			$('#grid'+window.curPosX+'r'+window.curPosY+'c .flagImg').attr('src',window.bombMarkImg);
			break;
		}
	}
	$('#cmdArea').hide();
}
function showCmdArea(e){
	var tmpArray = this.id.slice(4).split('r');
	var x = parseInt(tmpArray[0]);
	var y = parseInt(tmpArray[1]);
	hideCmdArea();
	if(window.canTouchMap[x][y] != window.canTouchFlag||window.flagMap[x][y]==window.safeFlag){
		return false;
	}
	window.curPosX = x;
	window.curPosY = y;
	$('#grid'+x+'r'+y+'c .flagImg').attr('src',window.markCircleImg);
	var dx = window.mapRowNum-x;
	var dy = y-0.5;
	if(window.myHeight<window.myWidth&&dx>=window.mapRowNum-1){
		dx--;
	}
	if(y==0){
		dy=0;
	}
	if(y==window.mapColNum-1){
		dy-=0.5;
	}
	$('#cmdArea').css('left',dy*window.gridSize)
	.css('bottom',dx*window.gridSize).show();
	return false;
}
function generateMap(){
	for(var i = 0; i < window.mapRowNum; i++){
		for(var j = 0; j < window.mapColNum; j++){
			window.map[i][j] = window.floorFlag;
			window.flagMap[i][j] = window.unknownFlag;
			window.canTouchMap[i][j] = window.canNotTouchFlag;
		}
	}
	var gap = Math.max(1,(window.curLevel+1));
	gap = Math.min(gap,window.mapColNum-3);
	window.curBombNum = 1*window.mapRowNum+Math.floor(Math.random()*window.mapRowNum*gap);
	for(var i = 0; i < window.curBombNum; i++){
		while(1){
			var rx = Math.floor(Math.random()*window.mapRowNum);
			var ry = Math.floor(Math.random()*window.mapColNum);
			if(window.map[rx][ry] != window.bombFlag){
				break;
			}
		}
		window.map[rx][ry] = window.bombFlag;
	}
	var times = 0;
	while(1){
		if(times>10){break;}
		var x = Math.floor(Math.random()*window.mapRowNum);
		var y = Math.floor(Math.random()*window.mapColNum);
		if(window.map[x][y] != window.bombFlag){
			break;
		}
		times++;
	}
	if(times>10){//地图过于奇葩(可能是地雷过多),重新生成
		generateMap();
		return;
	}
	if(canSetTreasure(window.map,x,y)){
		window.treasurePos.x = x;
		window.treasurePos.y = y;
		window.map[x][y] = window.treasureFlag;
	}
	else{//地图过于奇葩(地雷将地图隔断成了两块),重新生成
		generateMap();
		return;
	}
	for(var i = 0; i < window.mapRowNum; i++){
		for(var j = 0; j < window.mapColNum; j++){
			if(window.map[i][j] == window.treasureFlag){
				$('#grid'+i+'r'+j+'c .gridBgImg').attr('src','').hide();
				$('#grid'+i+'r'+j+'c .flagImg').attr('src',window.treasureImg).show();
			}
			else{
				$('#grid'+i+'r'+j+'c .gridBgImg').attr('src',window.floorImg).show();
				$('#grid'+i+'r'+j+'c .flagImg').attr('src','').hide();
			}
		}
	}
}
function canReach(map,x,y){
	if(x<0||x>=window.mapRowNum||y<0||y>=window.mapColNum){
		return false;
	}
	return (map[x][y] == window.floorFlag);
}
function travel(map,x,y){//返回map中点x,y可达的点的个数
	map[x][y] = window.traveled;
	var num = 1;
	if(canReach(map,x-1,y)){
		num += travel(map,x-1,y);
	}
	if(canReach(map,x+1,y)){
		num += travel(map,x+1,y);
	}
	if(canReach(map,x,y-1)){
		num += travel(map,x,y-1);
	}
	if(canReach(map,x,y+1)){
		num += travel(map,x,y+1);
	}
	if(canReach(map,x-1,y-1)){
		num += travel(map,x-1,y-1);
	}
	if(canReach(map,x-1,y+1)){
		num += travel(map,x-1,y+1);
	}
	if(canReach(map,x+1,y-1)){
		num += travel(map,x+1,y-1);
	}
	if(canReach(map,x+1,y+1)){
		num += travel(map,x+1,y+1);
	}
	return num;
}
function canSetTreasure(map,x,y){//宝藏一定要与任何一点连通(不能被炸弹分割)，不然在随机挑选出生地的时候又得检查
	var num = travel(map,x,y);
	var rows = map.length;
	var cols = map[x].length;
	for(var i = 0; i < rows; i++){
		for(var j = 0; j < cols; j++){
			if(map[i][j] == window.traveled){
				map[i][j] = window.floorFlag;
			}
		}
	}
	if(num!=window.mapColNum*window.mapRowNum-window.curBombNum){//必须宝藏与地图上任意地板都可达才行
		return false;
	}
	return true;
}
function myResize(){
	var app = document.getElementById('app');
	app.style.width = window.myWidth+'px';
	app.style.height = window.myHeight+'px';
	window.gridSize = Math.min(Math.floor(window.myHeight/mapRowNum),Math.floor(window.myWidth/mapColNum));
	var topBarSize = window.gridSize;
	var offsetTop;
	var offsetLeft = (window.myWidth-window.gridSize*mapColNum)/2;
	if(window.myHeight>window.myWidth){
		offsetTop = (window.myHeight-window.gridSize*mapRowNum-topBarSize)/2+topBarSize/2;
		$('#topBar').css('width',window.myWidth).css('height',topBarSize).css('left',0).css('top',offsetTop-topBarSize).css('line-height',window.gridSize+'px');
	}
	else{
		offsetTop = (window.myHeight-window.gridSize*mapRowNum)/2;
		$('#topBar').css('width',topBarSize).css('height',window.myHeight).css('top',0).css('left',offsetLeft-2*topBarSize);
	}
	window.mapContainer.style.width = window.gridSize*mapColNum+'px';
	window.mapContainer.style.height = window.gridSize*mapRowNum+'px';
	window.mapContainer.style.marginLeft = offsetLeft+'px';
	window.mapContainer.style.marginTop = offsetTop+'px';
	window.mapContainer.style.display = 'block';
	var num = window.gridsArray.length;
	for(var i = 0; i < num; i++){
		var tmpGrid = window.gridsArray[i];
		var tmpRows = Math.floor(i/window.mapColNum);
		var tmpCols = i%window.mapColNum;
		tmpGrid.style.width = window.gridSize+'px';
		tmpGrid.style.height = window.gridSize+'px';
		tmpGrid.style.left = window.gridSize*tmpCols+'px';
		tmpGrid.style.top = window.gridSize*tmpRows+'px';
	}
	var tmpWidth = window.gridSize;
	$('#markSafeImg').css('width',tmpWidth).css('height',tmpWidth);
	$('#markFlagImg').css('width',tmpWidth).css('height',tmpWidth);
	$('#cmdArea').hide();
}
function gameInit(){
	$('#myScript').remove();
	baseInit();
	wxInit('bomb','扫雷夺宝','扫雷夺宝','bomb/bomb.png');
	$('#mapContainer').on('touchmove',function(e){
		e.preventDefault();
		return false;
	});
	$(document.body).on('click',function(){
		hideCmdArea();
	});
	$('#markSafeImg').attr('src',window.moveImg).on('click',function(){
		var x = window.curPosX;
		var y = window.curPosY;
		switch(window.map[x][y]){
			case window.floorFlag:
			dfsExplore(x,y);
			if(window.canTouchMap[window.treasurePos.x][window.treasurePos.y] == window.canTouchFlag){
				setTimeout(function(){
					userWin('恭喜过关');
				},500);
				return;
			}
			window.flagMap[x][y] = window.safeFlag;
			break;
			case window.bombFlag:
			window.flagMap[x][y] = window.bombFlag;
			$('#grid'+x+'r'+y+'c .flagImg').attr('src',window.bombImg).show();
			setTimeout(userLose,500);
			break;
			default:break;
		}
	});
	$('#markFlagImg').attr('src',window.bombMarkImg).on('click',function(){
		var x = window.curPosX;
		var y = window.curPosY;
		if(window.flagMap[x][y] == window.bombMarkFlag){
			window.flagMap[x][y] = window.visitedFlag;
			$('#grid'+x+'r'+y+'c .flagImg').attr('src',window.circleImg).show();
		}
		else{
			$('#grid'+x+'r'+y+'c .flagImg').attr('src',window.bombMarkImg).show();
			window.flagMap[x][y] = window.bombMarkFlag;
		}
	});
	readLocalData();
	initMap();
	generateMap();
	$(window).resize(myResize);
	$(window).resize();
	gameStart();
}