window.wxConfig = {
	server:'http://115.28.23.36:3000',
	imgUrl:'http://115.28.23.36:3000/image/defaultWxShare.png',
	descContent:"游戏的一生,一生的游戏",
	title:'游戏世界',
	remindDomId:false,
	remindTime:3000
}
function wxSetServer(url){
	window.wxConfig.server = location.origin+'/'+url;
}
function wxSetImgUrl(url){
	window.wxConfig.imgUrl = location.origin+'/image/'+url;
}
function wxSetContent(str){
	window.wxConfig.descContent = str;
}
function wxSetTitle(str){
	window.wxConfig.title = str;
}
function wxSetRemindId(id){
	window.wxConfig.remindDomId = id;
}
function wxRemind(callback){
	if(window.wxConfig.remindDomId){
		document.getElementById(window.wxConfig.remindDomId).style.display = 'block';
		setTimeout(function(){
			document.getElementById(window.wxConfig.remindDomId).style.display = 'none';
			if(callback){
				callback();
			}
		},window.wxConfig.remindTime);
	}
}
function wxBindEvent(){
	if(!('WeixinJSBridge' in window)){return;}
	WeixinJSBridge.on('menu:share:appmessage', function(){
		WeixinJSBridge.invoke('sendAppMessage',{
			"img_url": window.wxConfig.imgUrl,
			"link": window.wxConfig.server,
			"desc": window.wxConfig.descContent,
			"title": window.wxConfig.title
		});
	});
	WeixinJSBridge.on('menu:share:timeline', function(){
		WeixinJSBridge.invoke('shareTimeline',{
			"img_url": window.wxConfig.imgUrl,
			"link": window.wxConfig.server,
			"desc": window.wxConfig.descContent,
			"title": window.wxConfig.descContent
		});
	});
	WeixinJSBridge.on('menu:share:weibo', function(){
		WeixinJSBridge.invoke('shareWeibo',{
			"title": window.wxConfig.title,
			"content": window.wxConfig.descContent,
			"img_url": window.wxConfig.imgUrl,
			"link": window.wxConfig.server
		});
	});
}
function wxInit(server,title,content,imgUrl,remindDomId){
	wxSetServer(server);
	wxSetTitle(title);
	wxSetContent(content);
	wxSetImgUrl(imgUrl);
	if(remindDomId){
		wxSetRemindId(remindDomId);
	}
	if('WeixinJSBridge' in window){
		wxBindEvent();
	}
	else{
		document.addEventListener('WeixinJSBridgeReady',wxBindEvent,false);
	}
}