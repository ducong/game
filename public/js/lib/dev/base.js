function setCookie(key,value,expiredays){
	var exdate = new Date();
	exdate.setDate(exdate.getDate()+expiredays)
	document.cookie = key + '=' + escape(value)+ ((expiredays==null) ? '' : ';expires='+exdate.toGMTString());
}
function getCookie(c_name){
	if(document.cookie.length>0){
  		var c_start=document.cookie.indexOf(c_name + '=')
  		if (c_start!=-1){ 
    		c_start=c_start+c_name.length+1;
    		var c_end=document.cookie.indexOf(';',c_start);
    		if(c_end==-1){
    			c_end=document.cookie.length;
    		}
    		return unescape(document.cookie.substring(c_start,c_end));
    	} 
  	}
	return '';
}
function deleteCookie(key){
	setCookie(key,'',-1);
}
function getLocalData(key){
	if('localStorage' in window){
		return window.localStorage.getItem(key);
	}
	return getCookie(key);
}
function setLocalData(key,value,expiredays){
	if('localStorage' in window){
		window.localStorage.setItem(key,value);
	}
	else{
		if(typeof(expiredays) == 'undefined'){
			expiredays = 30;
		}
		setCookie(key,value,expiredays)
	}
}
function deleteLocalData(key){
	if('localStorage' in window){
		window.localStorage.removeItem(key);
	}
	else{
		deleteCookie(key);
	}
}
function baseOnResize(){
	window.myWidth=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;
    window.myHeight=window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight;
	window.fontSize = Math.floor(16*(Math.min(window.myWidth,window.myHeight))/320);
	document.body.style.fontSize = window.fontSize+'px';
}
function baseInit(){
	window.RAF = window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function (callback){window.setTimeout(callback, 17);};
	baseOnResize();
	if(typeof($)!='undefined'){
		$(window).resize(baseOnResize);
	}
	if(typeof(FastClick)!='undefined'){
		FastClick.attach(document.body);
	}
}
Array.prototype.binaryFind = function(goal,s,e){
	//假定数组已经按从大到小排序
	//排序方法为array.sort(function(a,b){return b-a;});
	var start,end;
	if(typeof(s)=='undefined'){
		start = 0;
	}
	else{
		start = s;
	}
	if(typeof(e)=='undefined'){
		end = this.length-1;
	}
	else{
		end = e;
	}
	if(start>end){return -1;}
	var mid = Math.floor((start+end)/2);
	if(this[mid]==goal){
		//若数组中有重复元素则返回首次出现的位置
		var j = mid;
		while(j>0&&this[j-1]==goal){j--;}
		return j;
	}
	var r = -1;
	if(this[mid]<goal){
		r = this.binaryFind(goal,start,mid-1);
	}
	else{
		r = this.binaryFind(goal,mid+1,end);
	}
	return r;
}
