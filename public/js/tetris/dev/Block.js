function Block(container,smallGridSize,midGridSize,bigGridSize){
	this.container = container;
	this.colNum = 0;
	this.rowNum = 0;
	this.midGridSize = midGridSize;
	this.bigGridSize = bigGridSize;
	this.setSize(smallGridSize);
	return this;
}
Block.prototype.setSize = function(gridSize){
	this.gridSize = gridSize;
	this.gapSize = Math.max(1,Math.floor(0.05*this.gridSize));
	this.borderRadius = this.gridSize*0.1;
	this.width = this.gridSize*this.colNum+this.gapSize*(this.colNum-1);
	this.height = this.gridSize*this.rowNum+this.gapSize*(this.rowNum-1);
}
Block.prototype.init = function(obj){
	if(!obj||!obj.shape){return;}
	var randomNum = Math.floor(Math.random()*4);//顺时针旋转的角度
	this.shape = [];
	this.color = obj.color;
	var curRow = obj.shape.length;
	var curCol = obj.shape[0].length;
	switch(randomNum){
		case 0://不旋转
		for(var i = 0; i < curRow; i++){
			this.shape.push([]);
			for(var j = 0; j < curCol; j++){
				this.shape[i].push(obj.shape[i][j]);
			}
		}
		break;
		case 1://旋转90度,行数和列数调换
		for(var i = 0; i < curCol; i++){
			this.shape.push([]);
			for(var j = 0; j < curRow; j++){
				this.shape[i].push(obj.shape[curRow-1-j][i]);
			}
		}
		break;
		case 2://旋转180度
		for(var i = 0; i < curRow; i++){
			this.shape.push([]);
			for(var j = 0; j < curCol; j++){
				this.shape[i].push(obj.shape[curRow-1-i][curCol-1-j]);
			}
		}
		break;
		case 3://旋转270度,行数和列数调换
		for(var i = 0; i < curCol; i++){
			this.shape.push([]);
			for(var j = 0; j < curRow; j++){
				this.shape[i].push(obj.shape[curRow-1-j][curCol-1-i]);
			}
		}
		break;
	}
	this.rowNum = this.shape.length;
	this.colNum = this.shape[0].length;
	this.block = document.createElement('div');
	this.block.className = 'block';
	this.container.appendChild(this.block);
	for(var i = 0; i < this.rowNum; i++){
		for(var j = 0; j < this.colNum; j++){
			if(this.shape[i][j]==1){
				var gridNode = document.createElement('div');
				gridNode.className = 'grid smallGrid';
				gridNode.style.backgroundColor = 'rgb('+this.color+')';
				$(gridNode).data('x',j).data('y',i);
				this.block.appendChild(gridNode);
			}
		}
	}
	this.width = this.gridSize*this.colNum+this.gapSize*(this.colNum-1);
	this.height = this.gridSize*this.rowNum+this.gapSize*(this.rowNum-1);
	$(this.block).css('width',this.width).css('height',this.height);
	this.resize();
	this.enableDrag();
}
Block.prototype.resize = function(){
	var t = this;
	$(this.block).find('.smallGrid').each(function(){
		var i = $(this).data('y');
		var j = $(this).data('x');
		var toLeft = j*(t.gridSize+t.gapSize);
		var toTop = i*(t.gridSize+t.gapSize);
		$(this).css('width',t.gridSize).css('height',t.gridSize).css('border-radius',t.borderRadius).css('left',toLeft).css('top',toTop);
	})
}
Block.prototype.setPosition = function(x,y){
	this.curLeft = this.toLeft = x;
	this.curTop = this.toTop = y;
	$(this.block).css('left',this.toLeft).css('top',this.toTop);
}
Block.prototype.enableDrag = function(){
	this.dragable = true;
	var t = this;
	$(t.block).on('touchstart',function(e){
		var touch = e.originalEvent.touches[0];
		t.startX = touch.clientX;
		t.startY = touch.clientY;
		
	}).on('touchmove',function(e){
		var touch = e.originalEvent.touches[0];
		var dx = Math.floor(touch.clientX-t.startX);
		var dy = Math.floor(touch.clientY-t.startY);
		t.curLeft = t.toLeft+dx;
		t.curTop = t.toTop+dy;
		$(this).css('left',t.curLeft).css('top',t.curTop);
	}).on('touchend',function(e){
		var canFill = false;
		var callback,leftGoal,topGoal;
		if(canFill){

		}
		else{
			leftGoal = t.toLeft;
			topGoal = t.toTop;
			callback = function(){
				console.log('callback moveEnd');
			}
		}
		t.moveTo(leftGoal,topGoal,callback);
	});
}
Block.prototype.moveTo = function(leftGoal,topGoal,callback){
	var t = this;
	var dx = (leftGoal-t.curLeft)/20;
	var dy = (topGoal-t.curTop)/20;
	function animate(){
		$(t.block).css('left',t.curLeft).css('top',t.curTop);
		if(t.curLeft==leftGoal&&t.curTop==topGoal){
			t.toLeft = t.curLeft;
			t.toTop = t.curTop;
			if(callback){
				callback();
			}
			return;
		}
		if(t.curLeft!=leftGoal){
			if(dx>0){
				t.curLeft = Math.min(leftGoal,t.curLeft+dx);
			}
			else{
				t.curLeft = Math.max(leftGoal,t.curLeft+dx);
			}
		}
		if(t.curTop!=topGoal){
			if(dy>0){
				t.curTop = Math.min(topGoal,t.curTop+dy);
			}
			else{
				t.curTop = Math.max(topGoal,t.curTop+dy);
			}
		}
		RAF(animate);
	}
	animate();
}