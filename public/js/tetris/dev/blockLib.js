var blockArray = [
	{
		shape:[//形状,可以任意旋转
			[1,1,1],
			[1,1,1],
			[1,1,1]
		],
		weight:1//权重，决定出现的概率
	},
	{
		shape:[
			[1,1],
			[1,1]
		],
		weight:4
	},
	{
		shape:[
			[1,1],
			[1,0]
		],
		weight:5
	},
	{
		shape:[
			[1,1,1],
			[1,0,0]
		],
		weight:3
	},
	{
		shape:[
			[1,1,1,1],
			[1,0,0,0]
		],
		weight:2
	},
	{
		shape:[
			[1,1,1],
			[1,0,1]
		],
		weight:2
	},
	{
		shape:[
			[1,1,1],
			[1,1,0]
		],
		weight:2
	},
	{
		shape:[
			[1,1,1],
			[1,0,0],
			[1,0,0]
		],
		weight:3
	},
	{
		shape:[
			[1]
		],
		weight:1
	},
	{
		shape:[
			[1,1]
		],
		weight:2
	},
	{
		shape:[
			[1,1,1]
		],
		weight:3
	},
	{
		shape:[
			[1,1,1,1]
		],
		weight:2
	},
	{
		shape:[
			[1,1,1,1,1]
		],
		weight:1
	}
];
(function(){
//根据各个方块的权重生成一个随机数组，以供随机提取，生成方法为将各个方块的编号push到这个随机数组中,
//每个方块的编号push的次数为对应的权重值，然后随机打乱数组顺序(这个应该不需要，因为每次抽取也是随机的，所以选中的概率只由个数决定，但我喜欢打乱~~)
	window.blockColorArray = ['152,220,85','92,190,228','89,203,134','77,213,276','231,106,130','237,149,74','220,101,85','126,142,213','140, 90, 153','88, 69, 94','50, 113, 53','184, 181, 160','199, 108, 87','87, 118, 199','166, 52, 113'];
	var colorLen = window.blockColorArray.length;
	var blockLen = blockArray.length;
	if(colorLen<blockLen){
		alert('颜色值小于方块种类');
		return;
	}
	window.randomBlockArray = [];
	for(var i = 0; i < blockLen; i++){
		var block = blockArray[i];
		if(!block||!block.weight){continue;}
		var randomNum = Math.floor(Math.random()*window.blockColorArray.length);
		block.color = window.blockColorArray[randomNum];
		window.blockColorArray.splice(randomNum,1);
		for(var j = 0; j < block.weight; j++){
			window.randomBlockArray.push(i);
		}
	}
	var num = window.randomBlockArray.length;
	for(var i = 0; i < num; i++){
		var j = Math.floor(Math.random()*num);
		var tmp = window.randomBlockArray[j];
		window.randomBlockArray[j] = window.randomBlockArray[i];
		window.randomBlockArray[i] = tmp;
	}
	// var styleDom = document.createElement('style');//将动态的样式通过新建style标签的方式注入，这样的优点是不用针对具体的dom节点去修改css，直接更改class就行了
	// var styleStr = '';
	// for(var i = 0; i < blockLen; i++){
	// 	var block = blockArray[i];
	// 	if(!block||!block.weight){continue;}
	// 	styleStr += '.blockColor'+i+'{background-color:rgb('+block.color+');}.blockColorShadow'+i+'{background-color:rgba('+block.color+'0.7)}';
	// }
	// styleDom.innerHTML = styleStr;
	// document.head.appendChild(styleDom);
})();