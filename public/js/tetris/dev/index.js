;
window.mapColNum = 10;
window.mapRowNum = 10;
function drawMap(){
	var mapContainer = $('#mapContainer')[0];
	for(var i = 0; i < window.mapColNum; i++){
		for(var j = 0; j < window.mapRowNum; j++){
			var bgGridNode = document.createElement('div');
			bgGridNode.className = 'bigGrid bgGrid';
			bgGridNode.id = 'bgGrid'+i+'x'+j;
			var blockGridNode = document.createElement('div');
			blockGridNode.className = 'bigGrid blockGrid';
			blockGridNode.id = 'blockGrid'+i+'x'+j;
			mapContainer.appendChild(bgGridNode);
			mapContainer.appendChild(blockGridNode);
		}
	}
	drawMap = $.noop;
}
function chooseBlock(){
	var container = $('#blockContainer')[0];
	window.leftBlock = new Block(container,window.smallGridSize,window.midGridSize,window.bigGridSize);
	window.centerBlock = new Block(container,window.smallGridSize,window.midGridSize,window.bigGridSize);
	window.rightBlock = new Block(container,window.smallGridSize,window.midGridSize,window.bigGridSize);
	var index = Math.floor(Math.random()*window.randomBlockArray.length);
	leftBlock.init(blockArray[window.randomBlockArray[index]]);
	index = Math.floor(Math.random()*window.randomBlockArray.length);
	centerBlock.init(blockArray[window.randomBlockArray[index]]);
	index = Math.floor(Math.random()*window.randomBlockArray.length);
	rightBlock.init(blockArray[window.randomBlockArray[index]]);
	var partWidth = (window.mapWidth+window.bigGridSize)/3;
	var partHeight = partWidth;
	var partLeft = Math.floor((window.myWidth-partWidth*3)/2);
	leftBlock.setPosition(Math.floor(partLeft+(partWidth-leftBlock.width)/2),Math.floor((partHeight-leftBlock.height)/2));
	centerBlock.setPosition(Math.floor(partLeft+partWidth+(partWidth-centerBlock.width)/2),Math.floor((partHeight-centerBlock.height)/2));
	rightBlock.setPosition(Math.floor(partLeft+2*partWidth+(partWidth-rightBlock.width)/2),Math.floor((partHeight-rightBlock.height)/2));
}
function bindEvent(){
	$('#blockContainer').on('touchstart',function(e){
		e.preventDefault();
	});
	bindEvent = $.noop;
}
function myResize(){
	window.bigGridSize = Math.floor(1.64*window.fontSize);
	window.midGridSize = Math.floor(window.bigGridSize*0.95);
	window.smallGridSize =  Math.floor(window.bigGridSize*0.65);
	window.gridGap = Math.floor(0.1*window.fontSize);
	window.mapHeight = window.mapWidth = window.mapColNum*window.bigGridSize+(window.mapColNum-1)*window.gridGap;
	$('#app').css('width',myWidth).css('height',myHeight);
	$('#mapContainer').css('height',window.mapHeight);
	var gridStyleStr = '.bigGrid{width:'+window.bigGridSize+'px;height:'+window.bigGridSize+'px;}';
	if($('#gridStyle').length){
		$('#gridStyle').html(gridStyleStr);
	}
	else{
		var gridStyleDom = document.createElement('style');
		gridStyleDom.id = 'gridStyle';
		gridStyleDom.innerHTML = gridStyleStr;
		document.head.appendChild(gridStyleDom);
	}
	var leftGap = Math.floor((window.myWidth-window.mapWidth)/2);
	for(var i = 0; i < window.mapColNum; i++){
		for(var j = 0; j < window.mapRowNum; j++){
			var toTop = i*(window.bigGridSize+window.gridGap);
			var toLeft = j*(window.bigGridSize+window.gridGap);
			$('#bgGrid'+i+'x'+j).css('top',toTop).css('left',leftGap+toLeft);
			$('#blockGrid'+i+'x'+j).css('top',toTop).css('left',leftGap+toLeft);
		}
	}
}
function gameEnd(){}
function gameRestart(){}
function gameInit(){
	$('#myScript').remove();
	$('#scoreLogo').attr('src','/image/tetris/jiangbei.png');
	drawMap();
	myResize();
	chooseBlock();
	bindEvent();
	gameInit = $.noop;
}