/*447101029@qq.com*/
var squirrelArray = [];
function Squirrel(x,y){
	this.flag = squirrelFlag;
	this.pos = {x:x,y:y};
	this.dom = document.createElement('img');
	var index = mapColNum*x+y;
	this.dom.id = index+'grids';
	this.dom.src = squirrelImg;
	this.dom.style.width = gridSize+'px';
	this.dom.style.height = gridSize+'px';
	this.dom.style.left = y*gridSize+'px';
	this.dom.style.top = x*gridSize+'px';
	mapContainer.appendChild(this.dom);
	touchEventBind(this,this.dom);
	squirrelArray.push(this);
	return this;
}
Squirrel.clear = function(){
	var len = squirrelArray.length;
	for(var i = 0; i < len; i++){
		var d = squirrelArray[i].dom
		d.style.display = 'none';
		d.parentNode.removeChild(d);
	}
	squirrelArray = [];
}
Squirrel.prototype.move = function(dx,dy){//dx,dy表示相对位移
	var t = this;
	var nextPos;
	var around = [
	{
		x:t.pos.x,
		y:t.pos.y-1
	},
	{
		x:t.pos.x,
		y:t.pos.y+1
	},
	{
		x:t.pos.x-1,
		y:t.pos.y
	},
	{
		x:t.pos.x+1,
		y:t.pos.y
	}];
	var stop = false;
	for(var i = 0; i < 4; i++){
		nextPos = curMap[mapColNum*around[i].x+around[i].y];
		if(typeof(nextPos) == 'object'){//先检查动物
			switch(nextPos.flag){
				case boarFlag:
				moveStart = false;
				eatElem(nextPos,t.pos.x,t.pos.y,userLose);
				return;
				break;
			}
		}
	}
	for(var i = 0; i < 4; i++){
		nextPos = curMap[mapColNum*around[i].x+around[i].y];
		if(typeof(nextPos) == 'number'){//后检查植物
			switch(nextPos){
				case fruitFlag:
				moveStart = false;
				stop = true;
				eatElem(t,around[i].x,around[i].y);
				break;
				case nutFlag:
				moveStart = false;
				eatElem(t,around[i].x,around[i].y,nextLevel);
				return;
				break;
			}
		}
	}
	if(stop){return;}
	var nextPos = curMap[mapColNum*(t.pos.x+dx)+t.pos.y+dy];
	if(nextPos === grassFlag){//下个位置是植物才能前进
		elemMoveTo(t,t.pos.x+dx,t.pos.y+dy,function(){
			t.move(dx,dy);
		});
	}
	else{
		moveStart = false;
	}
}