/*447101029@qq.com*/
window.mapRowNum = 8;//地图行数
window.mapColNum = 14;//地图列数
window.gridSize = 0;//单个格子的尺寸
window.curLevel = 0;//当前关卡数
window.curMaxLevel = 0;//当前通过的最大关卡数
window.levelNum = 0;//关卡总数
window.curMap = null;//当前地图
window.curStep = 0;//当前地图花费的步数
window.curMinStep = 0;//当前地图的最少通关步数
window.mapMinY = 0;//当前地图中需要展示的最左边的列
window.mapMaxY = mapColNum-1;//当前地图中需要展示的最右边的列
window.moveStart = false;//标记移动是否已经开始
window.mapContainer = null;//mapContainer
function nextLevel(){
	var recordMinStep = parseInt(getLocalData('level'+curLevel))||10000000;
	setLocalData('level'+curLevel,Math.min(recordMinStep,curStep));
	wxSetContent('哈哈我只用'+Math.min(recordMinStep,curStep)+'步就过了第'+(curLevel+1)+'关,小伙伴们谁能击败我吗？');
	setLocalData('curLevel',++curLevel);
	curMaxLevel = Math.max(curMaxLevel,curLevel);
	setLocalData('curMaxLevel',curMaxLevel);
	refreshLevelBoard();
	curMap = null;
	if(curLevel<levelNum){
		alert('恭喜你使用了'+curStep+'步过关(本关最少只需'+curMinStep+'步)！请继续吧~');
		gameStart();
	}
	else{
		alert('恭喜你使用了'+curStep+'步过关(本关最少只需'+curMinStep+'步)！');
		alert('恭喜通关！敬请期待地图更新吧~');
		showLevelBoard();
	}
}
function initLevelBoard(){
	var container = document.getElementById('levelContainer');
	for(var i = 0; i < levelNum; i++){
		var dom = document.createElement('div');
		dom.id = i+'levelFlag';
		dom.className = 'levelFlag';
		container.appendChild(dom);
		var numDom = document.createElement('div');
		numDom.className = 'levelFlagNum';
		numDom.innerHTML = (i+1);
		dom.appendChild(numDom);
		var statusDom = document.createElement('div');
		statusDom.className = 'levelStatus';
		statusDom.id = i+'levelStatus';
		dom.appendChild(statusDom);
		if(i<=curMaxLevel){
			var recordMinStep = parseInt(getLocalData('level'+i));
			if(!isNaN(recordMinStep)){
				statusDom.innerHTML = recordMinStep+'/'+mapArray[i].minStep;
			}
			else{
				statusDom.innerHTML = '?';
			}
			dom.style.display = 'block';
		}
		else{
			dom.style.display = 'none';
		}
		$(dom).on('click',function(){
			hideLevelBoard();
			curLevel = parseInt(this.id);
			gameStart();
		});
	};
	if(!window.lowDevice){
		window.levelBoardScroll = new IScroll('#levelWrapper', { mouseWheel: true });
	}
}
function refreshLevelBoard(){
	var container = document.getElementById('levelContainer');
	for(var i = 0; i < levelNum; i++){
		var dom = document.getElementById(i+'levelFlag');
		var statusDom = document.getElementById(i+'levelStatus');
		if(i<=curMaxLevel){
			var recordMinStep = parseInt(getLocalData('level'+i));
			if(!isNaN(recordMinStep)){
				statusDom.innerHTML = recordMinStep+'/'+mapArray[i].minStep;
			}
			else{
				statusDom.innerHTML = '?';
			}
			dom.style.display = 'block';
		}
		else{
			dom.style.display = 'none';
		}
	}
}
function showLevelBoard(){
	var levelBoard = document.getElementById('levelBoard');
	if(levelBoard.style.display == 'block'){return;}
	levelBoard.style.display = 'block';
	if(!window.lowDevice){
		window.levelBoardScroll.refresh();
	}
	var curLeft = window.myWidth;
	var base = window.myWidth/20;
	function animate(){
		var dx = curLeft/20+base;
		curLeft = Math.max(curLeft-dx,0);
		levelBoard.style.left = curLeft+'px';
		if(curLeft>0){
			window.RAF(animate);
		}
	}
	animate();
}
function hideLevelBoard(){
	if(curLevel>=levelNum){
		curLevel = 0;
		gameStart();
	}
	var levelBoard = document.getElementById('levelBoard');
	if(levelBoard.style.display == 'none'){return;}
	var curLeft = 0;
	var base = window.myWidth/20;
	function animate(){
		var dx = (window.myWidth-curLeft)/20+base;
		curLeft = Math.min(curLeft+dx,window.myWidth);
		levelBoard.style.left = curLeft+'px';
		if(curLeft<window.myWidth){
			window.RAF(animate);
		}
		else{
			levelBoard.style.display = 'none';
		}
	}
	animate();
}
function touchEventBind(obj,dom){
	mouseEventBind(obj,dom);
	var t = obj;
	$(dom).on('touchstart',function(e){
		console.log('touchstart',e);
		if(!window.lowDevice){
			e.preventDefault();
		}
		if(t.dragStart){return false;}
		t.dragStart = true;
		t.dragPos = {
			x:e.originalEvent.changedTouches[0].pageX,
			y:e.originalEvent.changedTouches[0].pageY
		}
		return false;
	});
	$(dom).on('touchmove',function(e){
		if(!window.lowDevice){
			e.preventDefault();
		}
		if(!t.dragStart){return false;}
		var curPos = {
			x:e.originalEvent.changedTouches[0].pageX,
			y:e.originalEvent.changedTouches[0].pageY
		}
		var dx = Math.abs(curPos.x-t.dragPos.x);
		var dy = Math.abs(curPos.y-t.dragPos.y);
		if(Math.max(dx,dy)<gridSize/4){return false;}
		t.dragStart = false;
		if(dx>dy){
			if(curPos.x>t.dragPos.x){
				checkMove(t,0,1);
			}
			else{
				checkMove(t,0,-1);
			}
		}
		else{
			if(curPos.y>t.dragPos.y){
				checkMove(t,1,0);
			}
			else{
				checkMove(t,-1,0);
			}
		}
		return false;
	});
	$(dom).on('touchend',function(e){
		if(!window.lowDevice){
			e.preventDefault();
		}
		if(!t.dragStart){return false;}
		var curPos = {
			x:e.originalEvent.changedTouches[0].pageX,
			y:e.originalEvent.changedTouches[0].pageY
		}
		var dx = Math.abs(curPos.x-t.dragPos.x);
		var dy = Math.abs(curPos.y-t.dragPos.y);
		if(Math.max(dx,dy)<gridSize/4){return false;}
		t.dragStart = false;
		if(dx>dy){
			if(curPos.x>t.dragPos.x){
				checkMove(t,0,1);
			}
			else{
				checkMove(t,0,-1);
			}
		}
		else{
			if(curPos.y>t.dragPos.y){
				checkMove(t,1,0);
			}
			else{
				checkMove(t,-1,0);
			}
		}
		return false;
	});
}
function mouseEventBind(obj,dom){
	var t = obj;
	$(dom).on('mousedown',function(e){
		if(!window.lowDevice){
			e.preventDefault();
		}
		if(t.dragStart){return false;}
		t.dragStart = true;
		t.dragPos = {
			x:e.clientX,
			y:e.clientY
		}
		return false;
	});
	$(dom).on('mousemove',function(e){
		if(!window.lowDevice){
			e.preventDefault();
		}
		if(!t.dragStart){return false;}
		var curPos = {
			x:e.clientX,
			y:e.clientY
		}
		var dx = Math.abs(curPos.x-t.dragPos.x);
		var dy = Math.abs(curPos.y-t.dragPos.y);
		if(Math.max(dx,dy)<gridSize/4){return false;}
		t.dragStart = false;
		if(dx>dy){
			if(curPos.x>t.dragPos.x){
				checkMove(t,0,1);
			}
			else{
				checkMove(t,0,-1);
			}
		}
		else{
			if(curPos.y>t.dragPos.y){
				checkMove(t,1,0);
			}
			else{
				checkMove(t,-1,0);
			}
		}
		return false;
	});
}
function checkMove(t,dx,dy){//检查对象t能否沿着(dx,dy)方向移动
	if(moveStart){return;}
	var nextPos = curMap[mapColNum*(t.pos.x+dx)+t.pos.y+dy];
	if(nextPos === grassFlag){//下个位置是植物才能前进
		moveStart = true;
		addStep(t,dx,dy);
		elemMoveTo(t,t.pos.x+dx,t.pos.y+dy,function(){
			t.move(dx,dy);
		});
	}
}
function addStep(obj,dx,dy){
	curStep++;
	document.getElementById('curStep').innerHTML = curStep;
}
function eatElem(obj,x,y,callBack){//obj吃掉curMap[x,y]处的物体
	setTimeout(function(){
		var ox = obj.pos.x;
		var oy = obj.pos.y;
		elemMoveTo(obj,x,y,function(){
			elemMoveTo(obj,ox,oy,function(){
				var index = mapColNum*x+y;
				if(!window.lowDevice&&typeof(curMap[index] == 'object')){
					delete curMap[index];
				}
				curMap[index] = grassFlag;
				var elem = document.getElementById(index+'grids');
				var curOpacity = 100;
				function animation(){
					curOpacity = Math.max(0,curOpacity-10);
					if(window.lowDevice){
						elem.style.filter ='alpha(opacity='+curOpacity+')';
					}
					else{
						elem.style.opacity = curOpacity/100;
					}
					if(curOpacity<=0){
						elem.style.display = 'none';
						if(callBack){
							callBack();
						}
					}
					else{
						window.RAF(animation);
					}
				}
				animation();
			})
		})
	},200);
}
function elemMoveTo(obj,gx,gy,callBack){//obj移动到gx,gy表示的目的坐标
	var t = obj;
	if(gx==t.pos.x&&gy==t.pos.y){return;}
	var curLeft = t.pos.y*gridSize;
	var curTop = t.pos.x*gridSize;
	var goalLeft = gy*gridSize;
	var goalTop = gx*gridSize;
	var dLeft = gridSize/8;
	var dTop = gridSize/8;
	if(window.lowDevice){
		dLeft = gridSize/4;
		dTop = gridSize/4;
	}
	if(t.pos.x>gx){
		dTop *= -1;
	}
	if(t.pos.x==gx){
		dTop = 0;
	}
	if(t.pos.y>gy){
		dLeft *= -1;
	}
	if(t.pos.y==gy){
		dLeft = 0;
	}
	function animation(){
		if(dLeft>0){
			curLeft = Math.min(goalLeft,curLeft+dLeft);
		}
		else if(dLeft<0){
			curLeft = Math.max(goalLeft,curLeft+dLeft);
		}
		if(dTop>0){
			curTop = Math.min(goalTop,curTop+dTop);
		}
		else if(dTop<0){
			curTop = Math.max(goalTop,curTop+dTop);
		}
		t.dom.style.left = curLeft+'px';
		t.dom.style.top = curTop+'px';
		if(Math.abs(curLeft-goalLeft)<1&&Math.abs(curTop-goalTop)<1){
			var originIndex = mapColNum*t.pos.x+t.pos.y;
			var newIndex = mapColNum*gx+gy;
			t.pos.x = gx;
			t.pos.y = gy;
			t.dom.id = newIndex+'grids';
			curMap[originIndex] = grassFlag;
			curMap[newIndex] = t;
			if(callBack){callBack();}
		}
		else{
			window.RAF(animation);
		}
	}
	animation();
}
function userLose(){
	alert('你输啦！再来一次吧~');
	curMap = null;
	gameStart();
}
function initMap(){
	for(var i = 0; i < mapRowNum; i++){
		for(var j = 0; j < mapColNum; j++){
			var index = mapColNum*i+j;
			var r = Math.floor(Math.random()*treeImgArray.length);
			var tree = document.createElement('img');
			tree.style.display = 'block';
			tree.id = index+'tree';
			tree.setAttribute('index',index);
			tree.src = treeImgArray[r];
			tree.style.display = 'none';
			mapContainer.appendChild(tree);
		}
	}
	window.fruitSpirit = document.createElement('img');
	window.fruitSpirit.src = fruitImg;
	window.fruitSpirit.style.display = 'none';
	window.nutSpirit = document.createElement('img');
	window.nutSpirit.src = nutImg;
	window.nutSpirit.style.display = 'none';
	mapContainer.appendChild(window.fruitSpirit);
	mapContainer.appendChild(window.nutSpirit);
}
function gameStart(){
	if(curLevel>=levelNum){
		showLevelBoard();
		return;
	}
	Squirrel.clear();
	Mouse.clear();
	Boar.clear();
	var mapObj = mapArray[curLevel];
	curMap = mapObj.map.slice(0);//拷贝
	curMinStep = mapObj.minStep;
	curStep = 0;
	document.getElementById('curLevel').innerHTML = (curLevel+1);
	document.getElementById('curStep').innerHTML = curStep;
	mapMinY = mapColNum-1;
	mapMaxY = 0;
	for(var i = 0; i < mapRowNum; i++){
		for(var j = 0; j < mapColNum; j++){
			var index = mapColNum*i+j;
			var flag = curMap[index];
			if(flag !== 0){
				mapMinY = Math.min(j,mapMinY);
				mapMaxY = Math.max(j,mapMaxY);
			}
		}
	}
	gridSize = Math.min(Math.floor(window.myHeight/(mapRowNum-1)),Math.floor(window.myWidth/(mapMaxY-mapMinY+2)));
	mapContainer.style.width = gridSize*mapColNum+'px';
	mapContainer.style.height = gridSize*mapRowNum+'px';
	mapContainer.style.top = (window.myHeight-gridSize*mapRowNum)/2+'px';
	if(gridSize*(mapColNum-3)<window.myWidth){
		mapContainer.style.left = (window.myWidth-gridSize*mapColNum)/2+'px';
	}
	else{
		mapContainer.style.left = -(mapMinY-0.5)*gridSize+'px';
	}
	for(var i = 0; i < mapRowNum; i++){
		for(var j = 0; j < mapColNum; j++){
			var index = mapColNum*i+j;
			var flag = curMap[index];
			var tree = document.getElementById(index+'tree');
			if(flag == treeFlag){
				tree.style.width = gridSize+'px';
				tree.style.height = gridSize+'px';
				tree.style.left = j*gridSize+'px';
				tree.style.top = i*gridSize+'px';
				tree.style.display = 'block';
			}
			else{
				tree.style.display = 'none';
			}
			switch(flag){
				case squirrelFlag:
				curMap[index] = new Squirrel(i,j);
				break;
				case mouseFlag:
				curMap[index] = new Mouse(i,j);
				break;
				case boarFlag:
				curMap[index] = new Boar(i,j);
				break;
				case fruitFlag:
				window.fruitSpirit.id = index+'grids';
				if(window.lowDevice){
					window.fruitSpirit.style.filter = 'alpha(opacity=100)';
				}
				else{
					window.fruitSpirit.style.opacity = 1;
				}
				window.fruitSpirit.style.display = 'block';
				window.fruitSpirit.style.width = gridSize+'px';
				window.fruitSpirit.style.height = gridSize+'px';
				window.fruitSpirit.style.left = j*gridSize+'px';
				window.fruitSpirit.style.top = i*gridSize+'px';
				break;
				case nutFlag:
				window.nutSpirit.id = index+'grids';
				if(window.lowDevice){
					window.nutSpirit.style.filter = 'alpha(opacity=100)';
				}
				else{
					window.nutSpirit.style.opacity = 1;
				}
				window.nutSpirit.style.display = 'block';
				window.nutSpirit.style.width = gridSize+'px';
				window.nutSpirit.style.height = gridSize+'px';
				window.nutSpirit.style.left = j*gridSize+'px';
				window.nutSpirit.style.top = i*gridSize+'px';
				break;
			}
		}
	}
}
function showAboutBoard(){
	var levelBoard = document.getElementById('aboutBoard');
	if(levelBoard.style.display == 'block'){return;}
	levelBoard.style.display = 'block';
	document.getElementById('showAbout').style.top = '-9999px';
	document.getElementById('chooseLevel').style.bottom = '-9999px';
	if(!window.lowDevice){
		window.aboutBoardScroll.refresh();
	}
	var curTop = -window.myHeight;
	var base = window.myHeight/20;
	var goalTop = window.myHeight*0.1;
	function animate(){
		var dx = -curTop/20+base;
		curTop = Math.min(curTop+dx,goalTop);
		levelBoard.style.top = curTop+'px';
		if(curTop<goalTop){
			window.RAF(animate);
		}
	}
	animate();
}
function hideAboutBoard(){
	var levelBoard = document.getElementById('aboutBoard');
	if(levelBoard.style.display == 'none'){return;}
	document.getElementById('showAbout').style.top = '0.5em';
	document.getElementById('chooseLevel').style.bottom = '1em';
	var curTop = window.myHeight*0.1;
	var base = window.myHeight/20;
	function animate(){
		var dx = (window.myHeight+curTop)/20+base;
		curTop = Math.max(curTop-dx,-window.myHeight);
		levelBoard.style.top = curTop+'px';
		if(curTop>-window.myHeight){
			window.RAF(animate);
		}
		else{
			levelBoard.style.display = 'none';
		}
	}
	animate();
}
function myResize(){
	var app = document.getElementById('app');
	app.style.width = window.myWidth+'px';
	app.style.height = window.myHeight+'px';
	gridSize = Math.min(Math.floor(window.myHeight/(mapRowNum-1)),Math.floor(window.myWidth/(mapMaxY-mapMinY+2)));
	mapContainer.style.width = gridSize*mapColNum+'px';
	mapContainer.style.height = gridSize*mapRowNum+'px';
	mapContainer.style.top = (window.myHeight-gridSize*mapRowNum)/2+'px';
	if(gridSize*(mapColNum-3)<window.myWidth){
		mapContainer.style.left = (window.myWidth-gridSize*mapColNum)/2+'px';
	}
	else{
		mapContainer.style.left = -(mapMinY-0.5)*gridSize+'px';
	}
	var imgArray = mapContainer.getElementsByTagName('img');
	var len = imgArray.length;
	for(var i = 0; i < len; i++){
		var index = parseInt(imgArray[i].id);
		var col = index%mapColNum;
		var row = Math.floor(index/mapColNum);
		imgArray[i].style.width = gridSize+'px';
		imgArray[i].style.height = gridSize+'px';
		imgArray[i].style.left = col*gridSize+'px';
		imgArray[i].style.top = row*gridSize+'px';
	}
	var loadingArea = document.getElementById('loadingArea');
	if(loadingArea){
		loadingArea.style.top = (window.myHeight-loadingArea.offsetHeight)/2+'px';
	}
	document.getElementById('levelWrapper').style.height = window.myHeight+'px';
	document.getElementById('levelWrapper').style.width = window.myWidth-window.fontSize*4+'px';
	document.getElementById('aboutWrapper').style.height = window.myHeight*0.8-window.fontSize*1.2+'px';
}
function gameInit(){
	$('#myScript').remove();
	mapContainer = document.getElementById('mapContainer');
	baseInit();
	$(window).resize(myResize);
	wxInit('getTheNut','智取坚果','','getTheNut/squirrel.png');
	levelNum = mapArray.length;
	curLevel = parseInt(getLocalData('curLevel'))||0;
	curMaxLevel = parseInt(getLocalData('curMaxLevel'))||0;
	curMaxLevel = Math.max(curLevel,curMaxLevel);
	if(curMaxLevel>=levelNum){
		wxSetContent('哈哈我已经通关了,小伙伴们要不要我教你们通关啊~');
	}
	else{
		wxSetContent('哈哈我已经玩到第'+(curMaxLevel+1)+'关了,小伙伴们谁能超过我吗？');
	}
	document.getElementById('app').style.background = 'url('+appBgImg+') repeat repeat';
	document.getElementById('footerStatus').style.bottom = '0px';
	var chooseLevel = document.getElementById('chooseLevel');
	var levelBoard = document.getElementById('levelBoard');
	chooseLevel.style.bottom = '1em';
	chooseLevel.getElementsByTagName('img')[0].src = chooseLevelImg;
	levelBoard.style.background = 'url('+levelBoardBg+') 100% 100%';
	$(chooseLevel).on('click',showLevelBoard);
	var levelBoardExit = document.getElementById('levelBoardExit');
	levelBoardExit.getElementsByTagName('img')[0].src = levelBoardExitImg;
	$(levelBoardExit).on('click',hideLevelBoard);
	var showAbout = document.getElementById('showAbout');
	showAbout.style.top = '0.5em';
	showAbout.getElementsByTagName('img')[0].src = questionImg;
	$(showAbout).on('click',showAboutBoard);
	var aboutBoardExit = document.getElementById('aboutBoardExit');
	aboutBoardExit.getElementsByTagName('img')[0].src = aboutBoardExitImg;
	$(aboutBoardExit).on('click',hideAboutBoard);
	document.getElementById('aboutSquirrel').src = squirrelImg;
	document.getElementById('aboutNut').src = nutImg;
	document.getElementById('aboutBoar').src = boarImg;
	document.getElementById('aboutMouse').src = mouseImg;
	document.getElementById('aboutFruit').src = fruitImg;
	document.getElementById('aboutTree').src = treeImgArray[1];
	$('#app').on('touchmove',function(e){
		e.preventDefault();
		return false;
	});
	$(document).on('touchmove',function(e){
		e.preventDefault();
		return false;
	});
	initMap();
	initLevelBoard();
	gameStart();
	$(window).resize();
	window.aboutBoardScroll = new IScroll('#aboutWrapper', { mouseWheel: true });
}