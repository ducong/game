/*447101029@qq.com*/
window.version = 1;
window.keyArray = ['C','D','E','F','G','A','B'];
window.keyDuration = [1000,1000,1000,1000,1000,1000,1000];
window.audioArray = [];
window.base = 3;
window.repeatKeyNum = 8;
window.keyLength = 28;
function myResize(){
	if(!('innerWidth' in window)){
		window.innerWidth = document.documentElement.clientWidth; 
		window.innerHeight = document.documentElement.clientHeight; 
	}
	window.fontSize = Math.floor(16*(Math.min(window.innerWidth,window.innerHeight))/320);
	document.body.style.fontSize = window.fontSize+'px';
	$('#app').css('width',window.innerWidth).css('height',window.innerHeight);
	$('#musicSelect').css('top',(window.fontSize-$('#musicSelect').height())/2);
	$('#musicKeyList').css('left',(window.innerWidth-window.fontSize)/2);
	$('#musicFlag').css('left',(window.innerWidth-window.fontSize)/2);
	window.perDeg = (360/7)*Math.PI/180;
	window.bigR = Math.floor(window.innerWidth*0.4);
	window.smallR = Math.floor(window.innerWidth*0.18);
	window.rainbowBottom = 0;
	var borderLeft = Math.ceil(bigR*Math.tan(perDeg/2));
	$('#keyContainer').css('width',2*bigR)
	.css('height',2*bigR)
	.css('left',(window.innerWidth-2*bigR)/2)
	.css('bottom',rainbowBottom);
	$('#innerCircle').css('width',smallR*2)
	.css('height',smallR*2)
	.css('top',bigR-smallR)
	.css('left',bigR-smallR);
	$('.key').css('border-bottom-width',bigR)
	.css('border-left',borderLeft+'px solid transparent')
	.css('border-right',borderLeft+'px solid transparent')
	.css('left',bigR-borderLeft);
	$('.keyFlag').css('top',(bigR+smallR-window.fontSize)/2)
	.css('left',$('.keyFlag').width()/-2);
	for(var i = 1; i < 8; i++){
		var deg = (i-1)*perDeg*180/Math.PI;
		$('#key'+i).css('transform','rotate('+deg+'deg)');
		$('#key'+i+' .keyFlag').css('transform','rotate(-'+deg+'deg)');
	}
	$('#musicSelect').css('top',(window.fontSize-$('#musicSelect').height())/2);
	$('#musicKeyList').css('left',(window.innerWidth-window.fontSize)/2);
}
function initKey(){
	for(var j = 0; j < window.repeatKeyNum; j++){
		for(var i = 0; i < window.keyLength; i++){
			var b = window.base + Math.floor(i/7);
			var k = new Audio('/audio/piano/yamaha/'+b+window.keyArray[i%7]+'.mp3');
			window.audioArray.push({
				src:k,
				playing:false
			});
		}
	}
}
function playKey(num){
	num = parseInt(num);
	if(window.freeStyle){
		for(var i = 0; i < window.repeatKeyNum; i++){
			var a = window.audioArray[num+3*7+window.keyLength*i];
			if(!a.playing){
				a.playing = true;
				setTimeout(function(){
					a.playing = false;
				},window.keyDuration[num]);
				a.src.currentTime = 0;
				a.src.play();
				break;
			}
		}
		return;
	}
	var baseNum = window.curMusic.base-window.base;
	var nextKey = 1000;
	var tmpKeyArray = window.curMusic.keyList[window.curSetenceIndex].key.split(',')[Math.floor(window.curWordIndex)].split('&');
	var dx = 1;
	if(tmpKeyArray.length == 1){
		nextKey = parseInt(tmpKeyArray[0]);
	}
	else{
		if(window.curWordIndex %1 == 0){
			nextKey = parseInt(tmpKeyArray[0]);
		}
		else{
			nextKey = parseInt(tmpKeyArray[1]);
		}
		dx = 0.5;
	}
	if(nextKey>7){
		var up = 1+Math.floor((nextKey-8)/7);
		nextKey -= 7*up;
		baseNum += up;
	}
	else if(nextKey<1){
		var down = 1+Math.floor(-nextKey/7);
		nextKey += 7*down;
		baseNum -= down;
	}
	if(num+1 == nextKey){
		for(var i = 0; i < window.repeatKeyNum; i++){
			var a = window.audioArray[num+baseNum*7+window.keyLength*i];
			// a.src.currentTime = 0;
			// a.src.play();
			if(!a.playing){
				a.playing = true;
				setTimeout(function(){
					a.playing = false;
				},window.keyDuration[num]);
				a.src.currentTime = 0;
				a.src.play();
				break;
			}
		}
		window.curWordIndex += dx;
		var newLength = Math.ceil(window.curWordIndex*1.2*window.fontSize);
		if(window.curSetenceIndex %2 == 0){
			$('#topMusicList .outerList').css('width',newLength);
		}
		else{
			$('#bottomMusicList .outerList').css('width',newLength);
		}
		if(window.curWordIndex >= window.curMusic.keyList[window.curSetenceIndex].word.length){
			window.curSetenceIndex++;
			window.curWordIndex = 0;
			if(window.curSetenceIndex >= window.curMusic.keyList.length){
				setTimeout(function(){
					alert('恭喜弹奏完成');
					var nextMusicIndex = 1+(window.curMusicIndex+1)%musicLib.length;
					$('#musicSelect').val(nextMusicIndex).trigger('change');
				},window.keyDuration[num]);
			}
			else{
				setList(window.curSetenceIndex);
			}
		}
	}
}
function selectMusic(index){
	if(index == 0){
		window.freeStyle = true;
		$('#musicScoreArea').hide();
		$('#musicTitle').html('练习模式');
		$('#musicBase').html('1 = E');
		return;
	}
	window.curMusicIndex = index-1;
	window.freeStyle = false;
	$('#musicScoreArea').show();
	window.curMusic = $.extend({},musicLib[window.curMusicIndex],true);
	window.curSetenceIndex = 0;
	window.curWordIndex = 0;
	$('#musicTitle').html(window.curMusic.name);
	$('#musicBase').html('1 = '+window.keyArray[window.curMusic.base-1]);
	setList(0);
	setList(1);
}
function setList(index){
	var listId = 'topMusicList';
	if(index % 2 == 1){
		listId = 'bottomMusicList';
	}
	var list = window.curMusic.keyList[index];
	var wordNum = list.word.length;
	var numHtml = '';
	var wordHtml = '';
	var tmpKeyArray = list.key.split(',');
	for(var i = 0; i < wordNum; i++){
		wordHtml += '<div class="wordContainer">'+list.word[i]+'</div>';
		var tmpKey = tmpKeyArray[i].split('&');
		var keyNum = tmpKey.length;
		for(var j = 0; j < keyNum; j++){
			if(keyNum>1){
				if(j==0){
					numHtml += '<div class="numContainer mergeNumContainer mergeFirstNumContainer">';
				}
				else{
					numHtml += '<div class="numContainer mergeNumContainer">';
				}
			}
			else{
				numHtml += '<div class="numContainer">';
			}
			var t = parseInt(tmpKey[j]);
			if(t>7){
				var up = 1+Math.floor((t-8)/7);
				t -= 7*up;
				numHtml += '<div class="topPointContainer">';
				for(var k = 0; k < up; k++){
					numHtml += '<div class="point">.</div>'
				}
				numHtml += '</div><div class="num">'+t+'</div><div class="bottomPointContainer"></div>';
			}
			else if(t<1){
				var down = 1+Math.floor(-t/7);
				t += 7*down;
				numHtml += '<div class="topPointContainer"></div><div class="num">'+t+'</div><div class="bottomPointContainer">';
				for(var k = 0; k < down; k++){
					numHtml += '<div class="point">.</div>'
				}
				numHtml += '</div>';
			}
			else{
				numHtml += '<div class="topPointContainer"></div><div class="num">'+t+'</div><div class="bottomPointContainer"></div>';
			}
			numHtml += '</div>'
		}
	}
	var wordLength = Math.ceil(wordNum*1.2*window.fontSize);
	$('#'+listId+' .numList').html(numHtml).css('width',wordLength);
	$('#'+listId+' .wordList').html(wordHtml).css('width',wordLength);
	$('#'+listId+' .outerList').css('display','none').css('width',0);
	setTimeout(function(){
		$('#'+listId+' .outerList').show();
	},500);
	if(listId == 'bottomMusicList'){
		$('#bottomMusicList .outerList').css('left',window.innerWidth-wordLength-window.fontSize);
	}
}
function initMusic(){
	var musicNum = musicLib.length;
	var selectHtml = '<option value=0>练习模式</option>';
	for(var i = 0; i < musicNum; i++){
		selectHtml += '<option value="'+(i+1)+'">'+musicLib[i].name+'</option>';
	}
	$('#musicSelect').html(selectHtml);
}
function bindEvents(){
	$(window).on('resize',myResize);
	$('#musicSelect').on('change',function(){
		selectMusic(parseInt(this.value));
	});
	$('#keyContainer').on('click',function(e){
		var e = e || window.event;
		var offsetX = e.pageX-(window.innerWidth-2*window.bigR)/2;
		var offsetY = e.pageY-(window.innerHeight-window.rainbowBottom-2*window.bigR);
		var r = Math.sqrt(Math.pow(offsetX-bigR,2)+Math.pow(offsetY-bigR,2));
		if(r<smallR){return;}
		var deg = (180/Math.PI)*Math.asin(Math.abs(offsetX-bigR)/r);
		if(offsetX<bigR){
			if(offsetY<bigR){
				//左上
				deg = 180 - deg;
			}
		}
		else{
			if(offsetY<bigR){
				//右上
				deg = 180 + deg;
			}
			else{
				//右下
				deg = 360 - deg;
			}
		}
		var num = Math.floor(0.5+deg*Math.PI/(180*window.perDeg));
		if(num>6){
			num = 0;
		}
		playKey(num);
	});
}
// function playKey(num){
// 	num = parseInt(num);
// 	play_multi_sound('tone-'+window.base+window.keyArray[num]);
// 	// for(var i = 0; i < repeatKeyNum; i++){
// 	// 	var a = window.audioArray[num+7*i];
// 	// 	if(!a.playing){
// 	// 		var k = a.src;
// 	// 		k.play();
// 	// 		a.playing = true;
// 	// 		setTimeout(function(){
// 	// 			a.playing = false;
// 	// 		},k.duration*1000);
// 	// 		break;
// 	// 	}
// 	// }
// }
function play_multi_sound(s) {
	for (var a=0;a <audiochannels.length; a++) {
		thistime = new Date();
		if (audiochannels[a]['finished'] < thistime.getTime()) {
			try{
				audiochannels[a]['finished'] = thistime.getTime() + document.getElementById(s).duration*1000;
				audiochannels[a]['channel'] = document.getElementById(s);
				audiochannels[a]['channel'].currentTime = 0;
				audiochannels[a]['channel'].volume=1;
				audiochannels[a]['channel'].play();
				audiochannels[a]['keyvalue'] = s;
		  	}
		  	catch(v){
		  		console.log(v.message); 
		  	}
			break;
		}
	}
}
function stop_multi_sound(s, sender) {
	for (var a=0;a <audiochannels.length; a++){
		if (audiochannels[a]['keyvalue'] == s){
			try{
				audiochannels[a]['channel'] = document.getElementById(s);
				setTimeout(function(){
					//audiochannels[a]['channel'].volume=0;
					audiochannels[a]['channel'].pause();
					audiochannels[a]['channel'].currentTime = 0
				},2500);
		  	}
		  	catch(v){
		  		console.log(v.message); 
		  	}
		  	break;
		}
	}
}
function init(){
	if(window.inited){return;}
	window.inited = true;
	window.RAF = window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function (callback){window.setTimeout(callback, 17);};
	FastClick.attach(document.body);
	// var channel_max = 32;
	// window.audiochannels = new Array();
	// for (var a=0;a<channel_max;a++) {
	// 	window.audiochannels[a] = {};
	// 	window.audiochannels[a]['channel'] = new Audio();
	// 	window.audiochannels[a]['finished'] = -1;
	// 	window.audiochannels[a]['keyvalue'] = '';
	// }
	myResize();
	bindEvents();
	initMusic();
	selectMusic(1);
	setTimeout(initKey,1000);
}
$(document).ready(init);