/*447101029@qq.com*/
window.version = 1;
window.keyArray = ['C','D','E','F','G','A','B'];
window.audioArray = [];
window.base = 3;
window.repeatKeyNum = 3;
function myResize(){
	$('#app').css('width',window.innerWidth).css('height',window.innerHeight);
	window.keyWidth = (window.innerWidth-1)/7;
	$('#keyContainer button').css('width',window.keyWidth);
}
function lazyLoad(jsArray,callBack){
	var len = jsArray.length;
	var loadingArea = document.getElementById('loadingArea');
	if(len<1){
		loadingArea.parentNode.removeChild(loadingArea);
		if(callBack){callBack();}
		return;
	}
	function loadJs(curIndex){
		loadingArea.innerHTML = '正在加载资源:'+(curIndex+1)+'/'+len;
		head.load(jsArray[curIndex]+'?v='+version,function(){
			if(curIndex<len-1){
				loadJs(curIndex+1);
			}
			else{
				loadingArea.parentNode.removeChild(loadingArea);
				if(callBack){callBack();}
			}
		});
	}
	loadJs(0);
}
function initKey(){
	for(var j = 0; j < window.repeatKeyNum; j++){
		for(var i = 0; i < 7; i++){
			var k = new Audio('/audio/piano/yamaha/'+window.base+window.keyArray[i]+'.ogg');
			window.audioArray.push({
				src:k,
				playing:false
			});
		}
	}
}
function playKey(num){
	num = parseInt(num);
	play_multi_sound('tone-'+window.base+window.keyArray[num]);
	// for(var i = 0; i < repeatKeyNum; i++){
	// 	var a = window.audioArray[num+7*i];
	// 	if(!a.playing){
	// 		var k = a.src;
	// 		k.play();
	// 		a.playing = true;
	// 		setTimeout(function(){
	// 			a.playing = false;
	// 		},k.duration*1000);
	// 		break;
	// 	}
	// }
}
function play_multi_sound(s) {
	for (var a=0;a <audiochannels.length; a++) {
		thistime = new Date();
		if (audiochannels[a]['finished'] < thistime.getTime()) {
			try{
				audiochannels[a]['finished'] = thistime.getTime() + document.getElementById(s).duration*1000;
				audiochannels[a]['channel'] = document.getElementById(s);
				audiochannels[a]['channel'].currentTime = 0;
				audiochannels[a]['channel'].volume=1;
				audiochannels[a]['channel'].play();
				audiochannels[a]['keyvalue'] = s;
		  	}
		  	catch(v){
		  		console.log(v.message); 
		  	}
			break;
		}
	}
}
function stop_multi_sound(s, sender) {
	for (var a=0;a <audiochannels.length; a++){
		if (audiochannels[a]['keyvalue'] == s){
			try{
				audiochannels[a]['channel'] = document.getElementById(s);
				setTimeout(function(){
					//audiochannels[a]['channel'].volume=0;
					audiochannels[a]['channel'].pause();
					audiochannels[a]['channel'].currentTime = 0
				},2500);
		  	}
		  	catch(v){
		  		console.log(v.message); 
		  	}
		  	break;
		}
	}
}
head.ready(document,function(){
	var resArray = ['/js/lib/jquery.2.1.1.min.js','/js/lib/base.js','/js/lib/wxShare.js','/js/lib/fastclick.min.js'];
	lazyLoad(resArray,function(){
		baseInit();
		//addResizeListener('myResize');
		$(window).resize(myResize);
		wxInit('piano','全民钢琴','','piano/piano.png');
		$('app').on('touchmove',function(e){
			e.preventDefault();
			return false;
		});
		$(document).on('touchmove',function(e){
			e.preventDefault();
			return false;
		});
		//initKey();
		var channel_max = 32;
		window.audiochannels = new Array();
		for (var a=0;a<channel_max;a++) {
			window.audiochannels[a] = new Array();
			window.audiochannels[a]['channel'] = new Audio();
			window.audiochannels[a]['finished'] = -1;
			window.audiochannels[a]['keyvalue'] = '';
		}
		$('#keyContainer').on('click',function(e){
			e = e||window.event;
			playKey(e.target.id);
		});
		$(window).resize();
	});
})