/*447101029@qq.com*/
window.gridSize = 0;//单个格子的尺寸
window.tunnelArray = [[1,1,1,1],[1,1,0,1],[1,0,1,0],[0,0,1,1]];//数组四位数字分别表示上右下左四个方向是否有出口
window.tunnelImg = ['crossTunnelImgUrl','TTunnelImgUrl','lineTunnelImgUrl','LTunnelImgUrl'];
window.beerClassName = ['beer notConnect','beer caskConnect','beer glassConnect'];
window.crossTunnelType = 0;//十字管道
window.TTunnelType = 1;//T字管道
window.lineTunnelType = 2;//直线管道
window.LTunnelType = 3;//L形管道
window.notConnect = 0;//表示管道没有相连
window.caskConnect = 1;//表示管道与上边的酒桶相连
window.glassConnect = 2;//表示管道与下边的酒杯相连
window.scoreArray = [1,2,4,8,16,32];
window.addTimeArray = [1,2,3,4,5,6];
window.mapContainer = null;
window.tunnelContainer = null;
window.canTouchMove = false;
window.gameOver = false;
window.flowEnd = false;
window.rowNum = 6;
window.colNum = 6;
function Tunnel(tunnelType,pos,deg){
	this.ele = document.createElement('div');
	this.img = document.createElement('img');
	this.img.draggable = false;
	this.ele.appendChild(this.img);
	this.beer = document.createElement('div');
	this.beer.className = 'beer';
	this.ele.appendChild(this.beer);
	this.flowWater = document.createElement('div');
	this.flowWater.className = 'flowWater';
	this.ele.appendChild(this.flowWater);
	this.ele.className = 'tunnel';
	this.ele.style.width = window.gridSize+'px';
	this.ele.style.height = window.gridSize+'px';
	this.ele.onclick = tunnelClick;
	window.tunnelContainer.appendChild(this.ele);
	this.tunnelType = -1;
	this.setType(tunnelType);
	this.setPosition(pos);
	this.array = [0,0,0,0];
	this.deg = -1;
	this.setRotateDeg(deg,true);
	this.state = -1;
	this.disappeared = false;
	this.dropDownLength = 0;
	this.caskConnectArray = [];
	this.glassConnectArray = [];
	return this;
}
Tunnel.prototype.setRotateDeg = function(deg,noAnimate){
	var start = this.deg*90;
	var needRotate = (this.deg != deg);
	this.deg = deg;
	var goal = this.deg*90;
	for(var i = 0; i < 4; i++){
		var j = i-(this.deg%4);
		if(j<0){
			j += 4;
		}
		this.array[i] = window.tunnelArray[this.tunnelType][j];
	}
	if(!needRotate){return;}
	var $e = $(this.img);
	if(start<0||noAnimate){
		$e.css('transform','rotate('+goal+'deg)');
		return;
	}
	var $b = $(this.beer);
	function animate(){
		if(window.gameOver){return;}
		start = Math.min(goal,start+10);
		$e.css('transform','rotate('+start+'deg)');
		$b.css('transform','rotate('+start+'deg)');
		if(start<goal){
			window.RAF(animate);
		}
	}
	animate();
}
Tunnel.prototype.getRotateDeg = function(){
	return this.deg;
}
Tunnel.prototype.setType = function(type){
	if(this.tunnelType==type){return;}
	this.tunnelType = type;
	this.img.src = window[window.tunnelImg[this.tunnelType]];
}
Tunnel.prototype.getType = function(){
	return this.tunnelType;
}
Tunnel.prototype.rotate = function(){
	this.setRotateDeg(this.deg+1,false);
}
Tunnel.prototype.canEnter = function(direct){
	return (this.array[direct] === 1);
}
Tunnel.prototype.setPosition = function(pos){
	this.position = pos||{x:0,y:0}
	this.ele.style.left = this.position.y*window.gridSize+'px';
	this.ele.style.top = this.position.x*window.gridSize+'px';
	$(this.ele).data('pos',{
		x:this.position.x,
		y:this.position.y
	});
}
Tunnel.prototype.resize = function(){
	this.ele.style.width = window.gridSize+'px';
	this.ele.style.height = window.gridSize+'px';
	this.ele.style.left = this.position.y*window.gridSize+'px';
	this.ele.style.top = this.position.x*window.gridSize+'px';
}
Tunnel.prototype.setState = function(state,flag){
	this.state = state;
	if(flag){
		this.beer.className = window.beerClassName[this.state];
	}
}
Tunnel.prototype.getState = function(){
	return this.state;
}
Tunnel.prototype.hasConnectAllCask = function(array){
	var arrayLength = array.length;
	var curLength = this.caskConnectArray.length;
	if(arrayLength>curLength){
		return false;
	}
	for(var i = 0; i < arrayLength; i++){
		var flag = false;
		for(var j = 0; j < curLength; j++){
			if(this.caskConnectArray[j] == array[i]){
				flag = true;
				break;
			}
		}
		if(!flag){
			return false;
		}
	}
	return true;
}
Tunnel.prototype.hasConnectOneCask = function(array){
	var arrayLength = array.length;
	var curLength = this.caskConnectArray.length;
	for(var i = 0; i < arrayLength; i++){
		for(var j = 0; j < curLength; j++){
			if(this.caskConnectArray[j] == array[i]){
				return true;
			}
		}
	}
	return false;
}
Tunnel.prototype.addConnectCask = function(array){
	var arrayLength = array.length;
	var curLength = this.caskConnectArray.length;
	for(var i = 0; i < arrayLength; i++){
		var flag = false;
		for(var j = 0; j < curLength; j++){
			if(this.caskConnectArray[j] == array[i]){
				flag = true;
				break;
			}
		}
		if(!flag){
			this.caskConnectArray.push(array[i]);
		}
	}
	this.caskConnectArray.sort();
}
Tunnel.prototype.hasConnectAllGlass = function(array){
	var arrayLength = array.length;
	var curLength = this.glassConnectArray.length;
	if(arrayLength>curLength){
		return false;
	}
	for(var i = 0; i < arrayLength; i++){
		var flag = false;
		for(var j = 0; j < curLength; j++){
			if(this.glassConnectArray[j] == array[i]){
				flag = true;
				break;
			}
		}
		if(!flag){
			return false;
		}
	}
	return true;
}
Tunnel.prototype.hasConnectOneGlass = function(array){
	var arrayLength = array.length;
	var curLength = this.glassConnectArray.length;
	for(var i = 0; i < arrayLength; i++){
		for(var j = 0; j < curLength; j++){
			if(this.glassConnectArray[j] == array[i]){
				return true;
			}
		}
	}
	return false;
}
Tunnel.prototype.addConnectGlass = function(array){
	var arrayLength = array.length;
	var curLength = this.glassConnectArray.length;
	for(var i = 0; i < arrayLength; i++){
		var flag = false;
		for(var j = 0; j < curLength; j++){
			if(this.glassConnectArray[j] == array[i]){
				flag = true;
				break;
			}
		}
		if(!flag){
			this.glassConnectArray.push(array[i]);
		}
	}
	this.glassConnectArray.sort();
}
Tunnel.prototype.connectSameAs = function(otherTunnel){
	var otherCaskLength = otherTunnel.caskConnectArray.length;
	var curCaskLength = this.caskConnectArray.length;
	if(otherCaskLength != curCaskLength){
		return false;
	}
	for(var i = 0; i < otherCaskLength; i++){
		if(otherTunnel.caskConnectArray[i] != this.caskConnectArray[i]){
			return false;
		}
	}
	var otherGlassLength = otherTunnel.glassConnectArray.length;
	var curGlassLength = this.glassConnectArray.length;
	if(otherGlassLength != curGlassLength){
		return false;
	}
	for(var i = 0; i < otherGlassLength; i++){
		if(otherTunnel.glassConnectArray[i] != this.glassConnectArray[i]){
			return false;
		}
	}
	return true;
}
Tunnel.prototype.copyConnect = function(otherTunnel){
	this.caskConnectArray = otherTunnel.caskConnectArray.slice(0);
	this.glassConnectArray = otherTunnel.glassConnectArray.slice(0);
}
Tunnel.prototype.clearConnect = function(otherTunnel){
	this.caskConnectArray = [];
	this.glassConnectArray = [];
	this.setState(window.notConnect,false);
}
Tunnel.prototype.disappear = function(){
	this.disappeared = true;
}
Tunnel.prototype.appear = function(){
	this.disappeared = false;
	this.ele.style.opacity = 1;
}
Tunnel.prototype.isDisappeared = function(){
	return this.disappeared;
}
Tunnel.prototype.setDropDownLength = function(len){
	this.dropDownLength = len;
}
Tunnel.prototype.getDropDownLength = function(){
	return this.dropDownLength;
}
Tunnel.prototype.flowFrom = function(direct,callback){
	if(this.hasFlowed()){return;}
	this.flowed = true;
	var ele = this.flowWater;
	ele.style.display = 'block';
	var baseWidth = window.gridSize*0.2742;
	var baseGap = (window.gridSize-baseWidth)/2;
	var baseLength = window.gridSize*0.629;
	switch(direct){
		case 0:
		ele.style.left = baseGap+'px';
		ele.style.top = '0px';
		ele.style.width = baseWidth+'px';
		ele.style.height = '0px';
		flowY(ele,baseGap,baseLength,this,callback);
		break;
		case 1:
		ele.style.top = baseGap+'px';
		ele.style.right = '0px';
		ele.style.width = '0px';
		ele.style.height = baseWidth+'px';
		flowX(ele,baseGap,baseLength,this,callback);
		break;
		case 2:
		ele.style.left = baseGap+'px';
		ele.style.bottom = '0px';
		ele.style.width = baseWidth+'px';
		ele.style.height = '0px';
		flowY(ele,baseGap,baseLength,this,callback);
		break;
		case 3:
		ele.style.top = baseGap+'px';
		ele.style.left = '0px';
		ele.style.width = '0px';
		ele.style.height = baseWidth+'px';
		flowX(ele,baseGap,baseLength,this,callback);
		break;
		default:break;
	}
}
Tunnel.prototype.hasFlowed = function(){
	return this.flowed;
}
Tunnel.prototype.clearFlow = function(){
	this.flowed = false;
	var ele = this.flowWater;
	ele.style.left = '';
	ele.style.top = '';
	ele.style.right = '';
	ele.style.bottom = '';
	ele.style.width = '0px';
	ele.style.height = '0px';
	ele.style.display = 'none';
}
function flowY(ele,curGap,baseLength,t,callback){
	var curHeight = 0;
	var dLen = Math.floor(window.gridSize/8);
	function flowExpand(){
		if(window.flowEnd){return;}
		curGap = Math.max(curGap-dLen,0);
		curHeight = Math.min(curHeight+dLen,window.gridSize);
		ele.style.left = curGap+'px';
		ele.style.width = (window.gridSize-2*curGap)+'px';
		ele.style.height = curHeight+'px';
		if(curGap>0||curHeight<window.gridSize){
			window.RAF(flowExpand);
		}
		else if(callback){
			callback(t);
		}
	}
	function flowDown(){
		if(window.flowEnd){return;}
		curHeight = Math.min(curHeight+dLen,baseLength);
		ele.style.height = curHeight+'px';
		if(curHeight<baseLength){
			window.RAF(flowDown);
		}
		else{
			flowExpand();
		}
	}
	flowDown();
}
function flowX(ele,curGap,baseLength,t,callback){
	var curWidth = 0;
	var dLen = Math.floor(window.gridSize/8);
	function flowExpand(){
		if(window.flowEnd){return;}
		curGap = Math.max(curGap-dLen,0);
		curWidth = Math.min(curWidth+dLen,window.gridSize);
		ele.style.top = curGap+'px';
		ele.style.height = (window.gridSize-2*curGap)+'px';
		ele.style.width = curWidth+'px';
		if(curGap>0||curWidth<window.gridSize){
			window.RAF(flowExpand);
		}
		else if(callback){
			callback(t);
		}
	}
	function flowLeft(){
		if(window.flowEnd){return;}
		curWidth = Math.min(curWidth+dLen,baseLength);
		ele.style.width = curWidth+'px';
		if(curWidth<baseLength){
			window.RAF(flowLeft);
		}
		else{
			flowExpand();
		}
	}
	flowLeft();
}
function tunnelClick(){
	if(!window.canTouchMove){return false;}
	var pos = $(this).data('pos');
	var x = pos.x;
	var y = pos.y;
	window.curMap[x][y].rotate();
	mapOnChange();
}
function addScore(num){
	window.curScore += window.scoreArray[num-1];
	$('#score').html(window.curScore);
	//$('#addScoreImg').attr('src',window['score'+(num-1)+'ImgUrl']);
	$('#scoreAddArea').append(window['score'+(num-1)+'Img']).css('display','block');
	setTimeout(function(){
		$('#scoreAddArea').css('display','none').html('');
	},1000);
}
function getConnectGlassArray(){
	var result = [];
	for(var i = 0; i < window.colNum; i++){
		var tmpTunnel = window.curMap[window.rowNum-1][i];
		if(tmpTunnel.getState()==window.caskConnect&&tmpTunnel.canEnter(2)){
			result.push(i);
		}
	}
	return result;
}
function getConnectTunnelArray(glassArray){
	var result = [];
	for(var i = 0; i < window.rowNum; i++){
		for(var j = 0; j < window.colNum; j++){
			var tmpTunnel = window.curMap[i][j];
			if(tmpTunnel.getState()==window.caskConnect&&tmpTunnel.hasConnectOneGlass(glassArray)){
				result.push({
					x:i,
					y:j
				});
			}
		}
	}
	return result;
}
function addTime(num){
	window.leftTime += window.addTimeArray[num-1];
	$('#curTime').html(window.leftTime).css('font-size','1.5em');
	setTimeout(function(){
		$('#curTime').css('font-size','1em');
	},1000);
}
function glassJump(callback){
	var array = getConnectGlassArray();
	var num = array.length;
	if(num<1){return;}
	var curTop = 0;
	function jumpHigh(){
		curTop = Math.max(curTop-1,-20);
		for(var i = 0; i < num; i++){
			$('#spirit'+(array[i]+1)+'Img').css('top',curTop);
		}
		if(curTop>-10){
			window.RAF(jumpHigh);
		}
		else{
			jumpDown();
		}
	}
	function jumpDown(){
		curTop = Math.min(curTop+1,0);
		for(var i = 0; i < num; i++){
			$('#spirit'+(array[i]+1)+'Img').css('top',curTop);
		}
		if(curTop<0){
			window.RAF(jumpDown);
		}
		else if(callback){
			setTimeout(callback,100);
		}
	}
	jumpHigh();
}
function dfsShowFlow(ele){
	var nextTunnel,nextX,nextY;
	nextX = ele.position.x+1;
	nextY = ele.position.y;
	if(ele.canEnter(2)&&nextX==window.rowNum){
		window.curShowNum++;
		if(window.curShowNum>=window.needToShow){
			var connectGlassArray = getConnectGlassArray();
			var glassNum = connectGlassArray.length; 
			addScore(glassNum);
			addTime(glassNum);
			window.flowEnd = true;
			glassJump(mapClear);
			return;
		}
	}
	if(checkPos(nextX,nextY)&&ele.canEnter(2)){
		nextTunnel = window.curMap[nextX][nextY];
		if(nextTunnel.canEnter(0)&&!nextTunnel.hasFlowed()){
			nextTunnel.flowFrom(0,dfsShowFlow);
		}
	}

	nextX = ele.position.x;
	nextY = ele.position.y+1;
	if(checkPos(nextX,nextY)&&ele.canEnter(1)){
		nextTunnel = window.curMap[nextX][nextY];
		if(nextTunnel.canEnter(3)&&!nextTunnel.hasFlowed()){
			nextTunnel.flowFrom(3,dfsShowFlow);
		}
	}

	nextX = ele.position.x;
	nextY = ele.position.y-1;
	if(checkPos(nextX,nextY)&&ele.canEnter(3)){
		nextTunnel = window.curMap[nextX][nextY];
		if(nextTunnel.canEnter(1)&&!nextTunnel.hasFlowed()){
			nextTunnel.flowFrom(1,dfsShowFlow);
		}
	}

	nextX = ele.position.x-1;
	nextY = ele.position.y;
	if(checkPos(nextX,nextY)&&ele.canEnter(0)){
		nextTunnel = window.curMap[nextX][nextY];
		if(nextTunnel.canEnter(2)&&!nextTunnel.hasFlowed()){
			nextTunnel.flowFrom(2,dfsShowFlow);
		}
	}
}
function showFlow(glassArray){
	window.needToShow = glassArray.length;
	window.curShowNum = 0;
	for(var j = 0; j < window.colNum; j++){
		var tmpTunnel = window.curMap[0][j];
		if(tmpTunnel.canEnter(0)&&tmpTunnel.hasConnectOneGlass(glassArray)&&!tmpTunnel.hasFlowed()){
			tmpTunnel.flowFrom(0,dfsShowFlow);
		}
	}
}
function checkStatus(){//检查是否有酒杯连通,有的话清除所有管道和被连通的酒杯,若所有的酒杯都被连通则提示以及通关
	var connectGlassArray = getConnectGlassArray();
	var glassNum = connectGlassArray.length;
	if(glassNum>0){
		showFlow(connectGlassArray);
	}
	else{
		window.canTouchMove = true;
	}
}
function mapClear(){
	var connectGlassArray = getConnectGlassArray();
	var animateArray = getConnectTunnelArray(connectGlassArray);
	var animateNum = animateArray.length;
	for(var i = 0; i < animateNum; i++){
		window.curMap[animateArray[i].x][animateArray[i].y].disappear();
	}
	var curOpacity = 100;
	var goalOpacity = 0;
	function animate(){
		if(window.gameOver){return;}
		curOpacity = Math.max(goalOpacity,curOpacity-4);
		for(var i=0; i < animateNum; i++){
			window.curMap[animateArray[i].x][animateArray[i].y].ele.style.opacity = curOpacity/100;
		}
		if(curOpacity>goalOpacity){
			window.RAF(animate);
		}
		else{
			mapDropDown();
		}
	}
	animate();
}
function mapDropDown(){
	var animateArray = [];
	var maxDropDownLen = 0;
	for(var j = 0; j < window.colNum; j++){
		var disappearNum = 0;
		for(var i = window.rowNum-1; i >= 0; i--){
			var tmpTunnel = window.curMap[i][j];
			tmpTunnel.clearFlow();
			if(tmpTunnel.isDisappeared()){
				disappearNum++;
			}
			else{
				if(disappearNum>0){
					tmpTunnel.setState(window.notConnect,true);
					tmpTunnel.setDropDownLength(disappearNum);
					animateArray.push({
						x:i,
						y:j
					});
				}
			}
		}
		maxDropDownLen = Math.max(maxDropDownLen,disappearNum);
	}
	maxDropDownLen *= window.gridSize;
	var dLen = 0;
	var animateNum = animateArray.length;
	function animate(){
		if(window.gameOver){return;}
		dLen = Math.min(dLen+10,maxDropDownLen);
		for(var i=0; i < animateNum; i++){
			var tmpTunnel = window.curMap[animateArray[i].x][animateArray[i].y];
			var dy = Math.min(dLen,tmpTunnel.getDropDownLength()*window.gridSize);
			tmpTunnel.ele.style.top = (tmpTunnel.position.x*window.gridSize+dy)+'px';
		}
		if(dLen<maxDropDownLen){
			window.RAF(animate);
		}
		else{
			for(var i=0; i < animateNum; i++){
				var tmpTunnel = window.curMap[animateArray[i].x][animateArray[i].y];
				var newX = tmpTunnel.position.x+tmpTunnel.getDropDownLength();
				var newY = tmpTunnel.position.y;
				var nextTunnel = window.curMap[newX][newY];
				nextTunnel.setType(tmpTunnel.getType());
				nextTunnel.setRotateDeg(tmpTunnel.getRotateDeg(),true)
				nextTunnel.setPosition({
					x:newX,
					y:newY
				});
				nextTunnel.setState(window.notConnect,true);
				nextTunnel.appear();
				tmpTunnel.disappear();
			}
			mapFillEmpty();
		}
	}
	animate();
}
function mapFillEmpty(){
	var animateArray = [];
	var animateNum = 0;
	var maxDropDownLen = 0;
	for(var j = 0; j < window.colNum; j++){
		var disappearNum = 0;
		for(var i = window.rowNum-1; i >= 0; i--){
			var tmpTunnel = window.curMap[i][j];
			if(tmpTunnel.isDisappeared()){
				disappearNum++;
				var randomType = Math.floor(Math.random()*4);
				var randomDeg = Math.floor(Math.random()*4);
				tmpTunnel.setType(randomType);
				tmpTunnel.setRotateDeg(randomDeg,true);
				tmpTunnel.setPosition({
					x:i,
					y:j
				});
				tmpTunnel.setState(window.notConnect,true);
				tmpTunnel.appear();
				tmpTunnel.ele.style.top = (-disappearNum*window.gridSize)+'px';
				animateNum = animateArray.push({
					x:i,
					y:j
				});
			}
		}
		for(var i = 0; i < window.rowNum; i++){
			window.curMap[i][j].setDropDownLength(disappearNum);
		}
		maxDropDownLen = Math.max(maxDropDownLen,disappearNum);
	}
	maxDropDownLen *= window.gridSize;
	var dLen = 0;
	function animate(){
		if(window.gameOver){return;}
		dLen = Math.min(dLen+10,maxDropDownLen);
		for(var i=0; i < animateNum; i++){
			var tmpTunnel = window.curMap[animateArray[i].x][animateArray[i].y];
			if(dLen>tmpTunnel.getDropDownLength()*window.gridSize){
				continue;
			}
			var curTop = parseFloat(tmpTunnel.ele.style.top);
			tmpTunnel.ele.style.top = (curTop+10)+'px';
		}
		if(dLen<maxDropDownLen){
			window.RAF(animate);
		}
		else{
			for(var i = 0; i < animateNum; i++){
				var tmpTunnel = window.curMap[animateArray[i].x][animateArray[i].y];
				tmpTunnel.setPosition({
					x:animateArray[i].x,
					y:animateArray[i].y
				});
			}
			mapOnChange();
		}
	}
	animate();
}
function checkPos(x,y){
	return (x>=0&&x<window.rowNum&&y>=0&&y<window.colNum);
}
function mapRefresh(){//将所有管道重置为未连接状态
	for(var i = 0; i < window.rowNum; i++){
		for(var j = 0; j < window.colNum; j++){
			window.curMap[i][j].clearConnect();
		}
	}
}
function showNotConnect(){
	for(var i = 0; i < window.rowNum; i++){
		for(var j = 0; j < window.colNum; j++){
			var tmpTunnel = window.curMap[i][j];
			tmpTunnel.resize();
			if(tmpTunnel.getState() == window.notConnect){
				tmpTunnel.setState(window.notConnect,true);
			}
		}
	}
}
function mapOnChange(){
	window.canTouchMove = false;
	window.flowEnd = false;
	mapRefresh();
	beerFlow();
	glassFlow();
	checkStatus();
	showNotConnect();
}
function generateMap(){
	window.tunnelContainer.innerHTML = '';
	window.curScore = 0;
	$('#score').html(window.curScore);
	window.curMap = [];
	for(var i = 0; i < window.rowNum; i++){
		var tmpArray = [];
		for(var j = 0; j < window.colNum; j++){
			var randomType = Math.floor(Math.random()*4);
			var randomDeg = Math.floor(Math.random()*4);
			tmpArray.push(new Tunnel(randomType,{x:i,y:j},randomDeg));
		}
		window.curMap.push(tmpArray);
	}
	mapRefresh();
	beerFlow();
	glassFlow();
	if(getConnectGlassArray().length>0){
		generateMap();
	}
	showNotConnect();
}
function beerFlow(){
	for(var i = 0; i < window.colNum; i++){
		var tmpTunnel = window.curMap[0][i];
		if(tmpTunnel.canEnter(0)){
			if(tmpTunnel.getState()!=window.caskConnect){
				tmpTunnel.setState(window.caskConnect,true);
			}
			if(!tmpTunnel.hasConnectAllCask([i])){
				tmpTunnel.addConnectCask([i]);
				dfsFlow(0,i);
			}
		}
	}
}
function glassFlow(){
	for(var i = 0; i < window.colNum; i++){
		var tmpTunnel = window.curMap[window.rowNum-1][i];
		if(tmpTunnel.canEnter(2)){
			if(tmpTunnel.getState()==window.notConnect){
				tmpTunnel.setState(window.glassConnect,true);
			}
			if(!tmpTunnel.hasConnectAllGlass([i])){
				tmpTunnel.addConnectGlass([i]);
				dfsFlow(window.rowNum-1,i);
			}
		}
	}
}
function dfsFlow(x,y){
	var dx = x+1;
	var dy = y;
	var tmpTunnel = window.curMap[x][y];
	var nextTunnel = null;
	if(tmpTunnel.canEnter(2)&&checkPos(dx,dy)){
		nextTunnel = window.curMap[dx][dy];
		if(nextTunnel.canEnter(0)){
			if(nextTunnel.getState()==window.notConnect){
				nextTunnel.setState(tmpTunnel.getState(),true);
			}
			if(!nextTunnel.connectSameAs(tmpTunnel)){
				nextTunnel.copyConnect(tmpTunnel);
				dfsFlow(dx,dy);
			}
		}
	}
	dx = x;
	dy = y+1;
	tmpTunnel = window.curMap[x][y];
	if(tmpTunnel.canEnter(1)&&checkPos(dx,dy)){
		nextTunnel = window.curMap[dx][dy];
		if(nextTunnel.canEnter(3)){
			if(nextTunnel.getState()==window.notConnect){
				nextTunnel.setState(tmpTunnel.getState(),true);
			}
			if(!nextTunnel.connectSameAs(tmpTunnel)){
				nextTunnel.copyConnect(tmpTunnel);
				dfsFlow(dx,dy);
			}
		}
	}
	dx = x;
	dy = y-1;
	tmpTunnel = window.curMap[x][y];
	if(tmpTunnel.canEnter(3)&&checkPos(dx,dy)){
		nextTunnel = window.curMap[dx][dy];
		if(nextTunnel.canEnter(1)){
			if(nextTunnel.getState()==window.notConnect){
				nextTunnel.setState(tmpTunnel.getState(),true);
			}
			if(!nextTunnel.connectSameAs(tmpTunnel)){
				nextTunnel.copyConnect(tmpTunnel);
				dfsFlow(dx,dy);
			}
		}
	}
	dx = x-1;
	dy = y;
	tmpTunnel = window.curMap[x][y];
	if(tmpTunnel.canEnter(0)&&checkPos(dx,dy)){
		nextTunnel = window.curMap[dx][dy];
		if(nextTunnel.canEnter(2)){
			if(nextTunnel.getState()==window.notConnect){
				nextTunnel.setState(tmpTunnel.getState(),true);
			}
			if(!nextTunnel.connectSameAs(tmpTunnel)){
				nextTunnel.copyConnect(tmpTunnel);
				dfsFlow(dx,dy);
			}
		}
	}
}
function getPlace(num,array){
	var len = array.length;
	for(var i = 0; i < len; i++){
		if(num>=array[i]){
			return (i+1);
		}
	}
	return len;
}
function updateScore(){
	$.ajax({
		url:'/addTunnelRecord',
		type:'post',
		data:{
			score:window.curScore
		},
		success:function(reply){
			console.log(reply);
			if(reply.status=='success'){
				reply.place = Math.min(reply.place,reply.total-1);
				wxSetContent('我在疯狂管道中获得了'+reply.score+'分,在所有'+reply.total+'个玩家中排在第'+(reply.place+1)+'名！');
			}
		},
		error:function(err){
			console.log(err);
		}
	})
}
function gameEnd(){
	window.gameOver = true;
	window.flowEnd = true;
	window.canTouchMove = false;
	var curPlace = getPlace(window.curScore,window.recordArray);
	wxSetContent('我在疯狂管道中获得了'+window.curScore+'分,在所有'+window.recordArray.length+'个玩家中排在第'+curPlace+'名！');
	updateScore();
	var numStr = ''+window.curScore;
	var numLength = numStr.length;
	var numHtml = '';
	for(var i = 0; i < numLength; i++){
		numHtml += '<img src="'+window['num'+parseInt(numStr[i])+'ImgUrl']+'" height="'+(window.fontSize*1.5)+'">';
	}
	$('#userScore').html(numHtml);
	$('#chooseBoard').show();
}
function gameStart(){
	window.gameOver = false;
	window.canTouchMove = true;
	window.flowEnd = false;
	window.leftTime = 60;
	$('#curTime').html(window.leftTime);
	if(window.gameLoop){
		window.clearInterval(window.gameLoop);
	}
	window.gameLoop = setInterval(function(){
		window.leftTime--;
		$('#curTime').html(window.leftTime);
		if(window.leftTime==0){
			window.clearInterval(window.gameLoop);
			gameEnd();
		}
	},1000);
}
function showControlBoard(){
	$('#alertBoard').show();
}
function myResize(){
	console.log('myResize');
	$('#app').css('width',window.myWidth).css('height',window.myHeight);
	window.gridSize = Math.floor(window.myWidth/(window.colNum+0.4));
	window.containerSize = window.gridSize*window.colNum;
	$('.container').css('width',window.containerSize);
	$('.caskImg').css('width',window.gridSize);
	$('#caskContainer').css('margin-top',-window.gridSize/10).css('height',window.gridSize*104/96);
	var headerHeight = window.myWidth/4.6;
	$('#header').css('height',headerHeight);
	$('#score').css('left',(window.myWidth-window.containerSize));
	$('#timeRecord').css('right',(window.myWidth-window.containerSize));
	var lineWidth = Math.floor(window.containerSize/200);
	for(var i = 1; i < window.rowNum; i++){
		var offset = i*window.gridSize-lineWidth/2;
		$('#rowLine'+i).css('height',lineWidth).css('top',offset);
	}
	for(var i = 1; i < window.colNum; i++){
		var offset = i*window.gridSize-lineWidth/2;
		$('#colLine'+i).css('width',lineWidth).css('left',offset);
	}
	for(var i = 0; i < window.rowNum; i++){
		for(var j = 0; j < window.colNum; j++){
			window.curMap[i][j].resize();
		}
	}
	$('#mapContainer').css('height',window.gridSize*window.rowNum);
	$('#footer img').css('width',window.gridSize);
	var alertOkButtonWidth = window.containerSize*0.94*0.5;
	var alertOkButtonHeight = alertOkButtonWidth*96/294;
	var alertOkButtonLeft = (window.containerSize*0.94-alertOkButtonWidth)/2;
	$('#alertOkButton').css('width',alertOkButtonWidth).css('height',alertOkButtonHeight).css('left',alertOkButtonLeft);
	var chooseButtonWidth = window.containerSize*0.94*0.4;
	var chooseButtonHeight = alertOkButtonWidth*79/242;
	var chooseButtonLeft = window.containerSize*0.94*0.08;
	$('#chooseOkButton').css('width',chooseButtonWidth).css('height',chooseButtonHeight).css('left',chooseButtonLeft);
	$('#chooseNotButton').css('width',chooseButtonWidth).css('height',chooseButtonHeight).css('right',chooseButtonLeft);
}
function gameInit(){
	window.imgPrefix = '/image/tunnel/';
	$('#alertBoardBg').attr('src',window.imgPrefix+'startBoardBg.png');
	$('#alertOkButton').attr('src',window.imgPrefix+'startButton.png');
	$('.caskImg').attr('src',window.imgPrefix+'cask.png');
	for(var i = 1; i < 7; i++){
		$('#spirit'+i+'Img').attr('src',window.imgPrefix+'spirit'+i+'.png');
	}
	$('#clockImg').attr('src',window.imgPrefix+'clock.png');
	for(var i = 0; i < 6; i++){
		var tmpUrl = window.imgPrefix+'+'+Math.pow(2,i)+'.png';
		window['score'+i+'Img'] = document.createElement('img');
		window['score'+i+'Img'].src = tmpUrl;
	}
	$('#wxRemind').attr('src',window.imgPrefix+'share.png');
	if(window.curRank.length>1){
		window.curRank = window.curRank.substr(0,window.curRank.length-1);
		window.recordArray = window.curRank.split(',');
		var len = window.recordArray.length;
		for(var i = 0; i < len; i++){
			window.recordArray[i] = parseInt(window.recordArray[i]);
		}
		window.recordArray.sort(function(a,b){return b-a;});
	}
	else{
		window.recordArray = [];
	}
	window.mapContainer = document.getElementById('mapContainer');
	window.tunnelContainer = document.getElementById('tunnelContainer');
	$(window.mapContainer).on('touchmove',function(e){
		e.preventDefault();
		return false;
	});
	baseInit();
	wxInit('tunnel','疯狂管道','疯狂管道','tunnel/crossTunnelNew.png','wxRemind');
	var lineContainer = document.getElementById('lineContainer');
	for(var i = 1; i < window.rowNum; i++){
		var rowLine = document.createElement('div');
		rowLine.className = 'rowLine line';
		rowLine.id = 'rowLine'+i;
		lineContainer.appendChild(rowLine);
	}
	for(var i = 1; i < window.colNum; i++){
		var colLine = document.createElement('div');
		colLine.className = 'colLine line';
		colLine.id = 'colLine'+i;
		lineContainer.appendChild(colLine);
	}
	generateMap();
	$(window).on('resize',myResize);
	$(window).resize();
	$('#alertOkButton').on('click',function(){
		$('#alertBoard').hide();
		gameStart();
	});
	$('#app').show();
	showControlBoard();
	$('#myScript').remove();
	$('#chooseOkButton').on('click',function(){
		generateMap();
		gameStart();
		$('#chooseBoard').hide();
	});
	$('#chooseNotButton').on('click',function(){
		generateMap();
		wxRemind(showControlBoard);
		$('#chooseBoard').hide();
	});
	setTimeout(function(){
		$('#chooseBoardBg').attr('src',window.imgPrefix+'scoreBoardBg.png');
		$('#chooseOkButton').attr('src',window.imgPrefix+'playAgainButton.png');
		$('#chooseNotButton').attr('src',window.imgPrefix+'shareButton.png');
		var s = document.createElement('script');
		s.src = '/js/tunnel/dev/laterImg.js';
		document.body.appendChild(s);
	},30*1000);
}