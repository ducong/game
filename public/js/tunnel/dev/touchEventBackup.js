function touchDragAble(ele){
	bindEvent(ele,'touchstart',function(e){
		if(!window.canTouchMove){return false;}
		$(this).data('drag',true).data('eventPos',{
			x:e.changedTouches[0].pageX,
			y:e.changedTouches[0].pageY
		}).css('z-index',100);
		return false;
	});
	bindEvent(ele,'touchmove',function(e){
		if(!window.canTouchMove){return false;}
		if(!$(this).data('drag')){return false;}
		var beforePos = $(this).data('eventPos');
		var curPos = {
			x:e.changedTouches[0].pageX,
			y:e.changedTouches[0].pageY
		}
		$(this).data('eventPos',curPos);
		var dx = curPos.x-beforePos.x;
		var dy = curPos.y-beforePos.y;
		var left = parseInt(this.style.left);
		var top = parseInt(this.style.top);
		left += dx;
		top += dy;
		this.style.left = left+'px';
		this.style.top = top+'px';
		return false;
	});
	bindEvent(ele,'touchend',function(e){
		if(!window.canTouchMove){return false;}
		if(!$(this).data('drag')){return false;}
		$(this).data('drag',false).css('z-index',1);
		var left = parseInt(this.style.left);
		var top = parseInt(this.style.top);
		var nx = Math.floor(top/window.gridSize);
		if(top-nx*window.gridSize>window.gridSize/2){
			nx++;
		}
		var ny = Math.floor(left/window.gridSize);
		if(left-ny*window.gridSize>window.gridSize/2){
			ny++;
		}
		var pos = $(this).data('pos');
		var x = pos.x;
		var y = pos.y;
		if(nx==x&&ny==y){
			window.curMap[x][y].setPosition({
				x:x,
				y:y
			});
			tunnelClick($(this).data('pos'));
			return false;
		}
		if(checkPos(nx,ny)){
			var tmpData = {
				type:window.curMap[nx][ny].getType(),
				deg:window.curMap[nx][ny].getRotateDeg()
			}
			window.curMap[nx][ny].setType(window.curMap[x][y].getType());
			window.curMap[nx][ny].setRotateDeg(window.curMap[x][y].getRotateDeg(),true);
			window.curMap[nx][ny].setPosition({
				x:nx,
				y:ny
			});
			window.curMap[x][y].setType(tmpData.type);
			window.curMap[x][y].setRotateDeg(tmpData.deg,true);
			window.curMap[x][y].setPosition({
				x:x,
				y:y
			});
			mapOnChange();
		}
		else{
			window.curMap[x][y].setPosition({
				x:x,
				y:y
			});
		}
	})
}
function mouseDragAble(ele){
	bindEvent(ele,'mousedown',function(e){
		if(!window.canTouchMove){return false;}
		$(this).data('drag',true).data('eventPos',{
			x:e.clientX,
			y:e.clientY
		}).css('z-index',100);;
	});
	bindEvent(ele,'mousemove',function(e){
		if(!window.canTouchMove){return false;}
		if(!$(this).data('drag')){return false;}
		var beforePos = $(this).data('eventPos');
		var curPos = {
			x:e.clientX,
			y:e.clientY
		}
		$(this).data('eventPos',curPos);
		var dx = curPos.x-beforePos.x;
		var dy = curPos.y-beforePos.y;
		var left = parseInt(this.style.left);
		var top = parseInt(this.style.top);
		left += dx;
		top += dy;
		this.style.left = left+'px';
		this.style.top = top+'px';
	});
	bindEvent(ele,'mouseup',function(e){
		if(!window.canTouchMove){return false;}
		if(!$(this).data('drag')){return false;}
		$(this).data('drag',false).css('z-index',1);;
		e.stopPropagation();
		var left = parseInt(this.style.left);
		var top = parseInt(this.style.top);
		var nx = Math.floor(top/window.gridSize);
		if(top-nx*window.gridSize>window.gridSize/2){
			nx++;
		}
		var ny = Math.floor(left/window.gridSize);
		if(left-ny*window.gridSize>window.gridSize/2){
			ny++;
		}
		var pos = $(this).data('pos');
		var x = pos.x;
		var y = pos.y;
		if(nx==x&&ny==y){
			window.curMap[x][y].setPosition({
				x:x,
				y:y
			});
			tunnelClick($(this).data('pos'));
			return false;
		}
		if(checkPos(nx,ny)){
			var tmpData = {
				type:window.curMap[nx][ny].getType(),
				deg:window.curMap[nx][ny].getRotateDeg()
			}
			window.curMap[nx][ny].setType(window.curMap[x][y].getType());
			window.curMap[nx][ny].setRotateDeg(window.curMap[x][y].getRotateDeg(),true);
			window.curMap[nx][ny].setPosition({
				x:nx,
				y:ny
			});
			window.curMap[x][y].setType(tmpData.type);
			window.curMap[x][y].setRotateDeg(tmpData.deg,true);
			window.curMap[x][y].setPosition({
				x:x,
				y:y
			});
			mapOnChange();
		}
		else{
			window.curMap[x][y].setPosition({
				x:x,
				y:y
			});
		}
	});
}