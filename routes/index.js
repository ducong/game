var express = require('express');
var router = express.Router();
var models = require('../models');
var pvModel = models.pvModel;

function getCurTime(){
	var curTime = new Date();
	var year = curTime.getFullYear();
	var month = curTime.getMonth()+1;
	if(month<10){
		month = '0'+month;
	}
	var date = curTime.getDate();
	if(date<10){
		date = '0'+date;
	}
	var hour = curTime.getHours();
	if(hour<10){
		hour = '0'+hour;
	}
	var minute = curTime.getMinutes();
	if(minute<10){
		minute = '0'+minute;
	}
	var second = curTime.getSeconds();
	if(second<10){
		second = '0'+second;
	}
	var result = year+'-'+month+'-'+date+' '+hour+':'+minute+':'+second;
	return result;
}
Array.prototype.binaryFind = function(goal,s,e){
	//假定数组已经按从大到小排序
	//排序方法为array.sort(function(a,b){return b-a;});
	var start,end;
	if(typeof(s)=='undefined'){
		start = 0;
	}
	else{
		start = s;
	}
	if(typeof(e)=='undefined'){
		end = this.length-1;
	}
	else{
		end = e;
	}
	if(start>end){return -1;}
	var mid = Math.floor((start+end)/2);
	if(this[mid]==goal){
		//若数组中有重复元素则返回首次出现的位置
		var j = mid;
		while(j>0&&this[j-1]==goal){j--;}
		return j;
	}
	var r = -1;
	if(this[mid]<goal){
		r = this.binaryFind(goal,start,mid-1);
	}
	else{
		r = this.binaryFind(goal,mid+1,end);
	}
	return r;
}
// Array.prototype.binaryFind = function(goal,start,end){
// 	for(var i = 0; i < this.length; i++){
// 		if(this[i]==goal){
// 			return i;
// 		}
// 	}
// 	return -1;
// }
function addPv(url,ip){
	pvModel.findOne({'ip':ip,'url':url},function(err,record){
		if(err){
			console.log('search ip:'+ip+' error');
		}
		else{
			if(record){
				record.times.push(getCurTime());
				record.save();
			}
			else{
				var newRecord = new pvModel({
					ip:ip,
					times:[getCurTime()],
					url:url,
					tunnelGoals:0
				});
				newRecord.save();
			}
		}
	})
}
/* GET home page. */
router.get('/', function(req, res) {
	addPv('index',req.ip);
  	res.render('index');
});

router.get('/getTheNut', function(req, res) {
	addPv('getTheNut',req.ip);
  	res.render('getTheNut');
});

router.get('/piano', function(req, res) {
	addPv('piano',req.ip);
  	res.render('piano');
});

router.get('/bomb', function(req, res) {
	addPv('bomb',req.ip);
  	res.render('bomb');
});

router.get('/stickHero', function(req, res) {
	addPv('stickHero',req.ip);
  	res.render('stickHero');
});

router.get('/tetris', function(req, res) {
	addPv('tetris',req.ip);
  	res.render('tetris');
});

router.get('/tunnel', function(req, res) {
	addPv('tunnel',req.ip);
	pvModel.find({'url':'tunnel'},function(err,record){
		if(err){
			res.render('tunnel',{
  				systemTime:(new Date()).valueOf(),
  				curRank:''
  			});
		}
		else{
			var result = '';
			if(record&&record.length>0){
				var len = record.length;
				for(var i = 0; i < len; i++){
					result += record[i].tunnelGoals+',';
				}
			}
			res.render('tunnel',{
  				systemTime:(new Date()).valueOf(),
  				curRank:result
  			});
		}
	})
});

router.post('/addTunnelRecord', function(req, res) {
	pvModel.findOne({'ip':req.ip,'url':'tunnel'},function(err,record){
		if(err){
			res.send({
				status:'failed',
				code:0
  			});
		}
		else{
			if(record){
				record.tunnelGoals = req.body.score;
				record.save(function(err){
					if(err){
						res.send({
							status:'failed',
							code:1
  						});
					}
					else{
						pvModel.find({'url':'tunnel'},function(err,allRecord){
							if(err){
								res.send({
									status:'failed',
									code:2
  								});
							}
							else{
								var result = [];
								if(allRecord&&allRecord.length>0){
									var len = allRecord.length;
									for(var i = 0; i < len; i++){
										result.push(allRecord[i].tunnelGoals);
									}
									result.sort(function(a,b){
										return b-a;
									});
									var place = result.binaryFind(req.body.score);
									if(place>=0){
										res.send({
											status:'success',
											total:result.length,
											place:place,
											score:req.body.score
										});
									}
									else{
										res.send({
											status:'failed',
											code:3
  										});
									}
								}
								else{
									res.send({
										status:'failed',
										code:4
  									});
								}
								
							}
						});
					}
				})
			}
			else{
				res.send({
					status:'failed',
					code:5
  				});
			}
		}
	})
});
module.exports = router;
