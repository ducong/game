// var config = require('./config.json');
var isDev = true;

module.exports = function(grunt){

	// 项目配置
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			lib:{
				options: {
					banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n',
					sourceMap: isDev
				},
				files: {
					'public/js/lib/lib.min.js':[
						'public/js/lib/dev/jquery.2.1.1.min.js',
						'public/js/lib/dev/fastclick.min.js',
						'public/js/lib/dev/wxShare.js',
						'public/js/lib/dev/base.js'
					]
				}
			},
			stickHero:{
				options: {
					banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n',
					sourceMap: isDev
				},
				files: {
					'public/js/stickHero/index.min.js':[
						'public/js/stickHero/dev/index.js'
					]
				}
			},
			tetris:{
				options: {
					banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n',
					sourceMap: isDev
				},
				files: {
					'public/js/tetris/index.min.js':[
						'public/js/tetris/dev/blockLib.js',
						'public/js/tetris/dev/block.js',
						'public/js/tetris/dev/index.js'
					]
				}
			},
			getTheNut:{
				options: {
					banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n',
					sourceMap: isDev
				},
				files: {
					'public/js/getTheNut/index.min.js':[
						'public/js/getTheNut/dev/map.js',
						'public/js/getTheNut/dev/boar.js',
						'public/js/getTheNut/dev/mouse.js',
						'public/js/getTheNut/dev/squirrel.js',
						'public/js/getTheNut/dev/imageBase64.js',
						'public/js/getTheNut/dev/index.js'
					]
				}
			},
			tunnel:{
				options: {
					banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> */\n',
					sourceMap: isDev
				},
				files: {
					'public/js/tunnel/index.min.js':[
						'public/js/tunnel/dev/preloadImg.js',
						'public/js/tunnel/dev/index.js'
					]
				}
			}
		},
		sass: {
			lib:{
				files: {
					'public/css/lib/lib.min.css': 'public/css/lib/scss/index.scss'
				},
				options: {
					style: 'compressed',
					sourcemap: isDev
				}
			},
			stickHero: {
				files: {
					'public/css/stickHero/index.min.css': 'public/css/stickHero/scss/index.scss'
				},
				options: {
					style: 'compressed',
					sourcemap: isDev
				}
			},
			tetris: {
				files: {
					'public/css/tetris/index.min.css': 'public/css/tetris/scss/index.scss'
				},
				options: {
					style: 'compressed',
					sourcemap: isDev
				}
			}
		},
		watch: {
			libCss:{
				files: [
					'public/css/lib/scss/*.scss'
				],
				tasks: ['sass:lib'],
				options: {
					debounceDelay: 250,
				}
			},
			libJs:{
				files: [
					'public/js/lib/dev/*.js'
				],
				tasks: ['uglify:lib'],
				options: {
					debounceDelay: 300,
				}
			},
			stickHeroCss: {
				files: [
					'public/css/stickHero/scss/*.scss'
				],
				tasks: ['sass:stickHero'],
				options: {
					debounceDelay: 250,
				}
			},
			stickHeroJs: {
				files: [
					'public/js/stickHero/dev/*.js'
				],
				tasks: ['uglify:stickHero'],
				options: {
					debounceDelay: 300,
				}
			},
			stickHeroHtml: {
				files: [
					'views/dev/stickHero.html'
				],
				tasks: ['htmlmin:stickHero'],
				options: {
					debounceDelay: 500,
				}
			},
			tetrisCss: {
				files: [
					'public/css/tetris/scss/*.scss'
				],
				tasks: ['sass:tetris'],
				options: {
					debounceDelay: 250,
				}
			},
			tetrisJs: {
				files: [
					'public/js/tetris/dev/*.js'
				],
				tasks: ['uglify:tetris'],
				options: {
					debounceDelay: 300,
				}
			},
			tetrisHtml: {
				files: [
					'views/dev/tetris.html'
				],
				tasks: ['htmlmin:tetris'],
				options: {
					debounceDelay: 500,
				}
			},
			getTheNutJs: {
				files: [
					'public/js/getTheNut/dev/*.js'
				],
				tasks: ['uglify:getTheNut'],
				options: {
					debounceDelay: 300,
				}
			},
			getTheNutHtml: {
				files: [
					'views/dev/getTheNut.html'
				],
				tasks: ['htmlmin:getTheNut'],
				options: {
					debounceDelay: 500,
				}
			},
			tunnelJs: {
				files: [
					'public/js/tunnel/dev/*.js'
				],
				tasks: ['uglify:tunnel'],
				options: {
					debounceDelay: 300,
				}
			},
			tunnelHtml: {
				files: [
					'views/dev/tunnel.html'
				],
				tasks: ['htmlmin:tunnel'],
				options: {
					debounceDelay: 500,
				}
			}
		},
		concurrent: {
			options: {
				logConcurrentOutput: true
			},
			watchStickHero: {
				tasks: [
				'watch:stickHeroCss',
				'watch:stickHeroJs',
				'watch:stickHeroHtml',
				'watch:libCss',
				'watch:libJs'
				]
			},
			watchTetris: {
				tasks: [
				'watch:tetrisCss',
				'watch:tetrisJs',
				'watch:tetrisHtml',
				'watch:libCss',
				'watch:libJs'
				]
			},
			watchGetTheNut: {
				tasks: [
				'watch:getTheNutJs',
				'watch:getTheNutHtml',
				'watch:libCss',
				'watch:libJs'
				]
			},
			watchTunnel: {
				tasks: [
				'watch:tunnelJs',
				'watch:tunnelHtml',
				'watch:libCss',
				'watch:libJs'
				]
			}
		},
		htmlmin: {
			stickHero: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'views/stickHero.ejs': 'views/dev/stickHero.html'
				}
			},
			tetris: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'views/tetris.ejs': 'views/dev/tetris.html'
				}
			},
			getTheNut: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'views/getTheNut.ejs': 'views/dev/getTheNut.html'
				}
			},
			tunnel: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'views/tunnel.ejs': 'views/dev/tunnel.html'
				}
			}
		}
	});

	// 加载提供"uglify"任务的插件
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-concurrent');

	// 默认任务
	//grunt.registerTask('default', ['uglify', 'sass', 'htmlmin','watch']);
	grunt.registerTask('stickHero',[
		'uglify:lib',
		'sass:lib',
		'uglify:stickHero',
		'sass:stickHero',
		'htmlmin:stickHero',
		'concurrent:watchStickHero'
	]);
	grunt.registerTask('tetris',[
		'uglify:lib',
		'sass:lib',
		'uglify:tetris',
		'sass:tetris',
		'htmlmin:tetris',
		'concurrent:watchTetris'
	]);
	grunt.registerTask('getTheNut',[
		'uglify:lib',
		'sass:lib',
		'uglify:getTheNut',
		'htmlmin:getTheNut',
		'concurrent:watchGetTheNut'
	]);
	grunt.registerTask('tunnel',[
		'uglify:lib',
		'sass:lib',
		'uglify:tunnel',
		'htmlmin:tunnel',
		'concurrent:watchTunnel'
	]);
};